<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <style>
 
.quizArea{
 width: 95%;
margin:  auto;
padding:10px;
position: relative;
text-align: center;
}
.mc_quiz{
 color: #3a5336;
 margin-bottom: 0px;
}



.multipleChoiceQues{
 width:90%;
 margin: auto;
 padding: 10px;
  
}
.quizBox
{
 width:90%;
 margin: auto;

}

.question{
 text-align: center;
 font-size: 0.8em;
}

.buttonArea
{
 text-align: right;
 height: 4.5em;
}

button {
 height: 4em;
 width:130px;
 padding: 1.5em auto;
 margin: 1em auto;
 background-color:#f1f2ec;
 border: none;
 border-radius: 3px;
 color: #7aa4a9;
 text-transform: uppercase;
 letter-spacing: 0.5em;
 transition: all 0.2s cubic-bezier(.4,0,.2,1); 
 
}

#next:hover,
/* #submit:hover, */
.viewanswer:hover,
.viewchart:hover,
.backBtn:hover,
.replay:hover {
 letter-spacing: 0.4em;
}
/* .viewanswer,
.viewchart,
.replay{
 width: 30%;
} */

.backBtn{
 width:100px;
 height: 2em;
 font-size: 0.8em;
 margin-left: 70%;
}
#next:active,
/* #submit:active, */
.viewanswer:active,
.viewchart:active,
.backBtn:active,
.replay:active  {
 letter-spacing: 0.3em;
}

.resultArea{
 display: none;
 width:70%;
 margin: auto;
 
 padding: 10px;
 
}

.chartBox{
 width: 60%;
 margin:auto;
}

.resultPage1{
 
 text-align: center;
 
}
.resultBox h1{
 
}

.briefchart
{
 text-align:center;
}

.resultBtns{
 /* width: 60%; */
 margin: auto;
 text-align:center;
}
.resultPage2,
.resultPage3
{
 display: none;
 text-align: center;
}

.allAnswerBox{
 width: 100%;
 margin: 0;
 position: relative;
}

._resultboard{
 position: relative;
 display:inline-block;
 width: 40%;
 padding: 2%;
   height: 190px;
 vertical-align: top;
 border-bottom: 0.6px solid rgba(255,255,255,0.2);
 text-align: left;
 margin-bottom: 4px;
 
}

._resultboard:nth-child(even){
 
 
 margin-left: 5px;
 border-left: 0.6px solid rgba(255,255,255,0.2);
}
._resultboard:nth-last-child(2),
._resultboard:nth-last-child(1){
 border-bottom: 0px;
}

._header{
 font-weight: bold;
   margin-bottom: 8px;
   height: 90px;
}

._yourans,
._correct{
 margin-bottom: 8px;
 position: relative;
 line-height: 2;
 padding: 5px; 
}
._correct{
 background: #968089 ;
}
.h-correct{
 background: #968089;

}

.h-correct:after,
._correct:after {
 line-height: 1.4;
 position: absolute;
 z-index: 499;
 font-family: 'FontAwesome';
 content: "\f00c";
 bottom: 0;
 right: 7px;
 font-size: 1.9em;
 color: #2dceb1;
}
.h-incorrect{
 background: #ab4e6b ;
}
.h-incorrect:after {
 line-height: 1.4;
 position: absolute;
 z-index: 499;
 font-family: 'FontAwesome';
 content: "\f00d";
 bottom: 0;
 right: 7px;
 font-size: 1.9em;
 color: #ff383e;
}

.resultPage3 h1,
.resultPage1 h1,
.resultPage2 h1{
 text-align: center;
 padding-bottom: 10px;
   border-bottom: 1.3px solid rgba(21, 63, 101,0.9);
   color: #3a5336;
}

.my-progress {
 position: relative;
 display: block;
 margin: 3rem auto 0rem;
 width: 100%;
 max-width: 950px;
}

progress {
 display: block;
 position: relative;
 top: -0.5px;
   left: 5px;
 -webkit-appearance: none;
 -moz-appearance: none;
      appearance: none;
 background: #f1f2ec ;
 width: 100%;
 height: 2.5px;
 background: none;
 -webkit-transition: 1s;
 transition: 1s;
 will-change: contents;
}
progress::-webkit-progress-bar {
 background-color: #f1f2ec;
}
progress::-webkit-progress-value {
 background-color:#153f65;
 -webkit-transition: all 0.5s ease-in-out;
 transition: all 0.5s ease-in-out;
}

.my-progress-indicator {
 position: absolute;
 top: -6px;
 left: 0;
 display: inline-block;
 width: 5px;
 height: 5px;
 background: #7aa4a9;
 border: 3px solid #f1f2ec;
 border-radius: 50%;
 -webkit-transition: all .2s ease-in-out;
 transition: all .2s ease-in-out;
 -webkit-transition-delay: .3s;
         transition-delay: .3s;
 will-change: transform;
}
.my-progress-indicator.progress_1 {
 left: 0;
}
.my-progress-indicator.progress_2 {
 left: 9%;
}
.my-progress-indicator.progress_3 {
 left: 18%;
}
.my-progress-indicator.progress_4{
 left: 27%;
}
.my-progress-indicator.progress_5 {
 left: 36%;
}
.my-progress-indicator.progress_6 {
 left: 45%;
}
.my-progress-indicator.progress_7 {
 left: 54%;
}
.my-progress-indicator.progress_8 {
 left: 63%;
}
.my-progress-indicator.progress_9 {
 left: 72%;
}
.my-progress-indicator.progress_10 {
 left: 81%;
}
.my-progress-indicator.progress_11 {
 left: 90%;
}
.my-progress-indicator.progress_12 {
 left: 100%;
}
.my-progress-indicator.active {
 -webkit-animation: bounce .5s forwards;
         animation: bounce .5s forwards;
 -webkit-animation-delay: .5s;
         animation-delay: .5s;
 border-color: #153f65 ;

}

.animation-container {
 position: relative;
 width: 100%;
 -webkit-transition: .3s;
 transition: .3s;
 will-change: padding;
 overflow: hidden;
}

.form-step {
 position: absolute;
 -webkit-transition: 1s ease-in-out;
 transition: 1s ease-in-out;
 -webkit-transition-timing-function: ease-in-out;
         transition-timing-function: ease-in-out;
 will-change: transform, opacity;
}

.form-step.leaving {
 -webkit-animation: left-and-out .5s forwards;
         animation: left-and-out .5s forwards;
}

.form-step.waiting {
 -webkit-transform: translateX(400px);
         transform: translateX(400px);
}

.form-step.coming {
 -webkit-animation: right-and-in .5s forwards;
         animation: right-and-in .5s forwards;
}

@-webkit-keyframes left-and-out {
 100% {
   opacity: 0;
   -webkit-transform: translateX(-400px);
           transform: translateX(-400px);
 }
}

@keyframes left-and-out {
 100% {
   opacity: 0;
   -webkit-transform: translateX(-400px);
           transform: translateX(-400px);
 }
}
@-webkit-keyframes right-and-in {
 100% {
   opacity: 1;
   -webkit-transform: translateX(0);
           transform: translateX(0);
 }
}
@keyframes right-and-in {
 100% {
   opacity: 1;
   -webkit-transform: translateX(0);
           transform: translateX(0);
 }
}
@-webkit-keyframes bounce {
 50% {
   -webkit-transform: scale(1.5);
           transform: scale(1.5);
 }
 100% {
   -webkit-transform: scale(1);
           transform: scale(1);
 }
}
@keyframes bounce {
 50% {
   -webkit-transform: scale(1.5);
           transform: scale(1.5);
 }
 100% {
   -webkit-transform: scale(1);
           transform: scale(1);
 }
}
.sr-only {
 position: absolute;
 width: 1px;
 height: 1px;
 padding: 0;
 margin: -1px;
 overflow: hidden;
 clip: rect(0, 0, 0, 0);
 border: 0;
}

.hidden {
 display: none;
}


ul{
 list-style-type: none;
 width: 220px;
 margin: auto;
 text-align: left;
}

li {
 position: relative;
 /* padding: 10px; */
 padding-left: 40px;
 height:30px;
}
label{
 color: #fff;
}
label:before {
   content: "";
   width: 15px;
   height: 15px;
   background: #fff ;
   position: absolute;
   left: 7px;
   top: calc(50% - 13px);
   box-sizing: border-box;
   border-radius: 50%;
}

input[type="radio"] {
 opacity: 0;
 -webkit-appearance: none;
 display: inline-block;
 vertical-align: middle;
 z-index: 100;
 margin: 0;
 padding: 0;
 width: 100%;
 height: 30px;
 position: absolute;
 left: 0;
 top: calc(50% - 15px);
 cursor: pointer;
}

.bullet {
   position: relative;
   width: 25px;
   height: 25px;
   left: 2px;
    top: -3px;
   border: 5px solid #fff ;
   opacity: 0;
   border-radius: 50%;
}

input[type="radio"]:checked ~ .bullet {
 position:absolute;
 opacity: 1;
 animation-name: explode;
 animation-duration: 0.350s;
}

.line {
 position: absolute;
 width: 10px;
 height: 2px;
 background-color: #fff ;
 opacity:0;
}

.line.zero {
 left: 11px;
 top: -21px;
 transform: translateY(20px);
 width: 2px;
 height: 10px;
}

.line.one {
 right: -7px;
 top: -11px;
 transform: rotate(-55deg) translate(-9px);
}

.line.two {
 right: -20px;
 top: 11px;
 transform: translate(-9px);
}

.line.three {
 right: -8px;
 top: 35px;
 transform: rotate(55deg) translate(-9px);
}

.line.four {
 left: -8px;
 top: -11px;
 transform: rotate(55deg) translate(9px);
}

.line.five {
 left: -20px;
 top: 11px;
 transform: translate(9px);
}

.line.six {
 left: -8px;
 top: 35px;
 transform: rotate(-55deg) translate(9px);
}

.line.seven {
 left: 11px;
 bottom: -21px;
 transform: translateY(-20px);
 width: 2px;
 height: 10px;
}

input[type="radio"]:checked ~ .bullet .line.zero{
 animation-name:drop-zero;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

input[type="radio"]:checked ~ .bullet .line.one{
 animation-name:drop-one;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

input[type="radio"]:checked ~ .bullet .line.two{
 animation-name:drop-two;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

input[type="radio"]:checked ~ .bullet .line.three{
 animation-name:drop-three;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

input[type="radio"]:checked ~ .bullet .line.four{
 animation-name:drop-four;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

input[type="radio"]:checked ~ .bullet .line.five{
 animation-name:drop-five;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

input[type="radio"]:checked ~ .bullet .line.six{
 animation-name:drop-six;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

input[type="radio"]:checked ~ .bullet .line.seven{
 animation-name:drop-seven;
 animation-delay: 0.100s;
 animation-duration: 0.9s;
 animation-fill-mode: forwards;
}

@keyframes explode {
 0%{
   opacity: 0;
   transform: scale(10);
 }
 60%{
   opacity: 1;
   transform: scale(0.5);
 }
 100%{
   opacity: 1;
   transform: scale(1);
 }
}

@keyframes drop-zero {
 0% {
   opacity: 0;
   transform: translateY(20px);
   height: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: translateY(-2px);
   height: 0px;
   opacity:0;
 }
}

@keyframes drop-one {
 0% {
   opacity: 0;
   transform: rotate(-55deg) translate(-20px);
   width: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: rotate(-55deg) translate(9px);
   width: 0px;
   opacity:0;
 }
}

@keyframes drop-two {
 0% {
   opacity: 0;
   transform: translate(-20px);
   width: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: translate(9px);
   width: 0px;
   opacity:0;
 }
}

@keyframes drop-three {
 0% {
   opacity: 0;
   transform: rotate(55deg) translate(-20px);
   width: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: rotate(55deg) translate(9px);
   width: 0px;
   opacity:0;
 }
}

@keyframes drop-four {
 0% {
   opacity: 0;
   transform: rotate(55deg) translate(20px);
   width: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: rotate(55deg) translate(-9px);
   width: 0px;
   opacity:0;
 }
}

@keyframes drop-five {
 0% {
   opacity: 0;
   transform: translate(20px);
   width: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: translate(-9px);
   width: 0px;
   opacity:0;
 }
}

@keyframes drop-six {
 0% {
   opacity: 0;
   transform: rotate(-55deg) translate(20px);
   width: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: rotate(-55deg) translate(-9px);
   width: 0px;
   opacity:0;
 }
}

@keyframes drop-seven {
0% {
   opacity: 0;
   transform: translateY(-20px);
   height: 10px;
 }
 20% {
   opacity:1;
 }
 100% {
   transform: translateY(2px);
   height: 0px;
   opacity:0;
 }
}

 </style>
  </head>
  <body class="bg-info"  >
  <span id='close' class='close'>x</span>

  <div class="quizArea">
  <div class="multipleChoiceQues">

    <!-- <div class="my-progress">
        <progress class="my-progress-bar" min="0" max="100" value="0" step="9" aria-labelledby="my-progress-completion"></progress>    
        <p id="my-progress-completion" class="js-my-progress-completion sr-only" aria-live="polite">0% complete</p>
    </div> -->
    <div class="quizBox">
    <form action="">
     <div class="tabbable">
        <ul class="nav nav-tabs"  style="display:none; ">
          <li class="active" style="display:none; "><a href="#tab1" data-toggle="tab" id="focusmeplease">Questions</a></li>
         
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab1">
           <form>
               <div id="qn" name="qn"><h2>how are you?</h2></div>
               <input type="hidden" id="qn1" name="qn1" value="1">
               <br>
               <input type="text" id="text1" name="text1">
               <a href="#tab2" data-toggle="tab" class="btn pull-right">  <input type="submit" value="Next" id="next1"></a>
           </form>
           
          </div>
          <div class="tab-pane" id="tab2">
           <form>
               <div id="qn" name="qn"><h2>how is your day?</h2></div>
               <input type="hidden" id="qn2" name="qn2" value="2">
               <br>
               <input type="text" id="text2" name="text2">
               <a href="#tab3" data-toggle="tab" class="btn pull-right">  <input type="submit" value="Next" id="next2"></a>
           </form>
           
          </div>
          <div class="tab-pane" id="tab3">
           <form>
               <div id="qn" name="qn"><h2>What do you want?</h2></div>
               <input type="hidden" id="qn3" name="qn3" value="3">
               <br>
               <input type="text" id="text3" name="text3">
               <input type="submit" value="submit" id="submit" > 
           </form>
           
          </div>
        </div>
      </div>
</form>
<div class="tab-pane active" id="tab1">
    <!--  -->
  <div class="resultArea">  
    <div class="resultPage1">
      <div class="resultBox">
        <h1>Results</h1>
      </div>
      <div class="briefchart">
        <svg height="300" width="300" id="_cir_progress">
          <g>
            <rect x="0" y="1" width="30" height="15"  fill="#ab4e6b" />
            <text x="32" y="14" font-size="14"  class="_text_incor">Incorrect : 12 </text>
          </g>
          <g>
            <rect x="160" y="1" width="30" height="15"  fill="#968089" />
            <text x="192" y="14" font-size="14" class="_text_cor">Correct : 12</text>
         </g>          
          <circle class="_cir_P_x" cx="150" cy="150" r="120" stroke="#ab4e6b" stroke-width="20" fill="none" onmouseover="evt.target.setAttribute('stroke', 'rgba(171, 78, 107,0.7)');" onmouseout="evt.target.setAttribute('stroke','#ab4e6b');"></circle>

          <circle class="_cir_P_y" cx="150" cy="150" r="120" stroke="#968089" stroke-width="20"  stroke-dasharray="0,1000" fill="none"  onmouseover="evt.target.setAttribute('stroke', 'rgba(150, 128, 137,0.7)');" onmouseout="evt.target.setAttribute('stroke','#968089');"></circle>
          <text x="50%" y="50%" text-anchor="middle" stroke="none" stroke-width="1px" dy=".3em" class="_cir_Per">0%</text>
          </svg>
      </div>

      <!-- <div class="resultBtns">
        <button class="viewanswer w-25">View Answers</button>
    
       <button class="replay"><i class="fa fa-repeat" style="font-size:1em;"></i> <br/>Replay</button> 
      </div> -->
    </div>

    <div class="resultPage2">
      <h1>Your Result</h1>
      <div class="chartBox">
        <canvas id="myChart" width="400" height="400"></canvas>
      </div>
      <button class="backBtn">Back</button>
    </div>

    <div class="resultPage3">
      <!-- <h1>Your Answers</h1>
      <div class="allAnswerBox">
        
      </div> -->
      <button class="backBtn">Back</button>
    </div>

  </div>
</div>
      </div> 
      
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script>

// document.getElementById('focusmeplease').focus();
window.onload=function(){
  document.getElementById("focusmeplease").click();
};

      $("div[id^='myModal']").each(function(){
  
  var currentModal = $(this);
  
  //click next
  currentModal.find('.btn-next').click(function(){
    currentModal.modal('hide');
    currentModal.closest("div[id^='myModal']").nextAll("div[id^='myModal']").first().modal('show'); 
  });
  
  //click prev
  currentModal.find('.btn-prev').click(function(){
    currentModal.modal('hide');
    currentModal.closest("div[id^='myModal']").prevAll("div[id^='myModal']").first().modal('show'); 
  });

});
 
  $("#submit").on('click',function(e){
    // $('.multipleChoiceQues').hide();
    var qn1=$('#qn1').val();
    var text1=$('#text1').val();
    var qn2=$('#qn2').val();
    var text2=$('#text2').val();
    var qn3=$('#qn3').val();
    var text3=$('#text3').val();
   
    $.ajax({
   
        url: "submitanswers.php",
				type: "POST",
				data: {
                    qn1: qn1,
					text1: text1,
				    qn2: qn2,
					text2: text2,
                    qn3: qn3,
					text3: text3	
				},
				cache: false,
				success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
			 
					
				}
			});
 

  $('#myModal').modal('toggle');
  return false;
});

  $("#close").on('click',function(e){
    //  addClickedAnswerToResult(questions,presentIndex,clicked);
    $('.multipleChoiceQues').hide();
    $(".resultArea").show();
    // renderResult(resultList);

   e.preventDefault();
    // Coding
    $('#myModal').modal('toggle'); //or  $('#IDModal').modal('hide');
  //  window.location.href='webcast.php';  
    return false;

  });
  </script>
 
  <script>
   $(".resultArea").on('click','.replay',function(){
    window.location.reload(true);
  });
  </script>
  </body>
</html>

