<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_empid"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $empid=$_SESSION["user_empid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where emp_code='$empid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_empid"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link href="/your-path-to-fontawesome/css/fontawesome.css" rel="stylesheet">
<link href="/your-path-to-fontawesome/css/brands.css" rel="stylesheet">
<link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet">
</head>
<style>

@import url('https://fonts.googleapis.com/css?family=Roboto+Slab');

body{

 background: #7aa4a9;
 font-family: 'Roboto Slab', serif;
 font-size: 18px;
 color:#f1f2ec;
}

</style>
<body class="webcast">
<div class="container-fliud bg-whit">
<audio src="bg.mp3" autoplay="autoplay" loop="loop"></audio>
<div class="d-xl-none d-md-none">
<img src="img/mainlogo_hex.png"  class="img-fluid "  width="200px"  alt="">
<img src="img/logo_hex.png"  class="img-fluid float-right " width="120px" alt="">
</div>
<div class="content-area">
  <div class="main-area">
  <div class="row ">
  
        <div class="col-12">
           <img src="img/Headline.png" width=40%  class="" alt=""/> 
        </div>
		
    </div>
    <div class="row  user-info">
        <!-- <div class="col-12 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn-logout">Logout</a>
        </div> -->
    </div>
    
    <div class="row">
        <div class="col-12 col-md-9 web1 col-lg-9"> 
            <div class="embed-responsive embed-responsive-16by9">
            <!-- <iframe src="video.php" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe> -->
            <iframe src="video.php" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
            </div>   
         
        </div>

        <div class="row   user-info">
        <!-- <div class="col-12 float-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn-logout"><i class="fas fa-chevron-left"></i> Logout</a>
        </div> -->
    </div>

    <!-- <div class="d-xl-none d-md-none mt-3">
  <img src="img/bottomlogo.png" class="img-fluid" alt="">
  </div> -->
  <!-- <div class="col-12 col-md-3 bg col-lg-2">
  <h3>Results</h3>
  <P>Results:<?php echo $correct= $_SESSION['correct'];  ?></P>
  </div>&nbsp; -->
        <div class="col-12 col-md-3 bg col-lg-3">
          <img src="img/Ask Your Question.png" alt="" width="400px" class="pl-5 change">
            <div class="question-box "> 
              <form id="question-form" method="post" role="form">
                      <div class="row">
                        <div class="col-10 offset-1">
                            <!-- <h6>Ask your Question:</h6> -->
                            <div id="message"></div>
                            <div class="form-group">
                               <textarea class="form-control" style="background-color:#d9d9d9;" name="userQuestion" id="userQuestion" required placeholder="Enter your Message here" rows="6"></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-10 offset-1">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_empid" name="user_empid" value="<?php echo $_SESSION['user_empid']; ?>">
                            <!-- <input type="submit" id="submitQues" class="btn-submit" value="Submit Question" alt="Submit"> -->
                            <input type="image"  id="submitQues"  src="img/Submit Button (1).png" class="offset-6" alt="Submit" width="120" >
                             <div id="message"></div>
                        </div>
                      </div>  
                </form>
                <div id="polls" >
            
            <div class="col-12">
                        <iframe id="poll-question" src="#" width="100%" height="270" frameborder="0" scrolling="no"></iframe>
                </div> 
                </div> 
            </div>   
            
        </div>
     
    </div>
    <div >
  <img src="img/logo.png" alt="" class="img-fluid float-right mt-4" width="170px">
</div>
    
  </div>  
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
    
      <div class="modal-body  text-dark bg-info">
    
      <?php
include 'test1.php';
   ?>

                <!-- <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button> -->
      
    </div>

  </div>
</div>
  
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){       
   $('#myModal').modal('show');

    }); 
    

$(document).ready(function() {
    $("#my_audio").get(0).play();
});
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                $('#submitQues').attr('disabled', true);
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                $('#submitQues').attr('disabled', false);
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

var pollTimer;
function chkPolls()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'chkpoll'},
         type: 'post',
         success: function(output) {
			   if(output != 0)
			   {    
                    if($('#poll-question').attr('src') != output)
                   {
                       $("#poll-question").attr("src", output);
                       $('#polls').css('display','');
                   }
			   }
               else{
                   $("#poll-question").attr("src", '#');
                   $('#polls').css('display','none');
               }
         }
    });
}
chkPolls();
pollTimer = setInterval(function(){ chkPolls(); }, 10000);
</script>
<script>
$('#myModal').modal({
    backdrop: 'static',
    keyboard: false
})
</script>
<script>
$(document).ready(function() {
	$('#submitQues').on('click', function() {
		$("#submitQues").attr("disabled", "disabled");
		var user_empid = $('#user_empid').val();
	 
	 
			$.ajax({
				url: "submitresults.php",
				type: "POST",
				data: {
					user_empid : user_empid 
				 	
				},
				cache: false,
				success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
				 
					
				}
			});
 
	 
	});
});
</script>
</body>
</html>