<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: ./");
	}
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Siemens</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>

<body>
<div class="container-fluid">
  <div class="row p-3">
      <div class="col-5 offset-7 col-md-2 offset-md-6 text-right d-block d-md-none">
          <img src="img/siemens-logo.png" class="img-fluid" alt=""/> 
    </div>
      <div class="col-10 col-md-4">
          <img src="img/face-logo.png" class="img-fluid" alt=""/> 
      </div>
      <div class="col-12 col-md-2 offset-md-6 text-right d-none d-md-block">
          <img src="img/siemens-logo.png" class="img-fluid" alt=""/> 
    </div>
  </div>
  
  <div class="row mt-2">
      <div class="col-12 col-md-8">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" id="webcast" src="video.php" allowfullscreen></iframe>
          </div>
      </div>
      <div class="col-12 col-md-4">
          <div id="question" class="mb-5">
              <div id="question-form" class="panel panel-default">
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="5"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_fname']; ?>">
                          <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['user_email']; ?>">
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                </form>
              </div>
          </div> 
    </div>
  </div>

</div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
    update();
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
             if(output =='0')
             {
                 //location.href = 'logout.php';
             }
         }
});
}
setInterval(function(){ update(); }, 15000);

</script>
</body>
</html>