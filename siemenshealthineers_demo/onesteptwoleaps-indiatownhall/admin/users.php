<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["admin_user"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Users</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<div class="container-fluid">
     <div class="row p-2">
        <div class="col-12 col-md-2">
            <img src="../img/logo-tristar.png" class="img-fluid" alt=""/> 
        </div>
        <div class="col-12 col-md-2 offset-md-8">
            <img src="../img/logo-safety-at-sea.png" class="img-fluid" alt=""/> 
        </div>
        
    </div>

     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="users"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getUsers('1');
});

function update(pageNum)
{
  getUsers(pageNum);
}

function getUsers(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getusers', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#users").html(response);
            
        }
    });
    
}

function logoutUser(uid)
{
   $.ajax({
        url: 'ajax.php',
         data: {action: 'logoutuser', userid: uid},
         type: 'post',
         success: function(output) {
             getUsers('1');
         }
   });
}
</script>

</body>
</html>