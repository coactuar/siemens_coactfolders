<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Siemens Healthineers </title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
::-webkit-input-placeholder { /* Edge */
  color: #bfbfbf !important;
  /* font-weight: 400; */
  font-family: Verdana, sans-serif;
 
  
}

:-ms-input-placeholder { /* Internet Explorer */
  color: #bfbfbf !important;
  /* font-weight: 400; */
  font-family: Verdana, sans-serif;
  
}

::placeholder {
  color: #bfbfbf !important;
  /* font-weight: 400; */
  font-family: Verdana, sans-serif;
  
}

.join{
  width:150px;
}


</style>
</head>

<body style="background-color:#424242;">
  <div class="container-fluid"  >
    <div class="row " >
    
      <div class="col-12 col-md-6 col-lg-6 mt-3  p-0 " >
         <img src="img/womenimg.png"  class="h-100 " width="100% "  alt=""/>
      </div>
      <div class="col-12 col-md-5 col-lg-5 m-0 mt-3">
          <div class="container-fluid" style="background-color:#424242; color:#bfbfbf;">
              <div class="login  ">
                <img src="img/Kick off Fy22.png" class="responsive_margin" style="margin-top:120px;" alt=""/ width="180px"  >
                  <form id="login-form" class="" method="post" role="form">
                      <div id="login-message"></div>
                      <div class="form-group text-left"> 
                          <div class="form-group">
                            <input type="email" class="form-control" placeholder="E-mail:" name="email_id" id="email" style="background-color:#595959; color:#bfbfbf; border-radius: 3px;" autocomplete="off" required>
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password:" name="password" id="lname" style="background-color:#595959; color:#bfbfbf; border-radius: 3px;" autocomplete="off" required>
                          </div>
                          <div class="form-group">
                              <p style="font-size:12px; text-align:left;">The protection of your data and recognition of your rights with regards to the collection, processing and use of your data are important to Siemens Healthineers. As part of this event,Siemens Healthineers may process your personal data and pass it on to third parties. This is limited to personal data that you actively and voluntarily provide during the registration process.</p>
                              <p  style="font-size:12px; text-align:left; ">The collected data will only be passed on to the third parties to provide services and functions in relation to this event, to verify your identity and to answer and fulfil your specific requests e.g. digital virtual, hybrid or live event participation and similar services. Any data collected in relation to this event shall be handled in accordance with Siemens Healthineers Data Privacy Notice.</p>
                          </div>
                          <div class="form-group">
                            
                            <p style="font-size:12px; text-align:left;"> <input type='checkbox' name='concent' class="" required> I hereby consent to the processing of my above given personal data by the Siemens Healthineers company as referred to under Corporate Information for the purposes as described in the <u><a href="https://events.siemens-healthineers.com/declaration-of-consent-to-enter-the-virtual-event-platform">Declaration of consent to enter the Virtual Event platform.</a></u>. I have also read the further information regarding the processing of my personal data contained in the <u><a href="https://events.siemens-healthineers.com/event-platform-privacy-policy">Data Privacy Policy.</a></u>
                            <br/>I am aware that I can partially or completely revoke this consent at any time for the future by declaring my revocation to the contact address given in the Corporate information or by sending it to the following e-mail address: <u>dataprivacy.communication@siemens-healthineers.com</u></p>
                          </div>
                          <div class="form-group mt-3 text-left">
                            <input type="image" src="img/Join.png" class="join" value="submit"> 
                          </div>
                          <br>
                          <br>
                          <br>	
                      </div>	  
                  </form>
              </div>
          </div>
      </div>
    </div>
  </div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  $('#login').attr('disabled', true);
  $.post('register-data.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
          // $('#login-message').text("You are registered successfully.");
          // $('#login-message').removeClass().addClass('alert alert-success').fadeIn().delay(3000).fadeOut();
          // $('#login-form').trigger("reset");
          // $('#login').attr('disabled', false);
          location.href='webcast.php'; 
          return false;  
      }
      
      else
      {
          $('#login-message').text(data);
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  
  
  return false;
});
$(".chb").change(function() {
    $(".chb").prop('checked', false);
    $(this).prop('checked', true);
});
</script>

</body>
</html>