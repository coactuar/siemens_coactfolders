<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: ./");
		exit;
	}
?>   
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Video AWS</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#player{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<div id="player"></div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
        source: "https://dnnzuzbuznubl.cloudfront.net/maricoawards/live.m3u8",  
        //source: "https://coact.live/TRISTAR/TRISTARv4.mp4",    
        parentId: "#player",
        plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              3: '1080p',              
              2: '720p',              
              1: '480p', 
              0: '360p', 
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;
          }
        },
        poster: "img/world-map.png",
        width: "100%",
        height: "100%",
        
    });
    
    //player.play();
</script>
</body>
</html>