<?php
	require_once "config.php";

	if(!isset($_SESSION["user_email"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
          date_default_timezone_set('Asia/Kolkata');
            $logout_date   = date('Y/m/d H:i:s');
            $user_email=$_SESSION["user_email"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where email='$user_email'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_email"]);
            
            header("location: ./register.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Siemens Healthineers</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
textarea {
  resize: none;
}

</style>
</head>

<body class="video-page" >


<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-12 text-right">
            <img src="img/logo.jpg" class="w-25 " alt=""/> 
        </div>
    </div>
   
    <div class="row mt-4">
      <div class="col-12 col-md-8 ">
      
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="video.php" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></iframe></div>          
			<!--
			<div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" id="webcast" src="video.php" allowfullscreen></iframe>
            </div>
			-->
      </div>
      <div class="col-12 col-md-4 ">
      <div class="col-12 ">
      <div class="text-right">
      Hello <?php echo $_SESSION['user_name']; ?>!
      </div>
   
    <a class="btn btn-sm btn-danger float-right "   href="?action=logout">Logout</a> 
        </div> 
        </div>
<!-- <img src="img/background_regrestration.png" class="img-fluid" alt=""> -->
              <div id="question-form" class="panel panel-default d-none ">
                  <form method="POST"  action="#" class="form panel-body " role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                            <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="5"></textarea>
                          </div> 
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['user_email']; ?>">
                          <button class="btn btn-primary btn-sm" type="submit">Submit your Question</button>
                          </div>
                      </div>
                </form>
				
              </div>
              <!-- <div class="col-12 ">
    <a class="btn btn-sm btn-danger float-right"   href="?action=logout">Logout</a> Hello <?php echo $_SESSION['user_name']; ?>!
        </div>  -->
        </div>
       
            
		
      
		
		</div>
       
		
		
		 
		
    </div>

			  
			 
          
    
</div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>

$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);

function getQues()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'getques'},
         type: 'post',
         success: function(output) {
             $('#asked-ques').html(output);
         }
});
}
getQues();
setInterval(function(){ getQues(); }, 10000);




</script>


</body>
</html>