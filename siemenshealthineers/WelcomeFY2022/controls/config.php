<?php
session_start();

$event_title = "Best of Orthopaedic Techniques - International Live 2020";

$limit = 50;

$timezone = 'Asia/Kolkata';
date_default_timezone_set($timezone);

//---------- Database Credentials ----------

define('DB_HOST', "localhost");
define('DB_USER', "root");
define('DB_PASS', "");
define('DB_NAME', "boot_live");


/*
define('DB_HOST', "localhost");
define('DB_USER', "genseer");
define('DB_PASS', "genseer123");
define('DB_NAME', "boot_live");
*/
$link = mysqli_connect(DB_HOST,DB_USER,DB_PASS, DB_NAME);

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
?>