<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>AIRCLaunch</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>

<body class="login-page">
<div class="container-fluid">
    <div class="row ">
        <div class="col-10 offset-1 col-md-5 offset-md-1 col-lg-4 offset-lg-2 p-3 text-center">
           
        </div>
        
    </div>
    <div class="row ">
      <div class="col-12 col-md-6 col-lg-6 offset-lg-1 p-2" style="background:black;" >
         <img src="img/Login Page-top.jpg" class="img-fluid" alt=""/>
			<div class="embed-responsive embed-responsive-16by9 ">
				 <video id="video1" width="420" autoplay loop muted>
					<source src="vids/login.mp4" type="video/mp4">
  
					Your browser does not support HTML video.
				</video>
              <!--<iframe class="embed-responsive-item" id="webcast" src="video-login.php" allowfullscreen></iframe>-->
            </div>
			
      </div>
      <div class="col-12 col-md-5 col-lg-3 offset-lg-1 p-0 mt-lg-5 ">
			
			<div class="login mt-lg-5">
			<img src="img/welcome.png" class="img-fluid text-left mt-lg-5" alt=""/>
				<form autocomplete="off"  id="login-form" method="post" role="form">
					  <div id="login-message"></div>
					  
					  <div class="form-group">   
						<input type="email"  class="form-control" placeholder="Username" name="email" id="email" required autocomplete='off' />	
					  </div>
					  <div class="form-group">   
					  <input  type="password" class="form-control" placeholder="Password" name="password" id="password" required autocomplete='off' />
					  </div>
					  <div class="form-group mt-2 text-left">
						<input type="image" src="img/Loginbutton.png" value="submit">
					  </div>
				</form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script>
$(document).on('submit', '#login-form', function()
{  $('#login').attr('disabled', true);
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
          location.href='webcast.php'; 
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  
  
  return false;
});
</script>

</body>
</html>