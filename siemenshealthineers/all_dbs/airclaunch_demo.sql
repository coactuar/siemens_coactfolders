-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 10, 2022 at 06:25 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airclaunch_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_empid` varchar(200) NOT NULL,
  `user_question` varchar(200) NOT NULL,
  `asked_at` varchar(200) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_empid`, `user_question`, `asked_at`, `speaker`, `answered`) VALUES
(2, 'Mr Akshat Jharia', 'akshatjharia@coact.co.in', 'Hi', '2021/04/21 10:41:16', 0, 0),
(3, 'Mr Aj J', 'aj@coact.co.in', 'Hi', '2021/04/21 11:34:43', 0, 0),
(4, 'neeraj', 'neeraj@coact.co.in', 'hello', '2021/04/21 11:42:57', 0, 0),
(5, 'Miss Pooja Jaiswal', 'pooja@coact.co.in', 'test', '2021/04/21 11:48:19', 0, 0),
(6, 'Mr Nishanth S', 'Nishanth@coact.co.in', 'Hello', '2021/04/21 14:05:30', 0, 0),
(7, ' Nishanth S', 'nishanth@coact.co.in', 'Hello', '2021/04/21 14:11:14', 0, 0),
(8, ' Md. Tohidujjaman ', 'md.tohidujjaman@siemens-healthineers.com', 'Joining ', '2021/04/22 07:59:48', 0, 0),
(9, 'Mr Taposundar Majumdar', 'taposundar.majumdar@varian.com', 'Good Morning. Are we going to have the mandatory PPL again?', '2021/04/22 08:19:03', 0, 0),
(10, 'Mr Hemant Patil', 'hemant.patil@varian.com', 'What is a difference between Siemens Healthcare & Siemens Healthineers ', '2021/04/22 08:36:21', 0, 0),
(11, ' Malti Sachdev', 'malti.sachdev@varian.com', 'Question for Vivek\r\nCould you share with us how Healthineers is leveraging its manufacturing footprint  in the country especially in the light of \"Atmanirbhar\" vision of the country ?', '2021/04/22 08:42:31', 0, 0),
(12, 'Dr Manohar Kollegal', 'manohar.kollegal@siemens-healthineers.com', 'Is Varex Imaging also part of this integration? Is there a possibility of bringing Varex Imaging manufacturing into India? ', '2021/04/22 08:55:13', 0, 0),
(13, 'Mr ANISH PILLAI', 'anish.pillai@varian.com', 'How we Varian employee can introduce in front of customer now.  From VARIAN or SIEMENS HEALTHINEERS', '2021/04/22 08:58:49', 0, 0),
(14, 'Mr Iyer Krishnan Seshan', 's.krishnan@siemens-healthineers.com', 'What are the expectations from some of the Indian Key customers from this combination? How do we plan to meet those expectations? Can we hear some examples?', '2021/04/22 09:05:23', 0, 0),
(15, 'Mr Singaravelu V B', 'vb.singaravelu@siemens-healthineers.com', 'Will there be an orientation session to the SHPL frontline team on the solutions from Varian ? An overview will help to lookout for opportunities in the market and creating higher mindshare, jointly.', '2021/04/22 09:10:10', 0, 0),
(16, 'Mr ANISH PILLAI', 'anish.pillai@varian.com', 'Why employee HR policy is different for VARIAN. ', '2021/04/22 09:10:33', 0, 0),
(17, 'Mr Iyer Krishnan Seshan', 's.krishnan@siemens-healthineers.com', 'Hello Ashok,\r\n\r\nI am excited to read \"Have Fun\". Do you already have something in your mind as what one or the other events for us to have fun!', '2021/04/22 09:12:28', 0, 0),
(18, 'Mr Joydeep Bhattacharjee', 'joydeep.bhattacharjee@siemens-healthineers.com', 'What is the plan of giving access of Varian Product learning platforms to SHPL Sales Team and vice versa ?', '2021/04/22 09:12:48', 0, 0),
(19, 'Mr Kishore Kumar', 'kishore.kumar@siemens-healthineers.com', 'Can I know the top 3 customers of Varian in India?', '2021/04/22 09:15:04', 0, 0),
(20, 'Mr Jayanta Mukherjee', 'jayanta.mukherjee@siemens-healthineers.com', 'How do we envisage CTSI playing a role in the combined entity, and how do we leverage CTSI in our business ', '2021/04/22 09:15:14', 0, 0),
(21, 'Mr Ashutosh Singh', 'ashutoshsingh@siemens-healthineers.com', 'How can we support in ongoing projects of varian?', '2021/04/22 09:15:31', 0, 0),
(22, 'Mr Jayanta Mukherjee', 'jayanta.mukherjee@siemens-healthineers.com', 'Will the new Bangalore campus now also look at including R&D and local manufacturing for Varian - any thoughts on that ', '2021/04/22 09:17:00', 0, 0),
(23, 'Ms Sadhana Sheth', 'sadhana.sheth@siemens-healthineers.com', 'What\'s the employee number breakup of varian in India into distribution colleagues and IT development colleagues? Any plans of integrating them into overall location strategy of Healthineers in India ', '2021/04/22 09:18:38', 0, 0),
(24, 'Mr Dhiroj  Barad ', 'dhiroj.barad@siemens-healthineers.com', 'Q to Ashok and Dr Jagprag \r\nCTSI is running hospitals. Hospitals are our customers. What is your exp. in competing with your own customer, particularly in those cities having such hospitals? ', '2021/04/22 09:18:41', 0, 0),
(25, 'Ms Richa Gour', 'richa.gour@siemens-healthineers.com', 'If you had to sum-up customer\'s response to this announcement in 1/2 sentences, what would it be? What are they expecting going forward?', '2021/04/22 09:19:13', 0, 0),
(26, 'Mr shankar Haveri', 'shankar.haveri@siemens-healthineers.com', 'To Vivek & Ashok\r\nPractically how soon would you see Siemens colleagues handling Varian products and processes; Similarly Varian colleagues handling Siemens products & processes?  ', '2021/04/22 09:20:03', 0, 0),
(27, ' Vineet Gupta', 'vineet.gupta@varian.com', 'CTSI has partnered with GE globally earlier. how would be the scenario now as several sites of CTSI has GE as an existing partner company?', '2021/04/22 09:20:58', 0, 0),
(28, 'Mr Babasaheb  Shingade', 'babasaheb.shingade@varian.com', 'Is there a customer facing team exists in Healthineers that is based in India and serve Global customers ?\r\n', '2021/04/22 09:21:17', 0, 0),
(29, 'Miss Poonam Yadav', 'poonam.yadav@siemens-healthineers.com', 'Hello team , any comment on how digital health solution of SHS be useful / helpful with Varian portfolio.', '2021/04/22 09:25:14', 0, 0),
(30, 'Mr Singaravelu V B', 'vb.singaravelu@siemens-healthineers.com', 'Can we look at short term functional delegations between the two (merged) organizations so that we become familiar with each other\'s ecosystems ?', '2021/04/22 09:25:40', 0, 0),
(31, 'Mr Amil  Shah', 'shah.amil@siemens-healthineers.com', 'Firstly congratulations, to all and welcome varian team to Siemens heathineer family. As Siemens heathineer has initiated on make India vision . Dose varin has plans too for the same. ', '2021/04/22 09:25:57', 0, 0),
(32, 'Ms Sadhana Sheth', 'sadhana.sheth@siemens-healthineers.com', 'Big opportunity in district level  initiative from Varian that of having our solutions in every district. We should combine our forces for this initiative for combined portfolio of our combined entity', '2021/04/22 09:26:54', 0, 0),
(33, 'Mr SAIKAT SARKAR', 'saikat.sarkar@siemens-healthineers.com', 'Cancer patients are depressed & finically weak. How cancer patient will be benefited?  Will Treatment cost be reduced?  ', '2021/04/22 09:27:28', 0, 0),
(34, ' Abhishek Sinha', 'abhishekkumar.sinha@siemens-healthineers.com', 'Are competitors also gearing up for same kind of consolidation with a vision to create synergy in healthcare space ! \r\nAny panelist can take up this question.', '2021/04/22 09:27:33', 0, 0),
(35, 'Miss suprita iyer', 'suprita.iyer@varian.com', 'Do SHS and SFS is completely different entity . \r\nCan SFS provide Finance to our orders and do we work as Intercompany in that case or separate entities ', '2021/04/22 09:28:25', 0, 0),
(36, 'Mr HANUMANTH PRASAD', 'hanumanthprasad.k@varian.com', 'Can we expect oncology systems working on Syngo via platform with this joint venture', '2021/04/22 09:28:39', 0, 0),
(37, 'Mr sanjeev  bhatli', 'sanjeev.bhatli@siemens-healthineers.com', 'what does customer gain/benefit from this combination & this synergy created?', '2021/04/22 09:28:53', 0, 0),
(38, 'Ms Kim Dsouza', 'kim.dsouza@siemens-healthineers.com', 'what is envisaged on the efforts of DC being shared with Varian for research ,going forward ?', '2021/04/22 09:30:18', 0, 0),
(39, 'Mr Ajay Khapre', 'ajay.khapre@varian.com', 'What is the NPS of Siemens Healthineer? Does NPS will be measured as a combined Organization in India, going forward?', '2021/04/22 09:33:25', 0, 0),
(40, 'Mr Prabhat Kumar', 'kumar.prabhat@siemens-healthineers.com', 'Customers may look at American Oncology as their competition. How has been Varian\'s experience on this and how it\'s been addressed.\r\nThank You!', '2021/04/22 09:34:16', 0, 0),
(41, 'Mr Pranav Patil', 'pranav-patil@siemens-healthineers.com', 'Question to Dileep and Nilesh...\r\n\r\nare there any synergies between the two software wings of Siemens Healthineers and Varian - how would we leverage the common strength', '2021/04/22 09:34:38', 0, 0),
(42, 'Dr Richard Rumnong', 'richard.rumnong@siemens-healthineers.com', 'What will be the relationship between SHPL and AIO from here on?', '2021/04/22 09:35:47', 0, 0),
(43, ' Prashant Sinha', 'prashantkumar.sinha@siemens-healthineers.com', 'It\'s delighting to see the staggering NPS of 83 plus...Is it the industry/business segment\'s benchmark? Would you like to elaborate a bit more on this vis-a-vis the global score? ', '2021/04/22 09:36:59', 0, 0),
(44, 'Miss Eurekha Mohan', 'eurekha.m@varian.com', 'Hello all, Good morning! As Varian India is going under APOC now, we would like to know if there is any region wise changes for the Global teams as well? Thank you!', '2021/04/22 09:37:24', 0, 0),
(45, 'Mr Joydeep Bhattacharjee', 'joydeep.bhattacharjee@siemens-healthineers.com', 'Varian is probably using Google Cloud platform for AI development and ML .. will that continue in the same platform in future or will combine @ Healthineers AI Cloud ?', '2021/04/22 09:43:44', 0, 0),
(46, 'Mr Aditya Rao', 'aditya.rao@siemens-healthineers.com', 'When do we envisage the policies of practices of both the entities to be integrated?', '2021/04/22 09:44:09', 0, 0),
(47, 'Mr Singaravelu V B', 'vb.singaravelu@siemens-healthineers.com', 'How big a force are we with these teams coming together (SHPL + Varian ) ? How many employees in India totally now ?', '2021/04/22 09:48:01', 0, 0),
(48, 'Mr Ajay Khapre', 'ajay.khapre@varian.com', 'With such a huge combined organizational footprint in India, & growth opportunities, Can India be 4th Region/GEO in next 2-3 years?', '2021/04/22 09:50:08', 0, 0),
(49, 'Mr Kalpesh Bhatt', 'kalpesh.bhatt@siemens-healthineers.com', 'Just a comment, with two solid product providers Healthineers and Varian with CTSI  service provider will definately give huge milage in oncology positioning.', '2021/04/22 09:51:44', 0, 0),
(50, ' Vineet Gupta', 'vineet.gupta@varian.com', 'What does Siemens do to hypnotize their users?\r\n', '2021/04/22 09:57:34', 0, 0),
(51, 'Mr vinay Kumar', 'vinaykumar.raja@siemens-healthineers.com', 'Question To Mr.Vivek  _ With Varian coming inn , how do you foresee the molecular imaging portfolio as business \r\n', '2021/04/22 09:57:49', 0, 0),
(52, 'Mr GERARD MAGESH  JOSEPH', 'gerard.joseph@varian.com', 'I am THRILLED and look forward ', '2021/04/22 10:00:20', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reactions`
--

CREATE TABLE `tbl_reactions` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `reaction` varchar(50) NOT NULL,
  `reaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `ms` int(11) NOT NULL,
  `miss` int(11) NOT NULL,
  `mr` int(11) NOT NULL,
  `dr` int(11) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `login_date` varchar(200) DEFAULT NULL,
  `logout_date` varchar(200) DEFAULT NULL,
  `joining_date` varchar(200) NOT NULL,
  `logout_status` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `is_varified` int(11) NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `ms`, `miss`, `mr`, `dr`, `fname`, `lname`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `is_varified`, `password`) VALUES
(1, 0, 0, 1, 0, 'Viraj', 'Khot', '2021/06/17 14:58:16', '2021/06/17 14:59:16', '2021/04/20 14:42:21', 1, 'viraj@coact.co.in', 0, 'f6fdc964a9f615b2751458dbd918acfd'),
(2, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '2021/04/20 17:35:18', '2021/04/20 16:09:14', '2021/04/20 14:45:50', 0, 'somunaik1750@coact.co.in', 0, '4a9c2151f9ce38dad6c0bad7c5c8f4de'),
(3, 0, 0, 1, 0, 'Reynold', 'Roy', '2021/04/20 17:40:32', '2021/04/20 17:40:53', '2021/04/20 15:10:35', 0, 'reynold@coact.co.in', 0, '78a1de44e02770250fd9be329a3295fc'),
(4, 0, 0, 0, 0, 'mahesh', 'ouiu', '2021/04/20 15:21:32', '2021/04/20 15:22:32', '2021/04/20 15:21:32', 0, 'mhh@coact.co.in', 0, 'a0bd730ddd388e839ca910b2bfa89253'),
(5, 0, 0, 1, 0, 'Nishanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/20 17:30:40', 0, 'nishanth@coact.co.in', 0, '675f7da285df0b97c24aa3e1c0a1b30b'),
(6, 0, 0, 1, 0, 'Rakshith', 'shetty', '2021/04/20 17:30:57', '2021/04/20 17:31:04', '2021/04/20 17:30:57', 0, 'rakshith134@coact.co.in', 0, 'ba5598f17547b3419485bb4c63588269'),
(7, 1, 0, 0, 0, 'Rakshithss', 'shetty', '2021/04/21 11:25:07', '2021/04/21 11:26:07', '2021/04/20 17:35:02', 0, 'rakshith@coact.co.in', 0, '004fb0aa793002db3c2708db4fa08226'),
(8, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '2021/04/20 17:35:18', '2021/04/20 16:09:14', '2021/04/20 17:35:18', 0, 'somunaik1750@coact.co.in', 0, 'eb8481e19162443ed01687fee4f63794'),
(9, 0, 0, 1, 0, 'Rakshith', 'shetty', '2021/04/21 11:25:07', '2021/04/21 11:26:07', '2021/04/20 17:35:27', 0, 'rakshith@coact.co.in', 0, '12f5d5a9cd41d4a662cdec6e306a5d36'),
(10, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '2021/04/20 17:38:02', '2021/04/20 16:26:49', '2021/04/20 17:38:02', 0, 'somunaik11750@coact.co.in', 0, 'e61bedb6a316a15598a5c3b5ec700f51'),
(11, 0, 0, 0, 0, 'Mahesh', 'Patil', '2021/04/20 17:38:54', '2021/04/20 17:39:54', '2021/04/20 17:38:54', 0, 'mahe@coact.co.in', 0, '622f927ea6cb9395941200519905381a'),
(12, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '2021/04/20 17:40:24', '2021/04/20 16:46:38', '2021/04/20 17:40:24', 0, 'somuna1ik1750@coact.co.in', 0, 'd40738f5edc48eb3477b17fd3631f593'),
(13, 0, 0, 1, 0, 'Reynold', 'Roy', '2021/04/20 17:40:32', '2021/04/20 17:40:53', '2021/04/20 17:40:32', 0, 'reynold@coact.co.in', 0, '567752c3369b007f42d82d8649da2c49'),
(14, 1, 0, 0, 0, 'Viraj', 'Khot', '2021/06/17 14:58:16', '2021/06/17 14:59:16', '2021/04/20 17:41:32', 1, 'viraj@coact.co.in', 0, 'cf76439d81c9f80a56dc6f0694f6c2a4'),
(15, 0, 0, 1, 0, 'rakshith', 'Shetty', '2021/04/21 08:24:40', '2021/04/21 08:25:40', '2021/04/20 17:41:46', 0, 'admin@coact.co.in', 0, 'b5cbdb9435f64993746cfcc8ac00e78c'),
(16, 1, 0, 0, 0, 'Rakshith', 'shetty', '2021/04/20 17:51:37', '2021/04/20 16:54:40', '2021/04/20 17:51:37', 0, 'rakshith11@coact.co.in', 0, '33af9a93276a0d05cb76a33eb91551e0'),
(17, 0, 0, 1, 0, 'rakshith', 'Shetty', '2021/04/20 18:10:23', '2021/04/20 18:10:44', '2021/04/20 18:10:23', 0, 'admin123@coact.co.in', 0, 'f9a76d6cac3c409ee5a46dd6b2014879'),
(18, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '2021/04/20 18:17:57', '2021/04/20 20:26:26', '2021/04/20 18:17:57', 0, 'somunaik112750@coact.co.in', 0, 'e9825b83376eee200d2136a1f7178f79'),
(19, 0, 0, 1, 0, 'SOMNATH', 'NAIK ', '2021/04/20 18:19:08', '2021/04/20 16:51:10', '2021/04/20 18:19:08', 0, 'somunaika1750@coact.co.in', 0, '6e36631abf880f8b1badc7e9fa912ef7'),
(20, 0, 0, 0, 0, 'mahesh', 'patil', '2021/04/20 18:34:38', '2021/04/20 17:06:39', '2021/04/20 18:32:08', 0, 'mah@coact.co.in', 0, 'e366aa4a9b06cd54bcdc966ea6ba3579'),
(21, 0, 0, 0, 0, 'Mahesh', 'Patil', '2021/04/20 18:34:38', '2021/04/20 17:06:39', '2021/04/20 18:34:38', 0, 'mah@coact.co.in', 0, '4979144c182b203bdf047b21b852a2d5'),
(22, 0, 0, 0, 0, 'mahesh', 'patil', '2021/04/20 18:35:09', '2021/04/20 17:12:39', '2021/04/20 18:35:09', 0, 'mpst@coact.co.in', 0, 'd13c6382275447344ab64081a8ee1011'),
(23, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:12:17', '2021/04/22 07:51:06', '2021/04/20 19:04:13', 0, 'akshat@coact.co.in', 0, '8b329788430ecb88352c49be5aef0cb9'),
(24, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:12:17', '2021/04/22 07:51:06', '2021/04/20 19:09:20', 0, 'akshat@coact.co.in', 0, '289aea8afc1dff191bc3c62b345de749'),
(25, 0, 0, 0, 1, 'Rakshith', 'Shetty', '2021/04/21 11:31:28', '2021/04/21 11:00:20', '2021/04/20 19:54:39', 0, 'rakshith1234@coact.co.in', 0, 'c113e1029f47201d81837f58f20b5e3f'),
(26, 0, 0, 1, 0, 'Rakshith', 'Shetty', '2021/04/21 11:25:07', '2021/04/21 11:26:07', '2021/04/20 19:55:48', 0, 'rakshith@coact.co.in', 0, 'ea748dd15297f8b6b537c46c539216a9'),
(27, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:12:17', '2021/04/22 07:51:06', '2021/04/20 19:56:41', 0, 'akshat@coact.co.in', 0, '685c7584dd809d1c8a413606b37b97cb'),
(28, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/20 19:57:25', '2021/04/20 19:27:24', '2021/04/20 19:57:25', 0, 'akshat1@coact.co.in', 0, '6c893982d1b5e3c375ceb8cdebaeaf0b'),
(29, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:12:17', '2021/04/22 07:51:06', '2021/04/20 20:01:27', 0, 'akshat@coact.co.in', 0, 'c20a0b1b99ad29295543309030acfef3'),
(30, 0, 0, 1, 0, 'rakshith', 'shetty', '2021/04/21 08:24:40', '2021/04/21 08:25:40', '2021/04/20 20:44:48', 0, 'admin@coact.co.in', 0, 'f4c8399f4c4374e5989705690515882f'),
(31, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/20 20:57:22', '2021/04/20 20:58:22', '2021/04/20 20:57:22', 0, 'a@coact.co.in', 0, '3bdd8f7e25b3ed549f228f7b250c7e82'),
(32, 0, 0, 1, 0, 'rakshith', 'shetty', '2021/04/21 11:31:28', '2021/04/21 11:00:20', '2021/04/20 21:09:08', 0, 'rakshith1234@coact.co.in', 0, '6ec3a0cab44dfdb99b03dfa9c6d9a5b3'),
(33, 0, 0, 1, 0, 'SOMNATH', 'NAIK', '2021/04/20 21:14:58', '2021/04/20 19:48:03', '2021/04/20 21:14:58', 0, 'somunaik17500@coact.co.in', 0, 'f26d432ba0809c4f87b1f30a4a058470'),
(34, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '2021/04/20 21:18:08', '2021/04/20 21:19:08', '2021/04/20 21:18:08', 0, 'somuna1ik111750@coact.co.in', 0, '5b8e4f821a1847eec7bf45ea473dc225'),
(35, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/20 21:20:17', 1, 'akshatjharia@coact.co.in', 0, 'df77e101eadf6501c6cd2832a92ab478'),
(36, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/20 21:20:38', 1, 'akshatjharia@coact.co.in', 0, '9663dc716bc553cd68b8da52545effe0'),
(37, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/20 21:22:00', '2021/04/20 19:55:11', '2021/04/20 21:22:00', 0, 'ak@coact.co.in', 0, 'd433662cc952d7846efafe0b726a7bda'),
(38, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '2021/04/20 21:36:58', '2021/04/20 21:37:58', '2021/04/20 21:36:58', 0, 'somunaiik1750@coact.co.in', 0, '51a0a152377fedfeffd3fab2e45ad674'),
(39, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/20 21:51:33', 1, 'akshatjharia@coact.co.in', 0, '6974798988dd5ec026ffb276d173351c'),
(40, 0, 0, 0, 0, 'Sujatha ', 'Natesh ', '2021/04/22 08:59:15', '2021/04/22 09:00:15', '2021/04/20 21:58:53', 1, 'sujatha@coact.co.in', 0, '27e434c0e8e3c94afa39278a9f4de79d'),
(41, 0, 0, 1, 0, 'rakshith', 'shetty', '2021/04/21 11:26:50', '2021/04/21 10:23:44', '2021/04/20 22:59:45', 0, 'rakshith123@coact.co.in', 0, 'aefbc656896ceffda0cc3f0b5b2dccee'),
(42, 0, 0, 1, 0, 'Pranav', 'Patil', '2021/04/22 09:45:45', '2021/04/22 08:46:18', '2021/04/20 23:02:04', 1, 'Pranav.patil@siemens-healthineers.com', 0, '6a61ce504e7b2d7b64a5f957e5c5351a'),
(43, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '2021/04/22 09:38:47', '2021/04/22 09:39:19', '2021/04/20 23:11:49', 0, 'pooja@coact.co.in', 0, '6999829d95a650b9f4b4828ab2d19522'),
(44, 0, 0, 1, 0, 'Rahul', 'Shetty', '2021/04/21 08:24:40', '2021/04/21 08:25:40', '2021/04/21 08:24:40', 0, 'admin@coact.co.in', 0, '0d45cb59fb7f9425c145dfff994c81c4'),
(45, 0, 0, 1, 0, 'Rakshith', 'Shetty', '2021/04/21 08:26:09', '2021/04/21 06:59:11', '2021/04/21 08:26:09', 0, 'rakshithshetty32@coact.co.in', 0, 'c3da46fde00c2c15b2b6eeb3014b3b1b'),
(46, 0, 0, 1, 0, 'Sanket ', 'Kathe', '2021/04/22 09:46:20', '2021/04/22 08:19:51', '2021/04/21 09:05:18', 0, 'sanket.kathe@siemens-healthineers.com', 0, '94a6ee63c2746971092bffef0b14c61e'),
(47, 0, 0, 1, 0, 'Sanket ', 'Kathe', '2021/04/22 09:46:20', '2021/04/22 08:19:51', '2021/04/21 09:24:27', 0, 'sanket.kathe@siemens-healthineers.com', 0, '9cb480b92b9ce01bce365f95cb7ccd1d'),
(48, 0, 0, 1, 0, 'Sanket ', 'Kathe', '2021/04/22 09:46:20', '2021/04/22 08:19:51', '2021/04/21 09:25:45', 0, 'sanket.kathe@siemens-healthineers.com', 0, '594daf84741abff44bdbfd3b9a781963'),
(49, 0, 0, 1, 0, 'Viraj', 'Khot', '2021/06/17 14:58:16', '2021/06/17 14:59:16', '2021/04/21 09:28:35', 1, 'viraj@coact.co.in', 0, '1602fb3147376510f4717c0c95330949'),
(50, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '2021/04/22 09:38:47', '2021/04/22 09:39:19', '2021/04/21 09:38:54', 0, 'pooja@coact.co.in', 0, 'bc79cbe5574df1d746b58e3d4444ff13'),
(51, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 10:22:12', 1, 'akshatjharia@coact.co.in', 0, '6c3eb4ba268a6b8af8de950c57e6214a'),
(52, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:12:17', '2021/04/22 07:51:06', '2021/04/21 10:22:54', 0, 'akshat@coact.co.in', 0, 'ed87047665765fe55841ec3dbd0840ff'),
(53, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 10:28:32', 1, 'akshatjharia@coact.co.in', 0, '7650ada15c408f85e992dd71e88b3a34'),
(54, 0, 0, 0, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 10:44:24', 1, 'akshatjharia@coact.co.in', 0, '010b07c677d5609a213570cbb4ba0316'),
(55, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 10:49:06', 1, 'akshatjharia@coact.co.in', 0, 'f29516d16a290450505781c170e9d84c'),
(56, 1, 0, 0, 0, 'Rakshith', 'shetty', '2021/04/21 11:25:07', '2021/04/21 11:26:07', '2021/04/21 11:25:07', 0, 'rakshith@coact.co.in', 0, '734c4fb1a4cf60ecdb87af39ae5c228c'),
(57, 0, 0, 1, 0, 'rakshith', 'shetty', '2021/04/21 11:26:50', '2021/04/21 10:23:44', '2021/04/21 11:26:50', 0, 'rakshith123@coact.co.in', 0, '0e2d7cce9d4e19587a07879e8e315957'),
(58, 0, 0, 1, 0, 'rakshith', 'shetty', '2021/04/21 11:31:28', '2021/04/21 11:00:20', '2021/04/21 11:31:28', 0, 'rakshith1234@coact.co.in', 0, '97b3fdf18a68a104ab7a6df214f13997'),
(59, 0, 0, 1, 0, 'Rakshith', 'Shetty', '2021/04/21 11:34:40', '2021/04/21 10:33:18', '2021/04/21 11:34:40', 0, 'rakshith12345@coact.co.in', 0, 'fb798fa1d6344e9cad38d56c10ba9fdb'),
(60, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 12:08:24', 1, 'akshatjharia@coact.co.in', 0, '0d2bca9a9422780cc31530c8bea3229f'),
(61, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '2021/04/22 09:38:47', '2021/04/22 09:39:19', '2021/04/21 12:11:39', 0, 'pooja@coact.co.in', 0, '2395603331491079bc94410a3e678f14'),
(62, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:12:17', '2021/04/22 07:51:06', '2021/04/21 12:16:34', 0, 'akshat@coact.co.in', 0, 'e9b75a0b9108c57ebb1c5c54a2b032d3'),
(63, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/21 15:32:52', '2021/04/21 15:33:01', '2021/04/21 13:01:43', 0, 'akshatjharia1@coact.co.in', 0, 'b75a1dbd0603c5e13cf4d0bbfc9c0fff'),
(64, 0, 0, 1, 0, 'Aj', 'J', '2021/04/22 09:09:19', '2021/04/22 09:10:19', '2021/04/21 13:04:15', 1, 'aj@coact.co.in', 0, 'c7d7c135d25fc5fb5a494cad0f4b5019'),
(65, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '2021/04/22 09:38:47', '2021/04/22 09:39:19', '2021/04/21 13:17:56', 0, 'pooja@coact.co.in', 0, '298e11079f37c55457bde4bd2081dad5'),
(66, 0, 0, 1, 0, 'Viraj', 'Khot', '2021/06/17 14:58:16', '2021/06/17 14:59:16', '2021/04/21 14:51:02', 1, 'viraj@coact.co.in', 0, '7d940095833e99b952d9b140fc96e0f8'),
(67, 0, 0, 0, 0, 'Sujatha ', 'Natesh ', '2021/04/22 08:59:15', '2021/04/22 09:00:15', '2021/04/21 15:25:23', 1, 'sujatha@coact.co.in', 0, 'f2ca5f6018bd106bf07d03692447dcb4'),
(68, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 15:25:31', 1, 'akshatjharia@coact.co.in', 0, 'd49ebedb40da3b0c87bad607f9ac6798'),
(69, 0, 0, 1, 0, 'T', 'J', '2021/04/21 15:27:26', '2021/04/21 14:11:15', '2021/04/21 15:27:26', 0, 'tj@coact.co.in', 0, '5d15dd3ee1ecd08a8d8d99c777783052'),
(70, 0, 0, 1, 0, 'NIshanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/21 15:31:39', 0, 'nishanth@coact.co.in', 0, 'e2ea37ff785c243feb8b190893b93301'),
(71, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/21 15:32:52', '2021/04/21 15:33:01', '2021/04/21 15:32:52', 0, 'akshatjharia1@coact.co.in', 0, '35192e440be4b37b4e7eaffda50533be'),
(72, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 15:33:13', 1, 'akshatjharia@coact.co.in', 0, '626c2da3fef676bc0f0d3cb80e54cb34'),
(73, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 15:33:25', 1, 'akshatjharia@coact.co.in', 0, '864d8568b8dae8e5a27874929624f9b6'),
(74, 0, 0, 1, 0, 'Nishanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/21 15:35:00', 0, 'Nishanth@coact.co.in', 0, 'c16cb4d50ea0d0c0c124e270b1b4e72d'),
(75, 0, 0, 0, 0, 'Pawan ', 'Shilwant ', '2021/04/22 09:03:21', '2021/04/22 09:04:21', '2021/04/21 15:35:16', 1, 'pawan@coact.co.in', 0, '6f5301a1e3dd6a743aa3e1e6ca253af8'),
(76, 0, 0, 0, 0, 'NIshanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/21 15:35:54', 0, 'nishanth@coact.co.in', 0, '355bd82988cd65678ae183f5a29e046f'),
(77, 0, 1, 0, 0, 'Sujatha ', 'Natesh ', '2021/04/22 08:59:15', '2021/04/22 09:00:15', '2021/04/21 15:36:53', 1, 'sujatha@coact.co.in', 0, '84da757c8a889afc6de05eb262242a6d'),
(78, 0, 0, 0, 0, 'Nishanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/21 15:38:50', 0, 'nishanth@coact.co.in', 0, '1b6bc3208e9cfc151f1abbc2505010c2'),
(79, 0, 0, 0, 0, 'Sujatha ', 'Natesh', '2021/04/22 08:59:15', '2021/04/22 09:00:15', '2021/04/21 15:41:39', 1, 'sujatha@coact.co.in', 0, 'b93226244359ba0f5ea3abd7e7ae61c4'),
(80, 0, 1, 0, 0, 'Pawan ', 'Shilwant ', '2021/04/22 09:03:21', '2021/04/22 09:04:21', '2021/04/21 15:45:35', 1, 'pawan@coact.co.in', 0, '2484623aa56c3319c5ebeefd5c24edfc'),
(81, 0, 1, 0, 0, 'Pawan ', 'Shilwant ', '2021/04/22 09:03:21', '2021/04/22 09:04:21', '2021/04/21 15:47:04', 1, 'pawan@coact.co.in', 0, 'cbd6556b606d76b1a54394a949ce1ad5'),
(82, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '2021/04/22 09:38:47', '2021/04/22 09:39:19', '2021/04/21 15:50:15', 0, 'pooja@coact.co.in', 0, '6ba8c8dad909f8fc9859fd648383cf6d'),
(83, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '2021/04/22 09:03:21', '2021/04/22 09:04:21', '2021/04/21 15:52:43', 1, 'pawan@coact.co.in', 0, 'b646fd35ea99d7e3c75a8a680a842cfa'),
(84, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '2021/04/21 15:54:48', '2021/04/21 14:41:43', '2021/04/21 15:54:48', 0, 'test@coact.co.in', 0, 'cc70a30b32cf8b744dad92ae14cc6a41'),
(85, 0, 0, 1, 0, 'Viraj', 'Khot', '2021/06/17 14:58:16', '2021/06/17 14:59:16', '2021/04/21 16:16:01', 1, 'viraj@coact.co.in', 0, '29e94e2612a359c1944dd1442c94f1af'),
(86, 0, 0, 1, 0, 'Pranav', 'Patil', '2021/04/22 09:45:45', '2021/04/22 08:46:18', '2021/04/21 17:55:03', 1, 'pranav.patil@siemens-healthineers.com', 0, 'bc401c52e849d0810865eb7c42b8b86e'),
(87, 0, 0, 0, 0, 'Shreya', 'Tole', '2021/04/22 10:05:44', '2021/04/22 11:30:12', '2021/04/21 18:26:14', 0, 'shreya.tole@varian.com', 0, '0d94b6b3e26c0e82836c4d0eaca7ffed'),
(88, 0, 0, 1, 0, 'Sreekanth ', 'Vakada', '2021/04/22 09:59:27', '2021/04/22 11:30:15', '2021/04/21 18:34:09', 0, 'sreekanth.vakada@varian.com', 0, '75cca40bfd9ca19a592e33a8b7b1a052'),
(89, 0, 0, 1, 0, 'Anand Mohan', 'Singh', '2021/04/22 10:00:42', '2021/04/22 10:01:13', '2021/04/21 18:36:44', 0, 'Anand.Singh@varian.com', 0, '7a030ec12243dc94d8c4bbfd39a3ebcc'),
(90, 0, 0, 1, 0, 'Vaibhav', 'Mhatre', '2021/04/22 10:47:21', '2021/04/22 11:30:14', '2021/04/21 18:48:15', 0, 'vaibhav.mhatre@varian.com', 0, '437f3eeaea1b0aab2fa6860479d8e47f'),
(91, 1, 0, 0, 0, 'Poonam', 'Yadav', '2021/04/22 09:31:30', '2021/04/22 11:30:12', '2021/04/21 19:03:13', 0, 'poonam.yadav@siemens-healthineers.com', 0, '1ff259ddcbaba5b578718a3320a305ef'),
(92, 0, 0, 1, 0, 'Mani', 'V. S.', '2021/04/22 09:57:48', '2021/04/22 11:30:14', '2021/04/21 19:03:28', 0, 'vs.mani@siemens-healthineers.com', 0, '258527a6715ccf7795b1dea02cb5a42c'),
(93, 0, 0, 0, 1, 'Deepa', 'Nebhnani', '2021/04/22 10:31:58', '2021/04/22 10:00:59', '2021/04/21 19:05:12', 0, 'deepa.nebhnani@siemens-healthineers.com', 0, '2c9561e10b15652e457978e46f52253f'),
(94, 0, 0, 1, 0, 'alankar', 'angre', '2021/04/21 19:09:26', '2021/04/21 19:10:26', '2021/04/21 19:09:26', 0, 'alankar.angre@varian.com', 0, 'e415c5e0d6c26eed507b06145ade00e2'),
(95, 0, 0, 1, 0, 'Brijesh', 'Singh', '2021/04/22 09:53:56', '2021/04/22 10:00:57', '2021/04/21 19:11:34', 1, 'Brijesh-singh@siemens-healthineers.com', 0, 'c60195647a9f655d7a22b32c723e6ac8'),
(96, 0, 0, 0, 0, 'Narasimha Murthy', 'MV', '2021/04/22 10:02:30', '2021/04/22 11:05:11', '2021/04/21 19:11:49', 0, 'narasimha.mv@siemens-healthineers.com', 0, '0379eef899e35ad891622adcfea4ee67'),
(97, 0, 0, 1, 0, 'Prashant ', 'Kadkade', '2021/04/22 10:00:41', '2021/04/22 11:30:15', '2021/04/21 19:13:45', 0, 'prashant.kadkade@siemens-healthineers.com', 0, 'ea8ddf07bb1bcb074c99a1aebfad6198'),
(98, 0, 0, 1, 0, 'Raghavan ', 'Narasimhan ', '2021/04/22 09:57:11', '2021/04/22 10:27:16', '2021/04/21 19:17:17', 0, 'Narasimhan.raghavan@varian.com', 0, '79e5551e47942190871bc0670b0dc2ac'),
(99, 0, 0, 1, 0, 'Dinesh ', 'B', '2021/04/22 11:07:51', '2021/04/22 10:00:51', '2021/04/21 19:18:37', 0, 'dinesh.balasundarababu@siemens-healthineers.com', 0, 'fb94c77709df32b121a7e3c1bb6e38a6'),
(100, 0, 0, 1, 0, 'Sambhav', 'Jain', '2021/04/21 19:23:59', '2021/04/21 19:24:59', '2021/04/21 19:23:59', 0, 'sambhav.jain@siemens-healthineers.com', 0, 'ff1f2489b3770c17586972be232f178b'),
(101, 0, 0, 0, 0, 'Surinder', 'Singh', '2021/04/22 10:00:56', '2021/04/22 10:00:56', '2021/04/21 19:28:57', 0, 'surinder.singh@siemens-healthineers.com', 0, '9421608e87e426f06f3f34daec5210ce'),
(102, 0, 0, 0, 0, 'Aditya', 'Rao', '2021/04/22 09:59:58', '2021/04/22 11:30:10', '2021/04/21 19:32:45', 0, 'aditya.rao@siemens-healthineers.com', 0, 'b29af1dd2a7c8cf6b8888d14b61a4cbc'),
(103, 0, 0, 0, 0, 'subrata', 'patnaik', '2021/04/22 10:49:50', '2021/04/22 10:01:20', '2021/04/21 19:35:21', 0, 'subrata.patnaik@siemens-healthineers.com', 0, 'e30f443105e39000539c2a9e4cde7012'),
(104, 0, 0, 1, 0, 'Mohammed Saiful', 'Islam', '2021/04/22 10:05:31', '2021/04/22 10:03:32', '2021/04/21 20:07:24', 0, 'Mohammed.saiful@siemens-healthineers.com', 0, 'f20104a3f00ee531e4d4b7b15ed73393'),
(105, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/21 20:08:03', 1, 'akshatjharia@coact.co.in', 0, '2b693fc9645c5d55dd139cd288215629'),
(106, 0, 0, 1, 0, 'Jamshed', 'Khan', '2021/04/22 10:03:15', '2021/04/22 10:26:43', '2021/04/21 20:20:50', 0, 'jamshed-hayat.khan@siemens-healthineers.com', 0, '95fb66435ef470921e891c62ead4f6dd'),
(107, 0, 0, 1, 0, 'Chinmay', 'Desai', '2021/04/22 10:45:20', '2021/04/22 09:27:20', '2021/04/21 20:25:49', 0, 'desai.chinmay@siemens-healthineers.com', 0, '091252f2f0ecb96ae50674ecb444bd43'),
(108, 0, 0, 1, 0, 'Rajeev ', ' Gupta', '2021/04/21 20:35:29', '2021/04/21 20:36:29', '2021/04/21 20:35:29', 0, 'gupta.rajeev@siemens-healthineers.com', 0, '6106f4658521dd1e25c1c8feca401c79'),
(109, 0, 0, 1, 0, 'Murugesapandi', 'KS', '2021/04/22 10:00:30', '2021/04/22 11:30:15', '2021/04/21 20:37:46', 0, 'murugesapandi.ks@siemens-healthineers.com', 0, 'ce48cb8a30199d7853a08a793544e903'),
(110, 0, 0, 1, 0, 'subrata', 'patnaik', '2021/04/22 10:49:50', '2021/04/22 10:01:20', '2021/04/21 20:57:09', 0, 'subrata.patnaik@siemens-healthineers.com', 0, '7dffbf2e5b843b39f404f67a5a2e048a'),
(111, 0, 0, 1, 0, 'HANUMANTH', 'PRASAD', '2021/04/22 09:43:56', '2021/04/22 11:30:22', '2021/04/21 21:11:28', 0, 'hanumanthprasad.k@varian.com', 0, 'd3a0e8c65de641ff7e56fbd5dcaca086'),
(112, 0, 0, 1, 0, 'Ganesh', 'Sawangikar', '2021/04/22 09:59:21', '2021/04/22 10:01:22', '2021/04/21 21:21:58', 0, 'ganesh.sawangikar@varian.com', 0, '37b6b7057ac19e72dfa2fda16b7fc733'),
(113, 0, 0, 1, 0, 'Ashutosh ', 'Singh', '2021/04/22 10:01:33', '2021/04/22 11:30:21', '2021/04/21 22:23:27', 0, 'ashutoshsingh@siemens-healthineers.com', 0, '8500ec6c906f7a9dd40163a1b0f12fb8'),
(114, 0, 0, 1, 0, 'Sanjeev ', 'Kumar', '2021/04/22 10:05:22', '2021/04/22 10:11:22', '2021/04/21 22:50:38', 0, 'sanjeev.kumar1@siemens-healthineers.com', 0, '0f05a77b3a48270a2bf2e2794c31d9cf'),
(115, 0, 0, 1, 0, 'Keyur', 'Bavishi', '2021/04/22 10:09:48', '2021/04/22 09:54:50', '2021/04/21 23:02:03', 0, 'keyur.bavishi@siemens-healthineers.com', 0, '9e7470f4eeb34b493576c3d1c88dc1fa'),
(116, 0, 0, 1, 0, 'Mahesh', 'Jejurkar', '2021/04/22 10:06:34', '2021/04/22 10:02:56', '2021/04/21 23:49:54', 0, 'mahesh.jejurkar@varian.com', 0, '1d37b029e45f66081f735771362b7ff1'),
(117, 0, 0, 1, 0, 'Ankur', 'Kapoor', '2021/04/22 10:01:05', '2021/04/22 11:30:16', '2021/04/22 07:39:53', 0, 'kapoor.ankur@siemens-healthineers.com', 0, '63fc9ef801f2fb012a6fd8db9901ec7a'),
(118, 0, 0, 1, 0, 'Himank', 'Kalra', '2021/04/22 09:52:26', '2021/04/22 10:00:03', '2021/04/22 07:52:33', 0, 'himank.kalra@varian.com', 0, '562fad36dfcfbb82c7cfda34a9301392'),
(119, 0, 1, 0, 0, 'Sharon', 'Fernandes', '2021/04/22 09:57:40', '2021/04/22 11:30:10', '2021/04/22 08:07:09', 0, 'sharon.fernandes@varian.com', 0, '25d03e867efacf3ad4cf31b4ae9da595'),
(120, 0, 0, 1, 0, 'Kaushik', 'Mazumdar', '2021/04/22 08:30:23', '2021/04/22 08:31:23', '2021/04/22 08:30:23', 1, 'Kaushik.Mazumdar@varian.com', 0, '87f76494ecb7c858420c4ed673a3296d'),
(121, 0, 0, 1, 0, 'Ashutosh ', 'Srivastav', '2021/04/22 08:35:06', '2021/04/22 08:36:06', '2021/04/22 08:35:06', 1, 'ashutosh.srivastav@siemens-healthineers.com', 0, 'cb7e62c4e2bd538078a326635d5fce22'),
(122, 0, 0, 0, 0, 'Giridharan', 'Iyer', '2021/04/22 08:41:18', '2021/04/22 08:42:18', '2021/04/22 08:41:18', 1, 'giridharan.iyer@varian.com', 0, '392af38707e67dba1f409d4346de8e8d'),
(123, 1, 0, 0, 0, 'Anita', 'Shanbhag', '2021/04/22 08:41:48', '2021/04/22 08:42:48', '2021/04/22 08:41:48', 1, 'anita.shanbhag@siemens-healthineers.com', 0, 'f7a7a33e3f806ad398f1738826a05d0e'),
(124, 0, 0, 0, 0, 'Sujatha ', 'Natesh ', '2021/04/22 08:59:15', '2021/04/22 09:00:15', '2021/04/22 08:59:15', 1, 'sujatha@coact.co.in', 0, '4dc6dd93b5bcbf36725f7863ac705e74'),
(125, 0, 0, 1, 0, 'Md Hafizur Rahman', 'Khan', '2021/04/22 08:59:48', '2021/04/22 09:00:48', '2021/04/22 08:59:48', 1, 'hafizur.rahman@siemens-healthineers.com', 0, '6349e5f3d5525e0d1254634bfbc005b2'),
(126, 0, 0, 0, 0, 'Pawan ', 'Shilwant ', '2021/04/22 09:03:21', '2021/04/22 09:04:21', '2021/04/22 09:03:21', 1, 'pawan@coact.co.in', 0, '47bae5576aaa8c35535669c08c9e8acf'),
(127, 0, 0, 1, 0, 'Vishal', 'Suri', '2021/04/22 09:06:03', '2021/04/22 09:07:03', '2021/04/22 09:06:03', 1, 'vishal.suri@siemens-healthineers.com', 0, '4bab47b1e99482de80481b9bd9564e76'),
(128, 0, 0, 1, 0, 'Umang Bharat', 'Shah', '2021/04/22 09:06:29', '2021/04/22 09:07:29', '2021/04/22 09:06:29', 1, 'umang.shah@siemens-healthineers.com', 0, '45bb961612a4aab8c7d593bc1038a593'),
(129, 0, 0, 0, 1, 'A', 'H', '2021/04/22 09:09:19', '2021/04/22 09:10:19', '2021/04/22 09:09:19', 1, 'aj@coact.co.in', 0, 'f3a741d792938e037fb1654c88e3c687'),
(130, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:12:17', '2021/04/22 07:51:06', '2021/04/22 09:12:17', 0, 'akshat@coact.co.in', 0, '9fd8966286443801fd30a4c58952edba'),
(131, 0, 1, 0, 0, 'Bhawna ', 'Ahuja', '2021/04/22 09:16:16', '2021/04/22 10:01:13', '2021/04/22 09:16:16', 1, 'bhawna.ahuja@siemens-healthineers.com', 0, 'b212ca288857873b56d36d6c2a62eda8'),
(132, 0, 0, 1, 0, 'AJ', 'J', '2021/04/22 09:18:19', '2021/04/22 09:19:19', '2021/04/22 09:18:19', 1, 'Aj1@coact.co.in', 0, '24d65de377d27e6f0f8e1f3b5e53f338'),
(133, 0, 0, 1, 0, 'Sreekanth ', 'Vakada', '2021/04/22 09:59:27', '2021/04/22 11:30:15', '2021/04/22 09:21:44', 0, 'sreekanth.vakada@varian.com', 0, 'c4cd71850048beba388a2774902bac5a'),
(134, 0, 0, 0, 0, 'Md. Jakir', 'Pk', '2021/04/22 09:22:00', '2021/04/22 09:23:00', '2021/04/22 09:22:00', 1, 'jakir.hossain@siemens-healthineers.com', 0, '416442e5122332c4df9a12c150bf9fd7'),
(135, 0, 0, 0, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/22 09:22:36', 1, 'akshatjharia@coact.co.in', 0, 'ed63566062f3fc8b79d8fa773189dce8'),
(136, 0, 0, 1, 0, 'Sunil Kumar ', 'Garg', '2021/04/22 09:23:24', '2021/04/22 08:32:24', '2021/04/22 09:23:24', 1, 'sunil.garg@siemens-healthineers.com', 0, '98910ee9a30ab92a943848a0775a5961'),
(137, 1, 0, 0, 0, 'neeraj', 'reddy', '2021/08/05 08:40:38', '2021/08/05 08:42:39', '2021/04/22 09:23:45', 1, 'neeraj@coact.co.in', 0, '0d04e566349f25f9a9c1490380ea2033'),
(138, 0, 0, 1, 0, 'Mahi', 'G', '2021/04/22 09:24:37', '2021/04/22 08:10:39', '2021/04/22 09:24:37', 0, 'mahi@coact.co.in', 0, 'd6f61c0c7c67ba271c188be5d32fc716'),
(139, 0, 0, 1, 0, 'Golam', 'Mostafa', '2021/04/22 09:24:44', '2021/04/22 10:01:15', '2021/04/22 09:24:44', 1, 'golam.mostafa@siemens-healthineers.com', 0, '7d3db256afa27c4075c459b6b1194942'),
(140, 0, 0, 1, 0, 'Narasimha Prasad', 'Jonnavittula', '2021/04/22 09:54:12', '2021/04/22 10:01:12', '2021/04/22 09:24:46', 1, 'prasad.jn@varian.com', 0, 'e71db0728ea3854d4c91f97ae78efa25'),
(141, 0, 0, 0, 1, 'Sujatha ', 'Natesh', '2021/04/22 09:29:18', '2021/04/22 09:29:43', '2021/04/22 09:29:18', 0, 'anushree@coact.co.in', 0, '9a71c62b133b1af98d87aa1e9f58c64f'),
(142, 0, 0, 0, 0, 'Md.', 'Tohidujjaman ', '2021/04/22 09:53:48', '2021/04/22 09:29:28', '2021/04/22 09:29:21', 1, 'md.tohidujjaman@siemens-healthineers.com', 0, 'e38d47ced818ef2457c20b9df7877daf'),
(143, 0, 0, 1, 0, 'Sultan ', 'Hasan', '2021/04/22 09:31:01', '2021/04/22 09:32:01', '2021/04/22 09:31:01', 1, 'sultan.hasan@siemens-healthineers.com', 0, '3752c753186379004d231aee7921ace1'),
(144, 0, 1, 0, 0, 'Poonam', 'Yadav', '2021/04/22 09:31:30', '2021/04/22 11:30:12', '2021/04/22 09:31:30', 0, 'poonam.yadav@siemens-healthineers.com', 0, 'fde56b2bf899e06f9be280364dbfdab5'),
(145, 0, 0, 1, 0, 'Md. Ala', 'Uddin', '2021/04/22 09:34:27', '2021/04/22 11:30:18', '2021/04/22 09:34:27', 0, 'ala.uddin@siemens-healthineers.com', 0, '0e8c21740355b7055ae6022f0bfda0eb'),
(146, 0, 0, 1, 0, 'Nishanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/22 09:34:33', 0, 'Nishanth@coact.co.in', 0, 'd6c3b081a9816425ccdc9b00643a5fa7'),
(147, 0, 0, 1, 0, 'jay', 'nema', '2021/04/22 09:35:10', '2021/04/22 11:13:09', '2021/04/22 09:35:10', 1, 'jay.nema@siemens-healthineers.com', 0, 'a347d55b9855d0be47da1cb7b64c311e'),
(148, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/22 09:38:29', 1, 'akshatjharia@coact.co.in', 0, 'f931174ea5f5fbc36312ffa5787015d9'),
(149, 0, 0, 1, 0, 'PAWAN', 'SHILWANT', '2021/04/22 09:38:47', '2021/04/22 09:39:19', '2021/04/22 09:38:47', 0, 'pooja@coact.co.in', 0, '51ad7bfd4c35ee40057e5e4caf58de39'),
(150, 0, 1, 0, 0, 'Shruti', 'Sharma', '2021/04/22 09:39:31', '2021/04/22 10:01:02', '2021/04/22 09:39:31', 1, 'sharma.shruti@siemens-healthineers.com', 0, '6b9950e4981ba8f69b0f4398cb415e7e'),
(151, 0, 0, 1, 0, 'Nishanth', 'Nishanth', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/22 09:39:55', 0, 'nishanth@coact.co.in', 0, 'e076a24f0a0d3fbd828c629a62fbfb5c'),
(152, 1, 0, 0, 0, 'YOGINI', 'HAVARE', '2021/04/22 09:41:28', '2021/04/22 09:41:48', '2021/04/22 09:41:28', 0, 'yogini.havare@varian.com', 0, 'ce20919e72eb1738fe3204dea73b3a23'),
(153, 0, 0, 0, 0, 'Tanmay', 'Pandey', '2021/04/22 10:00:57', '2021/04/22 10:01:34', '2021/04/22 09:43:24', 0, 'tanmay.pandey@varian.com', 0, 'a7d38cced46f35abd22247ccdd507548'),
(154, 0, 0, 1, 0, 'Abu Saleh', 'Md Musa', '2021/04/22 09:43:33', '2021/04/22 10:31:35', '2021/04/22 09:43:33', 1, 'abu.musa@siemens-healthineers.com', 0, '7edccc368e29b951059b0932e53c28bd'),
(155, 0, 0, 1, 0, 'Shamimul Haque ', 'Tanjil', '2021/04/22 09:47:06', '2021/04/22 10:03:22', '2021/04/22 09:43:42', 1, 'shamimul.tanjil@siemens-healthineers.com', 0, '50d42137f47d8e54cd9af40e9dbaf285'),
(156, 0, 0, 1, 0, 'HANUMANTH', 'PRASAD', '2021/04/22 09:43:56', '2021/04/22 11:30:22', '2021/04/22 09:43:56', 0, 'hanumanthprasad.k@varian.com', 0, '63321d1fb5f75f08c4ee8679e21fa92d'),
(157, 0, 0, 1, 0, 'Akshat', 'Jharia', '2021/04/22 09:44:08', '2021/04/22 11:18:10', '2021/04/22 09:44:08', 1, 'akshatjharia@coact.co.in', 0, '0b79e63ae34179aa4bbbdd1af4351da8'),
(158, 0, 0, 1, 0, 'Pawan', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/22 09:45:00', 0, 'nishanth@coact.co.in', 0, '3bc63a8a3a70895246404029669c3e61'),
(159, 0, 0, 1, 0, 'Abhishek', 'Mahajan', '2021/04/22 09:45:15', '2021/04/22 09:46:15', '2021/04/22 09:45:15', 1, 'abhishek.mahajan@varian.com', 0, '1fbfbfef3319386c2545484608e5ae87'),
(160, 0, 0, 1, 0, 'Taposundar', 'Majumdar', '2021/04/22 09:45:37', '2021/04/22 11:30:21', '2021/04/22 09:45:37', 0, 'taposundar.majumdar@varian.com', 0, '1936a39f1addc8e1e223b21fa191f399'),
(161, 0, 0, 0, 0, 'Nishanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/22 09:45:37', 0, 'nishanth@coact.co.in', 0, 'b32d7262a48a1eff0dface9d738a0a87'),
(162, 0, 0, 1, 0, 'Pranav', 'Patil', '2021/04/22 09:45:45', '2021/04/22 08:46:18', '2021/04/22 09:45:45', 1, 'Pranav.patil@siemens-healthineers.com', 0, '5bc0f20b569869e48160939b8e0ec617'),
(163, 0, 0, 1, 0, 'Prafful ', 'Sorani', '2021/04/22 09:46:09', '2021/04/22 10:01:44', '2021/04/22 09:46:09', 0, 'prafful.sorani@varian.com', 0, '7ca7b8dce6abc821fc7d7e9049f9213c'),
(164, 0, 0, 1, 0, 'Sanket', 'Kathe', '2021/04/22 09:46:20', '2021/04/22 08:19:51', '2021/04/22 09:46:20', 0, 'sanket.kathe@siemens-healthineers.com', 0, '770deae8908945eb917d849320d9813a'),
(165, 0, 0, 1, 0, 'Nishanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/22 09:46:29', 0, 'Nishanth@coact.co.in', 0, 'b6a3d2fb67418ae4dae0a055e2e51f58'),
(166, 0, 0, 1, 0, 'Iyer Krishnan', 'Seshan', '2021/04/22 09:46:36', '2021/04/22 10:01:06', '2021/04/22 09:46:36', 1, 's.krishnan@siemens-healthineers.com', 0, 'a5c13b5176fd802cf212f44af759f060'),
(167, 0, 0, 1, 0, 'Purushottam', 'Awasthi', '2021/04/22 10:26:10', '2021/04/22 10:01:11', '2021/04/22 09:46:49', 0, 'purushottam.awasthi@varian.com', 0, 'c72fec2ffa846ba11af11a51cdddf7f6'),
(168, 0, 0, 1, 0, 'avinash', 'kamble', '2021/04/22 09:47:02', '2021/04/22 10:01:02', '2021/04/22 09:47:02', 1, 'avinash.kamble@varian.com', 0, 'f9a19a5767c8cd1ed5856cd7d58b4212'),
(169, 0, 0, 1, 0, 'Shamimul haque', 'Tanjil', '2021/04/22 09:47:06', '2021/04/22 10:03:22', '2021/04/22 09:47:06', 1, 'shamimul.tanjil@siemens-healthineers.com', 0, 'a4be37237e132df44c5a395b6920a8eb'),
(170, 0, 0, 1, 0, 'DHEEPARAJ', 'N', '2021/04/22 09:47:32', '2021/04/22 10:01:02', '2021/04/22 09:47:32', 1, 'dheeparaj.n@siemens-healthineers.com', 0, 'be5fbd486e48b372b34a908c5856503e'),
(171, 0, 0, 1, 0, 'Raju Francis', 'Alookkaran', '2021/04/22 09:47:53', '2021/04/22 11:30:16', '2021/04/22 09:47:53', 0, 'raju.francis@varian.com', 0, '63efa897961f9512f61c0897af339e06'),
(172, 0, 0, 1, 0, 'Amil ', 'Shah ', '2021/04/22 10:17:31', '2021/04/22 10:07:11', '2021/04/22 09:48:17', 0, 'shah.amil@siemens-healthineers.com', 0, 'f75ab109a30b43784fe61d525fc70051'),
(173, 1, 0, 0, 0, 'ABHIK', 'GHOSE', '2021/04/22 09:48:44', '2021/04/22 10:01:14', '2021/04/22 09:48:44', 1, 'Abhik.Ghose@varian.com', 0, '49798781120d615131eb579a936bbd78'),
(174, 0, 0, 1, 0, 'Syed Siraj Us-', 'Salekin', '2021/04/22 09:49:27', '2021/04/22 10:08:58', '2021/04/22 09:49:27', 1, 'siraj.salekin@siemens-healthineers.com', 0, '9dfb74e701e344da13ceabdc42dc5d44'),
(175, 0, 0, 1, 0, 'Vineet', 'Gupta', '2021/04/22 09:49:39', '2021/04/22 11:30:16', '2021/04/22 09:49:39', 0, 'vineetm.gupta@varian.com', 0, '3f7bac74b69ace04ca90e4d965cc1c62'),
(176, 0, 0, 1, 0, 'Arun', 'Kumar', '2021/04/22 10:00:05', '2021/04/22 10:01:03', '2021/04/22 09:49:56', 0, 'arun.kumar@varian.com', 0, '153eee6c0708d5faa0da6d551a4bd7a4'),
(177, 0, 0, 1, 0, 'Sanjay ', 'Nayak', '2021/04/22 09:50:02', '2021/04/22 11:27:02', '2021/04/22 09:50:02', 0, 'sanjay.nayak@siemens-healthineers.com', 0, 'd262cfec482f275046b4f48ef3bb8db6'),
(178, 1, 0, 0, 0, 'Sharmila', 'Mulky', '2021/04/22 09:50:54', '2021/04/22 11:30:16', '2021/04/22 09:50:54', 0, 'sharmila.mulky@siemens-healthineers.com', 0, '4e34a03f24aac6908e7ffe91ec2c1c3b'),
(179, 0, 1, 0, 0, 'Shyamili', 'Suvarna', '2021/04/22 09:51:00', '2021/04/22 11:30:16', '2021/04/22 09:51:00', 0, 'shyamili.suvarna@siemens-healthineers.com', 0, '145d33807518a4d701c3dbd613b152c0'),
(180, 0, 0, 0, 0, 'Sanjeev', 'Krishnan Thampi', '2021/04/22 09:51:06', '2021/04/22 10:01:06', '2021/04/22 09:51:06', 1, 'sanjeev.k@siemens-healthineers.com', 0, '69f42a50a3ee2af80c137dda6ca431d5'),
(181, 0, 0, 1, 0, 'Hassan ', 'Mohamed', '2021/04/22 09:51:23', '2021/04/22 09:43:23', '2021/04/22 09:51:23', 1, 'hassan.mohamed@siemens-healthineers.com', 0, '0eda8cfb3b6c4cf51e5b515db85a1eff'),
(182, 1, 0, 0, 0, 'Sareeta', 'Mendonca', '2021/04/22 09:52:02', '2021/04/22 13:20:33', '2021/04/22 09:52:02', 1, 'sareeta.mendonca@siemens-healthineers.com', 0, 'b26d73d2b164b6327f33b7b1a228577b'),
(183, 0, 0, 1, 0, 'AMIT', 'TANDON', '2021/04/22 09:52:23', '2021/04/22 11:47:22', '2021/04/22 09:52:23', 0, 'amit.tandon@varian.com', 0, 'b685f95dc3c8c7b48c33dae2a628811a'),
(184, 0, 1, 0, 0, 'Shalini', 'Kumari', '2021/04/22 09:52:24', '2021/04/22 10:04:26', '2021/04/22 09:52:24', 1, 'shalini.kumari@siemens-healthineers.com', 0, '6442f9c3f5f598cd4e535c08503041dd'),
(185, 0, 0, 1, 0, 'Himank', 'Kalra', '2021/04/22 09:52:26', '2021/04/22 10:00:03', '2021/04/22 09:52:26', 0, 'himank.kalra@varian.com', 0, '265fd2255d2ab0a0a9ff60d222df3b85'),
(186, 0, 0, 1, 0, 'karthik', 'guruswamy', '2021/04/22 09:52:59', '2021/04/22 10:02:30', '2021/04/22 09:52:59', 1, 'karthikkumar.guruswamy@varian.com', 0, '9b7c76485dd1dcd1026c52cf4cc7e851'),
(187, 0, 1, 0, 0, 'Preeti', 'Goel', '2021/04/22 09:53:11', '2021/04/22 10:01:08', '2021/04/22 09:53:11', 1, 'preeti.goel@siemens-healthineers.com', 0, '1843832d852302cc35da73980ceb56c3'),
(188, 0, 0, 1, 0, 'Anilkumar', 'a', '2021/04/22 09:53:47', '2021/04/22 11:30:19', '2021/04/22 09:53:47', 0, 'kumar.anil@siemens-healthineers.com', 0, 'ffebff05044717ffab8fa7479153bbc8'),
(189, 0, 0, 1, 0, 'Md.', 'Tohidujjaman ', '2021/04/22 09:53:48', '2021/04/22 09:29:28', '2021/04/22 09:53:48', 1, 'md.tohidujjaman@siemens-healthineers.com', 0, '4ae4436636755345b857ca87b2144120'),
(190, 0, 0, 1, 0, 'Abhinav', 'Kaushik', '2021/04/22 09:53:49', '2021/04/22 10:00:50', '2021/04/22 09:53:49', 1, 'abhinav.kaushik@siemens-healthineers.com', 0, 'e5b8b6666455bbfb72556de98c5c69a6'),
(191, 0, 0, 1, 0, 'BRIJESH', 'SINGH', '2021/04/22 09:53:56', '2021/04/22 10:00:57', '2021/04/22 09:53:56', 1, 'brijesh-singh@siemens-healthineers.com', 0, 'b68ed99064b96352ec6c0e1f6e8630a7'),
(192, 0, 0, 0, 0, 'Rajiv ', 'Dubey', '2021/04/22 09:54:08', '2021/04/22 11:07:18', '2021/04/22 09:54:08', 0, 'kumar.dubey@siemens-healthineers.com', 0, '8f3efc95a813ee0d1c57b6e92a764fed'),
(193, 0, 0, 0, 0, 'Narasimha', 'Jonnavittula', '2021/04/22 09:54:12', '2021/04/22 10:01:12', '2021/04/22 09:54:12', 1, 'prasad.jn@varian.com', 0, '6f340d040cd9f4fb2f2597f7416e3781'),
(194, 1, 0, 0, 0, 'Kim', 'Dsouza', '2021/04/22 09:54:16', '2021/04/22 11:30:17', '2021/04/22 09:54:16', 0, 'kim.dsouza@siemens-healthineers.com', 0, '3ca157877d06bc859b01a845dcbf5cdb'),
(195, 0, 0, 1, 0, 'Krunal ', 'Nakrani', '2021/04/22 09:54:27', '2021/04/22 10:00:58', '2021/04/22 09:54:27', 1, 'krunal.nakrani@varian.com', 0, 'e8a21f36b25334dab4668b1e2c8b75b8'),
(196, 0, 0, 1, 0, 'BIJURAJ', 'BALAN', '2021/04/22 09:54:27', '2021/04/22 10:01:57', '2021/04/22 09:54:27', 1, 'bijuraj.balan@varian.com', 0, '67003c87c3dd4aea4610e25a27faf5dc'),
(197, 0, 0, 0, 0, 'Manoj', 'Prasad', '2021/04/22 09:54:29', '2021/04/22 10:01:08', '2021/04/22 09:54:29', 0, 'manoj.prasad@siemens-healthineers.com', 0, '089f5ca588caf214b269468abd227c7b'),
(198, 0, 0, 1, 0, 'Sanjay', 'Chaudhary', '2021/04/22 09:54:29', '2021/04/22 11:30:11', '2021/04/22 09:54:29', 0, 'sanjay.chaudhary@siemens-healthineers.com', 0, 'c9a1354534899a64c34880ed0ec8336e'),
(199, 0, 0, 1, 0, 'ANISH', 'PILLAI', '2021/04/22 09:54:33', '2021/04/22 11:30:20', '2021/04/22 09:54:33', 0, 'anish.pillai@varian.com', 0, '3db9e346f977c8c99b5542ba9977288f'),
(200, 1, 0, 0, 0, 'Dolly ', 'Jacob', '2021/04/22 09:54:35', '2021/04/22 11:30:18', '2021/04/22 09:54:35', 0, 'dolly.jacob@siemens-healthineers.com', 0, '94d5fb84de9f1904c87052e1b61fb23a'),
(201, 0, 0, 1, 0, 'Rachit', 'Bijalwan', '2021/04/22 09:54:39', '2021/04/22 10:00:39', '2021/04/22 09:54:39', 0, 'rachit.bijalwan@siemens-healthineers.com', 0, '1601c1f5cba654f991f9379bbb6f0b88'),
(202, 1, 0, 0, 0, 'Gino ', 'George', '2021/04/22 09:55:16', '2021/04/22 09:57:46', '2021/04/22 09:55:16', 0, 'gino.george@varian.com', 0, 'cd009cbc4d63624410a27a4e8310515c'),
(203, 0, 0, 1, 0, 'Rajesh', 'Pingulkar', '2021/04/22 09:55:23', '2021/04/22 11:30:21', '2021/04/22 09:55:23', 0, 'rajesh.pingulkar@varian.com', 0, 'a822d3a3eeef76ac636a756b5ae171b0'),
(204, 0, 0, 1, 0, 'Rajneesh', 'Handa', '2021/04/22 09:55:42', '2021/04/22 11:30:14', '2021/04/22 09:55:42', 0, 'rajneesh.handa@siemens-healthineers.com', 0, 'f3aaa832c4d081f7235c17f17d91c205'),
(205, 0, 1, 0, 0, 'suprita', 'iyer', '2021/04/22 09:55:49', '2021/04/22 11:30:20', '2021/04/22 09:55:49', 0, 'suprita.iyer@varian.com', 0, '6280dd883e05a23bc15bbaadda3e9106'),
(206, 0, 0, 1, 0, 'Bobin', 'Baby', '2021/04/22 09:56:00', '2021/04/22 10:15:31', '2021/04/22 09:56:00', 0, 'bobin.baby@varian.com', 0, 'b99c0705dd03001bfc24a573c8887899'),
(207, 0, 0, 0, 0, 'Beryl', 'Dcruz', '2021/04/22 09:56:06', '2021/04/22 11:30:27', '2021/04/22 09:56:06', 0, 'beryl.dcruz@siemens-healthineers.com', 0, 'ef4829a71ee709893481c9edcebe6800'),
(208, 0, 0, 1, 0, 'Sreekanth ', 'Vakada', '2021/04/22 09:59:27', '2021/04/22 11:30:15', '2021/04/22 09:56:35', 0, 'sreekanth.vakada@varian.com', 0, 'f381eea6eb52ae7848b3527c18593824'),
(209, 0, 0, 1, 0, 'DHANANJAY', 'MAINKAR', '2021/04/22 09:56:39', '2021/04/22 11:30:09', '2021/04/22 09:56:39', 0, 'dhananjay.mainkar@siemens-healthineers.com', 0, '3394125b9e79b10cca6847dd68b9d08a'),
(210, 0, 0, 1, 0, 'Ashish ', 'Bhaskar', '2021/04/22 09:56:43', '2021/04/22 10:01:13', '2021/04/22 09:56:43', 0, 'ashish.bhaskar@varian.com', 0, '10089548eb155f4ac3b934ed8d840f01'),
(211, 1, 0, 0, 0, 'Jayashree', 'Karawadikar', '2021/04/22 09:56:43', '2021/04/22 10:01:00', '2021/04/22 09:56:43', 0, 'jayashree.karawadikar@varian.com', 0, '03fffba0964ac3249e93cf8cdf12fa8c'),
(212, 0, 0, 1, 0, 'Akshay', 'Gondhalekar', '2021/04/22 09:56:44', '2021/04/22 11:18:21', '2021/04/22 09:56:44', 0, 'akshay.gondhalekar@siemens-healthineers.com', 0, '6fe5dac8787dadb5472e16116df26406'),
(213, 0, 0, 1, 0, 'Pradeep', 'Khanna', '2021/04/22 09:56:46', '2021/04/22 10:00:46', '2021/04/22 09:56:46', 0, 'pradeep.khanna@varian.com', 0, '0ee3804380d6c0a21d65dfa37a498213'),
(214, 0, 0, 1, 0, 'Trishanu', 'Mukherjee', '2021/04/22 09:56:56', '2021/04/22 11:30:11', '2021/04/22 09:56:56', 0, 'trishanu.mukherjee@varian.com', 0, '1d3accd98b0586ed82f135156fbb9eb6'),
(215, 0, 0, 1, 0, 'MK', 'RAJAGOPAL', '2021/04/22 09:57:06', '2021/04/24 03:00:09', '2021/04/22 09:57:06', 1, 'rajagopal.mk@siemens-healthineers.com', 0, '84d9a49c2a4f56aa52980b482c44f0f3'),
(216, 0, 0, 1, 0, 'Paresh', 'Rane', '2021/04/22 09:57:09', '2021/04/22 11:30:14', '2021/04/22 09:57:09', 0, 'paresh.rane@varian.com', 0, '8bfe25cae121ff79a3939a504192a601'),
(217, 1, 0, 0, 0, 'Richa', 'Gour', '2021/04/22 09:57:10', '2021/04/22 10:01:10', '2021/04/22 09:57:10', 0, 'richa.gour@siemens-healthineers.com', 0, 'dfdbd9b1c838e712c6067fec7c1a9e63'),
(218, 0, 0, 1, 0, 'Shabhin', 'Bhaskaran', '2021/04/22 09:57:10', '2021/04/22 10:01:05', '2021/04/22 09:57:10', 0, 'shabhin.bhaskaran@siemens-healthineers.com', 0, '412edf91f00222a65898bc0962be6999'),
(219, 0, 0, 0, 0, 'Raghavan ', 'Narasimhan', '2021/04/22 09:57:11', '2021/04/22 10:27:16', '2021/04/22 09:57:11', 0, 'Narasimhan.Raghavan@varian.com', 0, 'd453d9d5134f6cddbafed7d8c3e58125'),
(220, 0, 0, 1, 0, 'Joydeep', 'Bhattacharjee', '2021/04/22 09:57:12', '2021/04/22 11:30:13', '2021/04/22 09:57:12', 0, 'joydeep.bhattacharjee@siemens-healthineers.com', 0, 'd725a4227a458b0c097fe09be0583f8b'),
(221, 0, 0, 1, 0, 'Mohit', 'Basuk', '2021/04/22 09:57:14', '2021/04/22 11:30:17', '2021/04/22 09:57:14', 0, 'mohit.basuk@varian.com', 0, '3834bc79796f48b68b02d5962f2d94d7'),
(222, 0, 0, 1, 0, 'Sunil', 'jakhar', '2021/04/22 09:57:23', '2021/04/22 10:01:24', '2021/04/22 09:57:23', 0, 'sunil.jakhar@varian.com', 0, 'e7bdc4523bdc94f1b8e0e6c076bea636'),
(223, 0, 0, 1, 0, 'Arish', 'Ali', '2021/04/22 09:57:25', '2021/04/22 11:30:19', '2021/04/22 09:57:25', 0, 'arish.ali@siemens-healthineers.com', 0, '5a7126f03e70d078ddee74bc96c789c7'),
(224, 1, 0, 0, 0, 'Nihad', 'Sultana', '2021/04/22 09:57:37', '2021/04/22 11:30:16', '2021/04/22 09:57:37', 0, 'nihad.sultana@siemens-healthineers.com', 0, 'c84619d9476c14115cc91817d6976bce'),
(225, 0, 1, 0, 0, 'Sharon', 'Fernandes', '2021/04/22 09:57:40', '2021/04/22 11:30:10', '2021/04/22 09:57:40', 0, 'sharon.fernandes@varian.com', 0, '6254a45073c49d817578a7b4daac9401'),
(226, 0, 0, 1, 0, 'Sudipta ', 'Mukherjee ', '2021/04/22 09:57:41', '2021/04/22 09:04:11', '2021/04/22 09:57:41', 0, 'sudipta.mukherjee@siemens-healthineers.com', 0, '49fbd4b8fa0f19fe77b91b8fec8327ce'),
(227, 0, 0, 1, 0, 'Prasad', 'Kabbur', '2021/04/22 09:57:44', '2021/04/22 09:31:14', '2021/04/22 09:57:44', 0, 'prasad.kabbur@siemens-healthineers.com', 0, 'c691130bf5af42e8ebf8fe0ff2a3c683'),
(228, 0, 0, 1, 0, 'Mani', 'V. S.', '2021/04/22 09:57:48', '2021/04/22 11:30:14', '2021/04/22 09:57:48', 0, 'vs.mani@siemens-healthineers.com', 0, 'debb864d4d04f659129f5ddce2d16bf7'),
(229, 1, 0, 0, 0, 'Chaitali', 'Sahasrabuddhe', '2021/04/22 09:57:50', '2021/04/22 10:00:50', '2021/04/22 09:57:50', 0, 'chaitali.sahasrabuddhe@siemens-healthineers.com', 0, '0b5c927936df07d41d839d1d2e02db82'),
(230, 0, 0, 1, 0, 'Nishad', 'Dani', '2021/04/22 09:57:56', '2021/04/22 09:48:57', '2021/04/22 09:57:56', 0, 'nishad.dani@siemens-healthineers.com', 0, '6959ec734c09c5d450a04c5e1f91fa48'),
(231, 0, 0, 1, 0, 'SAIKAT', 'SARKAR', '2021/04/22 09:58:03', '2021/04/22 11:30:15', '2021/04/22 09:58:03', 0, 'saikat.sarkar@siemens-healthineers.com', 0, '40e0815e9f10eb9107b4c5525b94345a'),
(232, 0, 0, 1, 0, 'Vishal', 'Sharma', '2021/04/22 09:58:04', '2021/04/22 11:30:14', '2021/04/22 09:58:04', 0, 'vishal.sharma@siemens-healthineers.com', 0, '7dff1c1a93f6101179265f5ab87316e7'),
(233, 0, 0, 1, 0, 'Sudahar', 'Harikrishnaperumal', '2021/04/22 09:58:22', '2021/04/22 10:01:00', '2021/04/22 09:58:22', 0, 'sudahar.harikrishnaperumal@varian.com', 0, '39eaf0a3622416e37213ca4762d965b1'),
(234, 0, 0, 1, 0, 'tom', 'george', '2021/04/22 09:58:28', '2021/04/22 10:01:19', '2021/04/22 09:58:28', 0, 'tom.george@varian.com', 0, '464fe96dcf31d181778ec190d2c4f8aa'),
(235, 0, 0, 1, 0, 'Satish', 'Shankar', '2021/04/22 09:58:37', '2021/04/22 10:08:38', '2021/04/22 09:58:37', 0, 'satish.shankar@varian.com', 0, '30926450b54e3bb44b5c4bffd92e4cfc'),
(236, 0, 0, 0, 1, 'SUJATA', 'NIADU', '2021/04/22 10:00:23', '2021/04/22 11:30:13', '2021/04/22 09:58:38', 0, 'sujata.naidu@siemens-healthineers.com', 0, '901b590502f7f522dfa9e560f783cdd8'),
(237, 0, 0, 1, 0, 'Raghu', 'Chandrashekar', '2021/04/22 09:58:39', '2021/04/22 11:02:20', '2021/04/22 09:58:39', 0, 'raghu.chandrashekar@siemens-healthineers.com', 0, 'd7abd774dbd17938c371ad40e53c826a'),
(238, 1, 0, 0, 0, 'Logeshwari', 'B', '2021/04/22 09:58:44', '2021/04/22 10:01:44', '2021/04/22 09:58:44', 0, 'logeshwari.balachandran@siemens-healthineers.com', 0, '63e836c437bb2460af62df868b6956e6'),
(239, 0, 0, 1, 0, 'shankar', 'Haveri', '2021/04/22 09:58:49', '2021/04/22 11:30:16', '2021/04/22 09:58:49', 0, 'shankar.haveri@siemens-healthineers.com', 0, '2ab86a717e680c5ba79cfb3990b1b63f'),
(240, 1, 0, 0, 0, 'Suryakant', 'Rawat', '2021/04/22 09:58:52', '2021/04/22 10:02:28', '2021/04/22 09:58:52', 0, 'suryakant.rawat@varian.com', 0, 'fbd45720f3ffceefd89b19db19868296'),
(241, 0, 0, 1, 0, 'Md', 'Istiak', '2021/04/22 09:58:55', '2021/04/22 09:04:25', '2021/04/22 09:58:55', 0, 'istiak.nazmun@siemens-healthineers.com', 0, 'a74a2471fdb3dc9082bc2104f280bf3a'),
(242, 1, 0, 0, 0, 'Mamatha', 'Kotian', '2021/04/22 09:58:57', '2021/04/22 11:30:17', '2021/04/22 09:58:57', 0, 'mamatha.kotian@siemens-healthineers.com', 0, '1f32e45a571611b0d3a146f98190659a'),
(243, 0, 0, 1, 0, 'Ajith', 'Kumar', '2021/04/22 09:59:00', '2021/04/22 10:01:31', '2021/04/22 09:59:00', 0, 'ajith.kumar@siemens-healthineers.com', 0, 'c93a1dab2c97d64e460b08cb5281fbd4'),
(244, 0, 0, 0, 1, 'RAHUL', 'UMBARKAR', '2021/04/22 09:59:10', '2021/04/22 11:30:11', '2021/04/22 09:59:10', 0, 'rahul.umbarkar@varian.com', 0, '3dd7cb38ae8def502f8e3f362e2486b9'),
(245, 0, 0, 1, 0, 'Vidur', 'Saigal', '2021/04/22 09:59:15', '2021/04/22 10:01:16', '2021/04/22 09:59:15', 0, 'vidur.saigal@siemens-healthineers.com', 0, 'cb46fc165099bee03d5c1cd4dae62c00'),
(246, 0, 0, 1, 0, 'aruswamy', 'Karuppuswamy', '2021/04/22 09:59:18', '2021/04/22 10:00:18', '2021/04/22 09:59:18', 0, 'aruswamy.karuppuswamy@varian.com', 0, '38fdeb5277f300a25bb13366665c60fc'),
(247, 0, 0, 1, 0, 'Ganesh', 'Sawangikar', '2021/04/22 09:59:21', '2021/04/22 10:01:22', '2021/04/22 09:59:21', 0, 'ganesh.sawangikar@varian.com', 0, '5fdbec068996a219e33f10b6ebd5f6f3'),
(248, 0, 0, 1, 0, 'sanjeev ', 'bhatli', '2021/04/22 09:59:24', '2021/04/22 11:30:12', '2021/04/22 09:59:24', 0, 'sanjeev.bhatli@siemens-healthineers.com', 0, '8500add38efc425485c5b55cc54be339'),
(249, 0, 0, 1, 0, 'Sreekanth ', 'Vakada', '2021/04/22 09:59:27', '2021/04/22 11:30:15', '2021/04/22 09:59:27', 0, 'sreekanth.vakada@varian.com', 0, 'bd125bd657237c1590a1f1c87035e476'),
(250, 0, 1, 0, 0, 'Srimita', 'Saha', '2021/04/22 09:59:30', '2021/04/22 11:30:37', '2021/04/22 09:59:30', 0, 'Srimita.Saha@varian.com', 0, '7dd69d2b33f73a1a8be12c45d07ffea1'),
(251, 0, 0, 1, 0, 'Ashutosh', 'Sharma', '2021/04/22 09:59:36', '2021/04/22 11:30:20', '2021/04/22 09:59:36', 0, 'sharma.ashutosh@siemens-healthineers.com', 0, 'e04bf1093d26c000aedca5583a601a49'),
(252, 0, 0, 0, 0, 'Malti', 'Sachdev', '2021/04/22 09:59:36', '2021/04/22 10:53:30', '2021/04/22 09:59:36', 0, 'malti.sachdev@varian.com', 0, 'cb9e3e3cbf3f911deb7c866629c77a42'),
(253, 0, 0, 1, 0, 'Hrishikesh', 'Darshetkar', '2021/04/22 09:59:39', '2021/04/22 10:01:10', '2021/04/22 09:59:39', 0, 'hrishikesh.darshetkar@varian.com', 0, 'ef7bb379743a18bc0fd074f79fd2c61e'),
(254, 1, 0, 0, 0, 'Sujit', 'Debnath', '2021/04/22 09:59:41', '2021/04/22 10:01:11', '2021/04/22 09:59:41', 0, 'sujit.debnath@varian.com', 0, 'a0a879d5bf99a28c9a3264a00198fc74'),
(255, 0, 0, 0, 0, 'tinto', 'k anto', '2021/04/22 09:59:43', '2021/04/22 08:43:43', '2021/04/22 09:59:43', 0, 'tinto.anto@siemens-healthineers.com', 0, '289144eedcb4b3e2e49c9c501b392bd4'),
(256, 0, 0, 1, 0, 'Aswin', 'Vk', '2021/04/22 09:59:46', '2021/04/22 09:08:47', '2021/04/22 09:59:46', 0, 'aswin.vk@siemens-healthineers.com', 0, 'e630c4d53206e7cfaa3889a97842e7a7'),
(257, 0, 0, 1, 0, 'Rahul', 'Kamath', '2021/04/22 09:59:48', '2021/04/22 11:30:14', '2021/04/22 09:59:48', 0, 'rahul.kamath@siemens-healthineers.com', 0, '81e8454a6ba461b0e876676eda62af39'),
(258, 0, 0, 1, 0, 'Animesh', 'Tiwari', '2021/04/22 09:59:48', '2021/04/22 10:01:19', '2021/04/22 09:59:48', 0, 'tiwari.animesh@siemens-healthineers.com', 0, '4e131deb5805b34317cba71b87d7fe07'),
(259, 0, 0, 1, 0, 'Raj', 'Upadhyay', '2021/04/22 09:59:52', '2021/04/22 10:00:53', '2021/04/22 09:59:52', 0, 'raj.upadhyay@siemens-healthineers.com', 0, '00d3c8114aafe0dc8e23ebbe7b460f5d'),
(260, 0, 1, 0, 0, 'Eurekha', 'Mohan', '2021/04/22 09:59:53', '2021/04/22 19:58:06', '2021/04/22 09:59:53', 1, 'eurekha.m@varian.com', 0, '9920c2e442312837bc853592ebecd33c'),
(261, 0, 0, 0, 0, 'Varsha', 'Lodhi', '2021/04/22 09:59:56', '2021/04/22 11:30:13', '2021/04/22 09:59:56', 0, 'Varsha.lodhi@varian.com', 0, 'e4921dd0be51d428ed7b28727cef8a0d'),
(262, 0, 0, 1, 0, 'Aditya', 'Rao', '2021/04/22 09:59:58', '2021/04/22 11:30:10', '2021/04/22 09:59:58', 0, 'aditya.rao@siemens-healthineers.com', 0, '82c5bbf74e5af7349caec10ee2923780'),
(263, 0, 0, 1, 0, 'Brij', 'Sengar', '2021/04/22 10:00:00', '2021/04/22 15:23:31', '2021/04/22 10:00:00', 1, 'brij.sengar@siemens-healthineers.com', 0, 'f5eb51de723300a9f1b025a9a18e7274'),
(264, 0, 0, 1, 0, 'Arun', 'Kumar', '2021/04/22 10:00:05', '2021/04/22 10:01:03', '2021/04/22 10:00:05', 0, 'arun.kumar@varian.com', 0, 'ba2768082124827eb0c8fb1cff402a2b'),
(265, 0, 0, 1, 0, 'Dinesh Kumar', 'Singh', '2021/04/22 10:00:06', '2021/04/22 11:30:15', '2021/04/22 10:00:06', 0, 'dinesh.singh@siemens-healthineers.com', 0, 'bf0fec0ef3c1ae020e393dea4bf71365'),
(266, 0, 0, 0, 0, 'srinivas', 'veturi', '2021/04/22 10:00:10', '2021/04/22 10:48:10', '2021/04/22 10:00:10', 0, 'srinivas.veturi@varian.com', 0, 'cea3e3fdc689db83b4c552746daf7291'),
(267, 0, 0, 0, 0, 'Nitesh ', 'Chauhan', '2021/04/22 10:00:13', '2021/04/22 10:01:14', '2021/04/22 10:00:13', 0, 'nitesh.chauhan@varian.com', 0, '8ca1a51d5285fb9baff0dfb110ff3935'),
(268, 0, 0, 1, 0, 'Nikesh', 'Varughese', '2021/04/22 10:00:18', '2021/04/22 11:30:09', '2021/04/22 10:00:18', 0, 'nikesh.varughese@siemens-healthineers.com', 0, '26bb8a692145c0c404e0d780b884517c'),
(269, 0, 0, 0, 0, 'Mir Shahriar', 'Mahmud', '2021/04/22 10:00:22', '2021/04/22 10:00:53', '2021/04/22 10:00:22', 0, 'shahriar.mahmud@siemens-healthineers.com', 0, '85bc5f1510b98cbc89570549da853978'),
(270, 1, 0, 0, 0, 'Mahua Basu', 'Banerjee', '2021/04/22 10:00:23', '2021/04/22 10:01:23', '2021/04/22 10:00:23', 0, 'mahua.banerjee@siemens-healthineers.com', 0, '0c7fd9a9f34b0ab048d97f942a430499'),
(271, 0, 0, 1, 0, 'Hemant', 'Patil', '2021/04/22 10:00:23', '2021/04/22 10:00:53', '2021/04/22 10:00:23', 0, 'hemant.patil@varian.com', 0, 'd2db921d685b96e78d50623dc1d0f63f'),
(272, 0, 0, 0, 1, 'SUJATA', 'NAIDU', '2021/04/22 10:00:23', '2021/04/22 11:30:13', '2021/04/22 10:00:23', 0, 'sujata.naidu@siemens-healthineers.com', 0, '8834c3d9e89639329712e452c38a5d49'),
(273, 0, 1, 0, 0, 'Rhea', 'Bhandarkar', '2021/04/22 10:00:23', '2021/04/22 11:30:28', '2021/04/22 10:00:23', 0, 'rhea.bhandarkar@varian.com', 0, '654540d81f92e77ef9e78ad56836dc53'),
(274, 0, 0, 1, 0, 'Joe', 'Kanichai', '2021/04/22 10:00:23', '2021/04/22 10:00:54', '2021/04/22 10:00:23', 0, 'joe.kanichai@siemens-healthineers.com', 0, '14d96b758fdab17e9178ebc9a36a71b1'),
(275, 0, 0, 1, 0, 'Sudhir', 'KN', '2021/04/22 10:00:24', '2021/04/22 08:52:55', '2021/04/22 10:00:24', 0, 'kn.sudhir@siemens-healthineers.com', 0, 'ab319b8cda8284b0566742b5b9f4619a');
INSERT INTO `tbl_users` (`id`, `ms`, `miss`, `mr`, `dr`, `fname`, `lname`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `is_varified`, `password`) VALUES
(276, 0, 0, 1, 0, 'Dhiroj ', 'Barad ', '2021/04/22 10:00:24', '2021/04/22 10:00:55', '2021/04/22 10:00:24', 0, 'dhiroj.barad@siemens-healthineers.com', 0, 'baa87bc709a5a7dd62e62086f49325a3'),
(277, 1, 0, 0, 0, 'Sadhana', 'Sheth', '2021/04/22 10:00:26', '2021/04/22 11:30:14', '2021/04/22 10:00:26', 0, 'sadhana.sheth@siemens-healthineers.com', 0, 'd5485fd0df51aba1839863aa1f554e10'),
(278, 1, 0, 0, 0, 'MAITHILI', 'AMDEKAR', '2021/04/22 10:00:28', '2021/04/22 11:30:17', '2021/04/22 10:00:28', 0, 'maithili.amdekar@siemens-healthineers.com', 0, '7d48872d9ca879ac2325cb52700dffed'),
(279, 0, 0, 1, 0, 'devendra', 'khaladkar', '2021/04/22 10:00:28', '2021/04/22 10:55:57', '2021/04/22 10:00:28', 0, 'devendra.khaladkar@varian.com', 0, '580bdc524f1b0133be16ea361cbdddc0'),
(280, 0, 0, 1, 0, 'hariharan', 'Subramanian', '2021/04/22 10:00:30', '2021/04/22 08:55:01', '2021/04/22 10:00:30', 0, 'hariharan.ns@siemens-healthineers.com', 0, 'a78c80ccca876e59020aa836b42a98d8'),
(281, 0, 0, 1, 0, 'Murugesapandi ', 'KS', '2021/04/22 10:00:30', '2021/04/22 11:30:15', '2021/04/22 10:00:30', 0, 'murugesapandi.ks@siemens-healthineers.com', 0, 'dfd886d5c075372d543417c4fa5045fc'),
(282, 0, 0, 1, 0, 'rakesh', 'eitha', '2021/04/22 10:00:32', '2021/04/22 09:12:33', '2021/04/22 10:00:32', 0, 'rakesh.eitha@siemens-healthineers.com', 0, '56618a9c066a825b5c583204e40f3aa0'),
(283, 0, 0, 0, 0, 'Ganesh Kumar', 'A', '2021/04/22 10:00:35', '2021/04/22 09:29:35', '2021/04/22 10:00:35', 0, 'ganeshkumar.a@siemens-healthineers.com', 0, 'a6b1bfde2f22dfd66186eb2552d4406a'),
(284, 0, 0, 1, 0, 'Deepak ', 'Mishra', '2021/04/22 10:00:36', '2021/04/22 11:30:19', '2021/04/22 10:00:36', 0, 'deepak.mishra@siemens-healthineers.com', 0, 'c1be85f298543e84543fc0a7d4dcae11'),
(285, 0, 0, 0, 0, 'Akash', 'Jadoun', '2021/04/22 10:00:38', '2021/04/22 11:30:15', '2021/04/22 10:00:38', 0, 'akashsingh.jadoun@varian.com', 0, '8104b4f36ecb902623fbdc16e76b79d5'),
(286, 0, 0, 1, 0, 'Prashant', 'Kadkade', '2021/04/22 10:00:41', '2021/04/22 11:30:15', '2021/04/22 10:00:41', 0, 'prashant.kadkade@siemens-healthineers.com', 0, '405d46778253151704f417d3dd850d94'),
(287, 0, 0, 1, 0, 'Surajkumar', 'Chandrasekharan nair', '2021/04/22 10:29:37', '2021/04/22 09:43:48', '2021/04/22 10:00:41', 0, 'c.suraj_kumar@siemens-healthineers.com', 0, 'afd68ad2c135f659557355a83c3d5447'),
(288, 0, 0, 1, 0, 'Amit', 'Sawant', '2021/04/22 10:00:41', '2021/04/22 11:30:19', '2021/04/22 10:00:41', 0, 'amit.sawant@varian.com', 0, '7cd1a22cdcdb49636ae29cbd5a601513'),
(289, 0, 0, 1, 0, 'anand mohan ', 'singh', '2021/04/22 10:00:42', '2021/04/22 10:01:13', '2021/04/22 10:00:42', 0, 'anand.singh@varian.com', 0, '1e316886fbe6517b84a8e3fb4ba095d8'),
(290, 0, 0, 1, 0, 'Shyam ', 'Parekh ', '2021/04/22 10:00:46', '2021/04/22 11:30:17', '2021/04/22 10:00:46', 0, 'shyam.parekh@siemens-healthineers.com', 0, '895fd9d85d6848d74131c549f1a999c9'),
(291, 0, 0, 1, 0, 'Nishanth', 'S', '2021/04/22 10:00:51', '2021/04/22 10:01:18', '2021/04/22 10:00:51', 0, 'nishanth@coact.co.in', 0, 'aa3dc2f88091859ca7085faed00630ea'),
(292, 0, 0, 0, 1, 'Dileep', 'Kumar', '2021/04/22 10:00:53', '2021/04/22 10:06:28', '2021/04/22 10:00:53', 0, 'kumar.dileep@siemens-healthineers.com', 0, 'e3ac33e288f00f3a443f255a864dd29b'),
(293, 0, 1, 0, 0, 'Tejashree', 'Punde', '2021/04/22 10:00:55', '2021/04/22 11:30:19', '2021/04/22 10:00:55', 0, 'wd976039@varian.com', 0, '8dc422d9d7766d89437f8f9dc1f0fb44'),
(294, 0, 0, 1, 0, 'Surinder', 'Singh', '2021/04/22 10:00:56', '2021/04/22 10:00:56', '2021/04/22 10:00:56', 0, 'Surinder.singh@siemens-healthineers.com', 0, '839a9219abd6ede42d9728b23439c1a6'),
(295, 0, 0, 1, 0, 'Rajat', 'Kumar', '2021/04/22 10:00:56', '2021/04/22 09:16:57', '2021/04/22 10:00:56', 0, 'rajat.kumar@siemens-healthineers.com', 0, '5c3ee5d98ae69dd36843f349a50652e0'),
(296, 0, 0, 1, 0, 'Tanmay', 'Pandey', '2021/04/22 10:00:57', '2021/04/22 10:01:34', '2021/04/22 10:00:57', 0, 'tanmay.pandey@varian.com', 0, '58ce9b36cf31975a653cb4c3f51a4d05'),
(297, 0, 0, 1, 0, 'Shek Md', 'Nurunnabi', '2021/04/22 10:01:03', '2021/04/22 11:30:16', '2021/04/22 10:01:03', 0, 'shek.nurunnabi@siemens-healthineers.com', 0, 'e6dad30818ff1471034b89687c7791d9'),
(298, 0, 0, 1, 0, 'Ankur', 'Kapoor', '2021/04/22 10:01:05', '2021/04/22 11:30:16', '2021/04/22 10:01:05', 0, 'kapoor.ankur@siemens-healthineers.com', 0, 'c7e9b9b4c8653e17b407ecbb676957cc'),
(299, 0, 1, 0, 0, 'Amrita ', 'Soni', '2021/04/22 10:01:16', '2021/04/22 10:01:46', '2021/04/22 10:01:16', 0, 'amrita.soni@siemens-healthineers.com', 0, '81ac6cb0501beef014daf14857661a0d'),
(300, 0, 0, 1, 0, 'Suman', 'Sil', '2021/04/22 10:01:16', '2021/04/22 09:15:17', '2021/04/22 10:01:16', 0, 'suman.sil@varian.com', 0, 'd7f1caa0d558f88d99b7af78ba58b39f'),
(301, 0, 0, 1, 0, 'Jitender', 'Kumar', '2021/04/22 10:01:20', '2021/04/22 09:37:20', '2021/04/22 10:01:20', 0, 'jitenderkumar@siemens-healthineers.com', 0, '5a312fc0ebe41337c347d49ec4891c9e'),
(302, 0, 0, 1, 0, 'Nitish', 'Pinisetti', '2021/04/22 10:01:22', '2021/04/22 10:00:23', '2021/04/22 10:01:22', 0, 'nitish.pinisetti@siemens-healthineers.com', 0, '981a90c19aacb5a6d6d83dbf904815a1'),
(303, 0, 0, 1, 0, 'Vivekanand', 'Kale', '2021/04/22 10:01:23', '2021/04/22 11:30:13', '2021/04/22 10:01:23', 0, 'vivekanand.kale@siemens-healthineers.com', 0, '35998c59c6ed1a699365c586b1e4f0b9'),
(304, 0, 0, 1, 0, 'Ashish', 'Godbole', '2021/04/22 10:01:25', '2021/04/22 11:33:21', '2021/04/22 10:01:25', 0, 'ashish.godbole@siemens-healthineers.com', 0, '17e5015f975be4bfff37da55b41e40ed'),
(305, 0, 0, 1, 0, 'GERARD MAGESH ', 'JOSEPH', '2021/04/22 10:01:26', '2021/04/22 10:01:24', '2021/04/22 10:01:26', 0, 'gerard.joseph@varian.com', 0, 'fd7b81f346332e2d76b2df78c3afe900'),
(306, 1, 0, 0, 0, 'Shubhangi', 'Bhosle', '2021/04/22 10:01:32', '2021/04/22 09:01:02', '2021/04/22 10:01:32', 0, 'shubhangi.bhosle@varian.com', 0, 'ac00e2740d58367f7353bccd758121fa'),
(307, 0, 0, 1, 0, 'Ashutosh', 'Singh', '2021/04/22 10:01:33', '2021/04/22 11:30:21', '2021/04/22 10:01:33', 0, 'ashutoshsingh@siemens-healthineers.com', 0, 'c31ec950946e0c13b9a5bb125da40922'),
(308, 0, 0, 1, 0, 'Jiju', 'James', '2021/04/22 10:01:35', '2021/04/22 11:30:12', '2021/04/22 10:01:35', 0, 'jiju.james@varian.com', 0, 'ca5c3753aef6ead0ed0488a00ab042d1'),
(309, 0, 0, 1, 0, 'FAISAL', 'HUSSAIN', '2021/04/22 10:01:38', '2021/04/22 11:30:16', '2021/04/22 10:01:38', 0, 'faisal.hussain@varian.com', 0, '6f3fb24634bbfe4af0f1e7cdf0eb0129'),
(310, 0, 0, 1, 0, 'Jagjeet', 'Kocher', '2021/04/22 10:01:41', '2021/04/22 10:15:56', '2021/04/22 10:01:41', 0, 'jagjeet.kocher@varian.com', 0, '5e1e094a87806714e7fd3a2b11509d7b'),
(311, 0, 0, 1, 0, 'Vivek', 'Iyer', '2021/04/22 10:01:43', '2021/04/22 11:30:15', '2021/04/22 10:01:43', 0, 'vivek.iyer@siemens-healthineers.com', 0, '017a05c6fe4fb3580b20dc3350d20234'),
(312, 0, 0, 1, 0, 'Venkatesh', 'N', '2021/04/22 10:01:46', '2021/04/22 11:31:41', '2021/04/22 10:01:46', 0, 'n.venkatesha@siemens-healthineers.com', 0, '0dffaaa2a8be10fba791273272b53048'),
(313, 0, 0, 1, 0, 'Mohait', 'Md. Abdulla Al', '2021/04/22 10:01:47', '2021/04/22 11:32:58', '2021/04/22 10:01:47', 0, 'mohait.abdulla@siemens-healthineers.com', 0, 'fbf09ff144a33c18de3c3f815f93c1d1'),
(314, 0, 0, 1, 0, 'Anjani Kumar', 'Tripathi', '2021/04/22 10:01:50', '2021/04/22 11:30:23', '2021/04/22 10:01:50', 0, 'anjanikumar.tripathi@siemens-healthineers.com', 0, '2f3d7a9615aa58adfcd7bb34ab7b1c2c'),
(315, 0, 0, 1, 0, 'vinay', 'Kumar', '2021/04/22 10:01:54', '2021/04/22 11:30:15', '2021/04/22 10:01:54', 0, 'vinaykumar.raja@siemens-healthineers.com', 0, '801995b7e12904b5c5e9617916b1ed0a'),
(316, 0, 0, 1, 0, 'Ashok', 'Bhanushali', '2021/04/22 10:01:58', '2021/04/22 11:30:17', '2021/04/22 10:01:58', 0, 'ashok.bhanushali@siemens-healthineers.com', 0, 'f6b3d0c64d6b8181a65641bc3b97bb40'),
(317, 0, 0, 1, 0, 'Sambit', 'Sahoo', '2021/04/22 10:02:05', '2021/04/22 10:01:00', '2021/04/22 10:02:05', 0, 'sambit.sahoo@siemens-healthineers.com', 0, 'cd647744c69f4477446c787e627f668e'),
(318, 0, 0, 1, 0, 'Raghuraman', 'K V', '2021/04/22 10:02:06', '2021/04/22 10:01:36', '2021/04/22 10:02:06', 0, 'raghuraman.kv@varian.com', 0, 'cda63d831bd0b11e7da1a4d2a6332a69'),
(319, 0, 0, 1, 0, 'Abhishek', 'Mahajan', '2021/04/22 10:02:06', '2021/04/22 10:01:06', '2021/04/22 10:02:06', 0, 'blx8356@varian.com', 0, 'eb03c93be415979b4d39fae9ddb7edfc'),
(320, 0, 0, 1, 0, 'Avdhut', 'More', '2021/04/22 10:02:09', '2021/04/22 11:30:15', '2021/04/22 10:02:09', 0, 'avdhut.more@siemens-healthineers.com', 0, '13cd36b1c7b6b38078df2d7b4885d156'),
(321, 0, 0, 1, 0, 'Raphael', 'Nadar', '2021/04/22 10:02:09', '2021/04/22 09:40:41', '2021/04/22 10:02:09', 0, 'raphael.nadar@varian.com', 0, '327c1456b1dddec1e942a3d14cd7a525'),
(322, 1, 0, 0, 0, 'Jaslin', 'Bawa', '2021/04/22 10:02:10', '2021/04/22 10:01:11', '2021/04/22 10:02:10', 0, 'jaslin.lamba@siemens-healthineers.com', 0, '76f297cdb2efd886c1247b34671ba288'),
(323, 0, 0, 1, 0, 'Suprit', 'E', '2021/04/22 10:02:10', '2021/04/22 10:03:10', '2021/04/22 10:02:10', 0, 'suprit.rampurkar@siemens-healthineers.com', 0, '8be71a8d5a74e734f1f6de01470bdc86'),
(324, 0, 0, 1, 0, 'Kartik', 'Iyer', '2021/04/22 10:02:11', '2021/04/22 10:09:19', '2021/04/22 10:02:11', 0, 'iyer.kartik@siemens-healthineers.com', 0, 'bc48947487a7afd2f0f565a1507c5f1a'),
(325, 0, 0, 1, 0, 'Perminder', 'Smagh', '2021/04/22 10:02:15', '2021/04/22 10:31:41', '2021/04/22 10:02:15', 0, 'perminder.smagh@siemens-healthineers.com', 0, '077a6671e6ae75504e08a8eeef8527b3'),
(326, 0, 0, 1, 0, 'Subhasish', 'Ghosh', '2021/04/22 10:02:16', '2021/04/22 08:34:18', '2021/04/22 10:02:16', 0, 'subhasish.ghosh@siemens-healthineers.com', 0, 'aaeec7ffb4b70138ac560e03e68daac5'),
(327, 0, 0, 1, 0, 'Jayanta', 'Mukherjee', '2021/04/22 10:02:18', '2021/04/22 11:30:17', '2021/04/22 10:02:18', 0, 'jayanta.mukherjee@siemens-healthineers.com', 0, '8a2ca996c563dd111355e286cf2f594c'),
(328, 1, 0, 0, 0, 'Ramya', 'Santhanam', '2021/04/22 10:02:21', '2021/04/22 10:00:51', '2021/04/22 10:02:21', 0, 'ramya.santhanam01@siemens-healthineers.com', 0, '92433a5b327585abacdfe90c81538be2'),
(329, 0, 0, 1, 0, 'Swanand', 'Darshetkar', '2021/04/22 10:02:21', '2021/04/22 11:30:16', '2021/04/22 10:02:21', 0, 'Swanand.Darshetkar@varian.com', 0, '332cc11119071cd4cdead50d93f02645'),
(330, 0, 0, 1, 0, 'Vipin', 'Raina', '2021/04/22 10:02:23', '2021/04/22 11:30:18', '2021/04/22 10:02:23', 0, 'vipin.raina@siemens-healthineers.com', 0, '90801a826d8d69371adb53865f2a4dad'),
(331, 0, 0, 1, 0, 'Girish', 'Khairnar', '2021/04/22 10:02:23', '2021/04/22 10:36:32', '2021/04/22 10:02:23', 0, 'girish.khairnar@varian.com', 0, 'c01ae8420467d793bad5f5c0e4d84908'),
(332, 1, 0, 0, 0, 'Mafuja ', 'As Parvin', '2021/04/22 10:02:23', '2021/04/22 09:36:54', '2021/04/22 10:02:23', 0, 'mafuja.parvin@siemens-healthineers.com', 0, '97b3f162f61a657f022300cf37edea20'),
(333, 0, 0, 1, 0, 'Yash', 'Jain', '2021/04/22 10:02:24', '2021/04/22 11:30:18', '2021/04/22 10:02:24', 0, 'yash.jain@siemens-healthineers.com', 0, '0c8529e4b3565dea60647e62f0382dda'),
(334, 1, 0, 0, 0, 'John', 'Poulose', '2021/04/22 10:02:25', '2021/04/22 10:01:25', '2021/04/22 10:02:25', 0, 'john.poulose@siemens-healthineers.com', 0, '502c919e7029771f769f033d40e780c7'),
(335, 0, 0, 1, 0, 'Akshay', 'Khedekar', '2021/04/22 10:02:26', '2021/04/22 11:30:15', '2021/04/22 10:02:26', 0, 'akshay.khedekar@varian.com', 0, '84b341d45ff39886fe87c8e1fbfdd1a0'),
(336, 0, 0, 1, 0, 'Vikas', 'Gourgonda', '2021/04/22 10:02:27', '2021/04/22 09:46:58', '2021/04/22 10:02:27', 0, 'vikas.gourgonda@siemens-healthineers.com', 0, 'f577a079d79ad3f310b0e0cb76a53806'),
(337, 0, 0, 0, 0, 'Narasimha Murthy', 'MV', '2021/04/22 10:02:30', '2021/04/22 11:05:11', '2021/04/22 10:02:30', 0, 'narasimha.mv@siemens-healthineers.com', 0, 'fef54809bf290fe18e1a672b6ad678ee'),
(338, 0, 0, 1, 0, 'Rajoo', 'Narang', '2021/04/22 10:02:35', '2021/04/22 11:30:12', '2021/04/22 10:02:35', 0, 'rajoo.narang@siemens-healthineers.com', 0, 'cbd941a2d865a9d741ae6463a65fe6cc'),
(339, 0, 0, 1, 0, 'Mohammad', 'Shahidullah', '2021/04/22 10:02:44', '2021/04/22 10:00:44', '2021/04/22 10:02:44', 0, 'mohammad.shahidullah@siemens-healthineers.com', 0, '3a7efadd3204445bd9628885424057e9'),
(340, 1, 0, 0, 0, 'Shweta', 'Malhotra', '2021/04/22 10:02:46', '2021/04/22 10:48:24', '2021/04/22 10:02:46', 0, 'shweta.malhotra@varian.com', 0, '8902800777890fcf15373ae9bac49263'),
(341, 0, 1, 0, 0, 'Neha', 'Nag', '2021/04/22 10:02:49', '2021/04/22 11:40:10', '2021/04/22 10:02:49', 0, 'neha.nag@siemens-healthineers.com', 0, '26de547ef2f431915df202a0f19dfc2e'),
(342, 0, 1, 0, 0, 'Colette', 'Parakh', '2021/04/22 10:02:49', '2021/04/22 11:36:50', '2021/04/22 10:02:49', 0, 'colette.parakh@siemens-healthineers.com', 0, '173bf23e1bb9c1d5e459eba5d00c316b'),
(343, 0, 0, 0, 0, 'Arjun Prasad ', 'Yadav', '2021/04/22 10:02:51', '2021/04/22 11:30:14', '2021/04/22 10:02:51', 0, 'arjun.yadav@siemens-healthineers.com', 0, '4d87687d982db714b6ed8baaf5379f08'),
(344, 0, 0, 1, 0, 'LUCKY', 'SANGWAN', '2021/04/22 10:02:52', '2021/04/22 09:40:23', '2021/04/22 10:02:52', 0, 'lucky.sangwan@siemens-healthineers.com', 0, '449a69dac67f505888ae95310608f967'),
(345, 0, 0, 1, 0, 'Subrata ', 'Patnaik ', '2021/04/22 10:49:50', '2021/04/22 10:01:20', '2021/04/22 10:02:53', 0, 'subrata.patnaik@siemens-healthineers.com', 0, 'f9c98fb5968bda35863ab473a0a89f49'),
(346, 0, 0, 1, 0, 'Bhargav', 'K', '2021/04/22 10:02:54', '2021/04/22 11:30:23', '2021/04/22 10:02:54', 0, 'bhargav.k@siemens-healthineers.com', 0, 'da3544872298747d7ccf5c4447129acb'),
(347, 0, 0, 1, 0, 'CHANDRA SEKHAR', 'Chimakurthy', '2021/04/22 10:02:56', '2021/04/22 08:38:57', '2021/04/22 10:02:56', 0, 'chandra.chimakurthy@siemens-healthineers.com', 0, 'fdc57c0516110511dc310f151ad3057d'),
(348, 0, 0, 1, 0, 'Karanbir', 'Singh', '2021/04/22 10:03:02', '2021/04/22 11:30:15', '2021/04/22 10:03:02', 0, 'karanbir.ahluwalia@siemens-healthineers.com', 0, '97ad8a8f39698d02d198add6b8cb62dc'),
(349, 0, 0, 1, 0, 'visha', 'suri', '2021/04/22 10:03:04', '2021/04/22 13:42:10', '2021/04/22 10:03:04', 1, 'vishal.suuri@siemens-healthineers.com', 0, 'df3c2f80cc43b6c4c11cb1e9d024dba8'),
(350, 0, 0, 1, 0, 'Harish', 'Nair', '2021/04/22 10:03:06', '2021/04/22 11:30:12', '2021/04/22 10:03:06', 0, 'harish.nair@siemens-healthineers.com', 0, 'c210180328bc010fa230262b1188ad75'),
(351, 0, 0, 1, 0, 'ARPAN', 'MALHOTRA', '2021/04/22 10:03:06', '2021/04/22 10:01:07', '2021/04/22 10:03:06', 0, 'arpan.malhotra@siemens-healthineers.com', 0, '8c0a731e0a27b0ad5238e755dd0a91b8'),
(352, 0, 0, 1, 0, 'Nitin', 'Dakre', '2021/04/22 10:03:10', '2021/04/22 11:30:15', '2021/04/22 10:03:10', 0, 'nitin.dakre@varian.com', 0, '0a3c66a4087718bbc54599eecbba5d8c'),
(353, 0, 0, 1, 0, 'Kishan', 'Raj', '2021/04/22 10:03:13', '2021/04/22 10:02:14', '2021/04/22 10:03:13', 0, 'kishan.raj@siemens-healthineers.com', 0, 'b900ede941d38f3d223d753395170643'),
(354, 0, 0, 0, 0, 'jamshed', 'khan', '2021/04/22 10:03:15', '2021/04/22 10:26:43', '2021/04/22 10:03:15', 0, 'jamshed-hayat.khan@siemens-healthineers.com', 0, 'd55c2adddddd52bfaf37823db3ff1c3d'),
(355, 0, 0, 1, 0, 'Neeraj', 'Banchhor', '2021/04/22 10:03:17', '2021/04/22 14:31:29', '2021/04/22 10:03:17', 1, 'neeraj.banchhor@varian.com', 0, 'e0eacb74bf4d998889492ccf2da19b82'),
(356, 0, 0, 1, 0, 'Kailash', 'Yagnik', '2021/04/22 10:03:19', '2021/04/22 11:51:55', '2021/04/22 10:03:19', 0, 'kailash.yagnik@siemens-healthineers.com', 0, 'e4563b8004cf1e1154a5a2519a658c09'),
(357, 0, 0, 1, 0, 'Kaushik', 'Ganguly', '2021/04/22 10:03:22', '2021/04/22 11:30:21', '2021/04/22 10:03:22', 0, 'kaushik.ganguly@siemens-healthineers.com', 0, '64579b7f30ac12dd66a9852ea9d76a90'),
(358, 0, 0, 1, 0, 'Sanjay', 'David', '2021/04/22 15:08:07', '2021/04/22 15:09:07', '2021/04/22 10:03:26', 1, 'sanjay.david@siemens-healthineers.com', 0, '3697b753b42ad8b766a58e6476c8a461'),
(359, 0, 0, 1, 0, 'Arun', 'Kaul', '2021/04/22 10:03:31', '2021/04/22 10:01:01', '2021/04/22 10:03:31', 0, 'arun.kaul@siemens-healthineers.com', 0, '3cbb512379929048e1c51f0a07ea2944'),
(360, 0, 0, 1, 0, 'Kiran', 'Doke', '2021/04/22 10:03:31', '2021/04/22 10:01:02', '2021/04/22 10:03:31', 0, 'Kiran.doke@siemens-healthineers.com', 0, '82147ce4fac4e91368da3252aec80246'),
(361, 0, 0, 1, 0, 'ASHOK', 'KUMAR GUPTA', '2021/04/22 10:03:36', '2021/04/22 09:24:07', '2021/04/22 10:03:36', 0, 'ashokkumar.gupta@siemens-healthineers.com', 0, '171f9a605fcf3bd72176a949a2dfb149'),
(362, 1, 0, 0, 0, 'Rosebud', 'Gomes', '2021/04/22 10:03:38', '2021/04/22 10:12:09', '2021/04/22 10:03:38', 0, 'rosebud.gomes@siemens-healthineers.com', 0, 'ec624ebe46336b66424441d9967ad817'),
(363, 0, 0, 1, 0, 'Ketan', 'Kansara', '2021/04/22 10:03:40', '2021/04/22 10:01:10', '2021/04/22 10:03:40', 0, 'ketan.kansara@siemens-healthineers.com', 0, 'f8cddb4c7bf00f0d016ca3f6d7f0cfeb'),
(364, 0, 0, 0, 0, 'Prashant', 'Sinha', '2021/04/22 10:03:43', '2021/04/22 11:30:20', '2021/04/22 10:03:43', 0, 'prashantkumar.sinha@siemens-healthineers.com', 0, 'f88d41caebe9bbd519190e293d8d8830'),
(365, 0, 0, 1, 0, 'Sumit', 'Ghosh', '2021/04/22 11:10:37', '2021/04/22 11:11:36', '2021/04/22 10:03:48', 0, 'sumit.ghosh@siemens-healthineers.com', 0, 'e70201ba4184320eb1edd43af7481b06'),
(366, 1, 0, 0, 0, 'PRIYA ', 'SATHYAMOORTHY ', '2021/04/22 10:03:50', '2021/04/22 08:56:50', '2021/04/22 10:03:50', 0, 'priya.sathyamoorthy@siemens-healthineers.com', 0, '08ede386e8c32c23c55b97bd324901c2'),
(367, 1, 0, 0, 0, 'Anu', 'Lidia', '2021/04/22 10:03:50', '2021/04/22 08:39:21', '2021/04/22 10:03:50', 0, 'anu.lidia@siemens-healthineers.com', 0, 'a46a31cb5fbd6fadaa5c4930b6623671'),
(368, 0, 0, 0, 0, 'Vikas', 'Nayak', '2021/04/22 10:03:51', '2021/04/22 10:00:52', '2021/04/22 10:03:51', 0, 'vikas.nayak@siemens-healthineers.com', 0, '8435efcae6ffad387874695081203e7f'),
(369, 1, 0, 0, 0, 'Balaji', 'Jagannathan', '2021/04/22 11:08:27', '2021/04/22 11:30:10', '2021/04/22 10:03:52', 0, 'balaji.jagannathan@varian.com', 0, '2625d3ea99f79512550019c530b1b41c'),
(370, 0, 0, 1, 0, 'Gobinda ', 'Pal', '2021/04/22 10:03:53', '2021/04/22 09:08:55', '2021/04/22 10:03:53', 0, 'gobinda.pal@varian.com', 0, 'bf155e0eafa3fb09cca0e8e0a2801c41'),
(371, 0, 0, 1, 0, 'Anil', 'Menon', '2021/04/22 10:03:55', '2021/04/22 11:30:29', '2021/04/22 10:03:55', 0, 'anilkumar.menon@siemens-healthineers.com', 0, '0ba0dcb9ae261a9be12bb1d040ceecef'),
(372, 1, 0, 0, 0, 'Inderpreet ', 'kaur', '2021/04/22 10:03:55', '2021/04/22 10:01:25', '2021/04/22 10:03:55', 0, 'inderpreet.kaur@varian.com', 0, '0d5756aeec547d128eeb751a2c48a0f0'),
(373, 0, 0, 1, 0, 'Chirag', 'Sakpal', '2021/04/22 10:03:55', '2021/04/22 08:39:55', '2021/04/22 10:03:55', 0, 'chirag.sakpal@siemens-healthineers.com', 0, 'c0ee19f888b9460fd45af99798b97206'),
(374, 0, 0, 1, 0, 'Mathew', 'John', '2021/04/22 10:03:56', '2021/04/22 12:32:32', '2021/04/22 10:03:56', 0, 'mathew.john@siemens-healthineers.com', 0, '264632bd4f9078486cb0c99bc9f5d65b'),
(375, 0, 0, 1, 0, 'Rahul', 'B V', '2021/04/22 10:04:00', '2021/04/22 11:30:12', '2021/04/22 10:04:00', 0, 'rahul.b_v@siemens-healthineers.com', 0, '244b926dd6c2d89bdae51d17afae2a96'),
(376, 0, 0, 0, 0, 'Pavan', 'Taori', '2021/04/22 10:04:04', '2021/04/22 10:01:04', '2021/04/22 10:04:04', 0, 'pavan.taori@siemens-healthineers.com', 0, 'b63a5d128181b555df4140d94cfee028'),
(377, 0, 0, 1, 0, 'Anil', 'Chindani', '2021/04/22 10:04:10', '2021/04/22 11:30:16', '2021/04/22 10:04:10', 0, 'anil.chindani@siemens-healthineers.com', 0, 'f1f80113fb18e93080eb111e1b7eb91d'),
(378, 0, 0, 1, 0, 'Subroto', 'Biswas', '2021/04/22 10:04:11', '2021/04/22 11:30:13', '2021/04/22 10:04:11', 0, 'subroto.biswas@siemens-healthineers.com', 0, 'eb39a0200d51128227a490c907fd2475'),
(379, 0, 0, 0, 1, 'Richard', 'Rumnong', '2021/04/22 10:04:17', '2021/04/22 10:01:17', '2021/04/22 10:04:17', 0, 'richard.rumnong@siemens-healthineers.com', 0, 'c65ccf028afeb5b8a81d35c99a20b8d8'),
(380, 1, 0, 0, 0, 'neeraj', 'reddy', '2021/08/05 08:40:38', '2021/08/05 08:42:39', '2021/04/22 10:04:18', 1, 'neeraj@coact.co.in', 0, '561a1eb3ca2a223e12b421c166a92ec0'),
(381, 0, 0, 1, 0, 'Ramachandran', 'Pandurangan', '2021/04/22 10:04:31', '2021/04/22 11:30:16', '2021/04/22 10:04:31', 0, 'p.ramachandran@siemens-healthineers.com', 0, '849bd636623590936bc008259f0eb008'),
(382, 0, 1, 0, 0, 'Poornima', 'Padmanabhan', '2021/04/22 10:04:31', '2021/04/22 10:01:01', '2021/04/22 10:04:31', 0, 'poornima.padmanabhan@varian.com', 0, 'e7dbe3d1b98e6f41a66e8dca4ba14d02'),
(383, 0, 0, 0, 0, 'Kamal', 'Chaudhry', '2021/04/22 10:04:33', '2021/04/22 11:30:24', '2021/04/22 10:04:33', 0, 'kamal.chaudhry@siemens-healthineers.com', 0, '8b4372e3c96ed603687808a3f36813fe'),
(384, 0, 0, 1, 0, 'Chackochan', 'PT', '2021/04/22 10:04:36', '2021/04/22 10:01:07', '2021/04/22 10:04:36', 0, 'chackochan.pt@siemens-healthineers.com', 0, '90445964044a336690bd3d6f45d4ec98'),
(385, 0, 0, 0, 0, 'Raman Kumar', 'Kumar', '2021/04/22 10:04:44', '2021/04/22 10:45:39', '2021/04/22 10:04:44', 0, 'raman.kumar@siemens-healthineers.com', 0, '00f0e7e3b7627860ef60d0481873d1dc'),
(386, 0, 0, 1, 0, 'Bhujangarao', 'Gundra', '2021/04/22 10:04:45', '2021/04/22 10:01:16', '2021/04/22 10:04:45', 0, 'bhujanga.gundra@siemens-healthineers.com', 0, 'adece6e462abb3c34201ddb64e343da3'),
(387, 0, 0, 1, 0, 'Milap', 'Shah', '2021/04/22 10:04:48', '2021/04/22 11:30:16', '2021/04/22 10:04:48', 0, 'milap.shah@siemens-healthineers.com', 0, '71c7f7710aa28aca5e373412748ba859'),
(388, 1, 0, 0, 0, 'Shikha', 'Pillai', '2021/04/22 10:04:51', '2021/04/22 11:30:11', '2021/04/22 10:04:51', 0, 'pillai.shikha@siemens-healthineers.com', 0, '6f683be8ef38dab14da4b5ee3c36486e'),
(389, 0, 0, 0, 0, 'Ajit ', 'Singh', '2021/04/22 10:05:13', '2021/04/22 10:01:13', '2021/04/22 10:05:13', 0, 'ajit.singh@varian.com', 0, 'ae4c68edfc0fe31ed805554292f2327b'),
(390, 0, 0, 1, 0, 'Sanjay', 'David', '2021/04/22 15:08:07', '2021/04/22 15:09:07', '2021/04/22 10:05:17', 1, 'sanjay.david@siemens-healthineers.com', 0, '85392f9e1b4bdcf3be57224634cce2a4'),
(391, 0, 0, 1, 0, 'Prabhat', 'Kumar', '2021/04/22 10:05:17', '2021/04/22 11:30:24', '2021/04/22 10:05:17', 0, 'kumar.prabhat@siemens-healthineers.com', 0, '86097e466af510737ef0f94e382a8b64'),
(392, 0, 0, 1, 0, 'Harish', 'Gulhar', '2021/04/22 10:05:20', '2021/04/22 11:30:35', '2021/04/22 10:05:20', 0, 'harish.gulhar@siemens-healthineers.com', 0, '858017ebf843123315c726907f4c5237'),
(393, 0, 0, 1, 0, 'Sanjeev ', 'Kumar', '2021/04/22 10:05:22', '2021/04/22 10:11:22', '2021/04/22 10:05:22', 0, 'sanjeev.kumar1@siemens-healthineers.com', 0, '57188016c03ff5f655c5f88deb066c67'),
(394, 0, 0, 1, 0, 'Ashwini ', 'Kumar', '2021/04/22 10:28:52', '2021/04/22 09:25:44', '2021/04/22 10:05:28', 0, 'ashwinikumar@siemens-healthineers.com', 0, '78283875577f9c3ab431af4ad47c6dc9'),
(395, 0, 0, 1, 0, 'uday', 'kumar', '2021/04/22 10:09:48', '2021/04/22 11:30:21', '2021/04/22 10:05:28', 0, 'kumar.udaya@siemens-healthineers.com', 0, '8e69942aeb0808a1c6906ba2a51a7eb0'),
(396, 0, 0, 0, 0, 'sandra ', 'marchon', '2021/04/22 10:05:29', '2021/04/22 10:03:59', '2021/04/22 10:05:29', 0, 'sandra.marchon@varian.com', 0, '27912e2dbf3fbbd90912ad947606bcf0'),
(397, 0, 0, 1, 0, 'Mohammed Saiful', 'Islam', '2021/04/22 10:05:31', '2021/04/22 10:03:32', '2021/04/22 10:05:31', 0, 'mohammed.saiful@siemens-healthineers.com', 0, 'c67b2b9915f2b58c47ed7be14e49cba5'),
(398, 0, 0, 0, 0, 'niraj', 'garg', '2021/04/22 10:05:42', '2021/04/22 10:01:13', '2021/04/22 10:05:42', 0, 'niraj.garg@siemens-healthineers.com', 0, '20630f0f4c47243dd1325c947786b16c'),
(399, 0, 0, 0, 0, 'Shreya', 'Tole', '2021/04/22 10:05:44', '2021/04/22 11:30:12', '2021/04/22 10:05:44', 0, 'shreya.tole@varian.com', 0, 'd2244aa76a63c0f19ed3afd28405ea02'),
(400, 0, 0, 0, 0, 'Srividhya', 'Subramanian', '2021/04/22 10:05:50', '2021/04/22 11:30:18', '2021/04/22 10:05:50', 0, 'subramanian.srividhya@siemens-healthineers.com', 0, 'f51340576c3a115761631013299c888b'),
(401, 0, 0, 0, 0, 'Bala', 'Murugan', '2021/04/22 10:08:33', '2021/04/22 10:01:04', '2021/04/22 10:05:51', 0, 'subhash.balamurugan@varian.com', 0, '6d511ae9fca767b90b6405fb6b7a0004'),
(402, 0, 0, 1, 0, 'Kishore', 'Kumar', '2021/04/22 10:05:55', '2021/04/22 11:30:16', '2021/04/22 10:05:55', 0, 'kishore.kumar@siemens-healthineers.com', 0, '0ba9f7411ec7ac091b0add59e504d131'),
(403, 0, 0, 1, 0, 'Karrthick', 'Kp', '2021/04/22 10:06:08', '2021/04/22 10:01:09', '2021/04/22 10:06:08', 0, 'karrthick.kp@varian.com', 0, 'ef72ba3b1d6e4b4638458158c5145132'),
(404, 0, 0, 1, 0, 'naveen', 'paulose', '2021/04/22 10:06:12', '2021/04/22 11:59:13', '2021/04/22 10:06:12', 0, 'naveen.paulose@siemens-healthineers.com', 0, 'fbc8ed3a210547751bb8426dcdd50229'),
(405, 0, 0, 1, 0, 'Raja', 'Roy', '2021/04/22 10:06:16', '2021/04/22 08:50:16', '2021/04/22 10:06:16', 0, 'raja.roy@siemens-healthineers.com', 0, 'e72b65d1bb4a48f00fe62ec7e217a768'),
(406, 0, 0, 1, 0, 'Sanchal', 'Ramachandran', '2021/04/22 10:06:25', '2021/04/22 11:30:26', '2021/04/22 10:06:25', 0, 'sanchal.ramachandran@varian.com', 0, '9d936982f337b7b2fe653d2b0d383266'),
(407, 0, 0, 0, 0, 'Vineet', 'Gupta', '2021/04/22 10:06:32', '2021/04/22 11:30:21', '2021/04/22 10:06:32', 0, 'vineet.gupta@varian.com', 0, '92997deb07f2a6b3b081512900b172e2'),
(408, 1, 0, 0, 0, 'pooja', 'singh', '2021/04/22 10:06:32', '2021/04/22 10:01:33', '2021/04/22 10:06:32', 0, 'pooja.singh@varian.com', 0, '04e27749af52951f4c872ee8db9220e2'),
(409, 0, 0, 1, 0, 'Mahesh', 'Jejurkar', '2021/04/22 10:06:34', '2021/04/22 10:02:56', '2021/04/22 10:06:34', 0, 'mahesh.jejurkar@varian.com', 0, '949bfc24acfe9b6eddb724a9b12bd0d1'),
(410, 1, 0, 0, 0, 'Vaibhav ', 'Ambavane', '2021/04/22 10:06:45', '2021/04/22 08:48:19', '2021/04/22 10:06:45', 0, 'vaibhav.ambavane@siemens-healthineers.com', 0, 'aac8cfaebd590e0b2816eba3bbb27bed'),
(411, 0, 0, 1, 0, 'krijesh', 'Pk', '2021/04/22 10:06:53', '2021/04/22 10:01:24', '2021/04/22 10:06:53', 0, 'krijesh.kunnath@siemens-healthineers.com', 0, '4ba2e17aff64748381295cc7201dcfa7'),
(412, 0, 0, 1, 0, 'simarpreet singh ', 'sahni', '2021/04/22 10:06:58', '2021/04/22 10:00:59', '2021/04/22 10:06:58', 0, 'simarpreet.sahni@siemens-healthineers.com', 0, '76fad5e187039f770ab7b7b33fab3a29'),
(413, 0, 0, 1, 0, 'Saurabh', 'Arora', '2021/04/22 10:07:00', '2021/04/22 09:37:00', '2021/04/22 10:07:00', 0, 'saurabh.arora@siemens-healthineers.com', 0, '75ef6aff7aeec1f92d6ba02830e6ceb7'),
(414, 0, 0, 1, 0, 'Ajay', 'Khapre', '2021/04/22 10:07:12', '2021/04/22 11:30:18', '2021/04/22 10:07:12', 0, 'ajay.khapre@varian.com', 0, '83d1a87fae779c43d8d3ce0fec74f3cb'),
(415, 0, 0, 1, 0, 'Piyush', 'Gupta', '2021/04/22 10:07:32', '2021/04/22 08:40:32', '2021/04/22 10:07:32', 0, 'piyush.gupta@varian.com', 0, '0a8b5e27a335086c09696e0748bd5b9c'),
(416, 0, 0, 1, 0, 'Razibul', 'Karim', '2021/04/22 10:08:04', '2021/04/22 10:01:35', '2021/04/22 10:08:04', 0, 'razibul.karim@siemens-healthineers.com', 0, '16f43052602bc6b26a9e230b43a557cc'),
(417, 0, 1, 0, 0, 'Devika ', 'Kamath', '2021/04/22 10:08:18', '2021/04/22 09:58:54', '2021/04/22 10:08:18', 0, 'devika.kamath@siemens-healthineers.com', 0, '049872f4848b7c5c3e71459939bbf24c'),
(418, 0, 0, 1, 0, 'Lokesh', 'Agrawal', '2021/04/22 10:08:24', '2021/04/22 10:09:24', '2021/04/22 10:08:24', 0, 'lokesh.agarwal@siemens-healthineers.com', 0, '528b7be43432664a314dc052d8cb9ef6'),
(419, 0, 0, 1, 0, 'vishnu.', 'YR', '2021/04/22 10:08:27', '2021/04/22 11:30:28', '2021/04/22 10:08:27', 0, 'vishnu.yr@varian.com', 0, '87143cd33a0ad0434a9b5f674f5babd1'),
(420, 1, 0, 0, 0, 'Bala', 'Murugan ', '2021/04/22 10:08:33', '2021/04/22 10:01:04', '2021/04/22 10:08:33', 0, 'subhash.balamurugan@varian.com', 0, '89e3396dd088aa5866ac72429b663035'),
(421, 0, 0, 1, 0, 'Ashish', 'Rana', '2021/04/22 10:08:55', '2021/04/22 10:18:55', '2021/04/22 10:08:55', 0, 'ashish.rana@siemens-healthineers.com', 0, '9466e0def9b6402c113894fbc2d342dd'),
(422, 0, 0, 1, 0, 'Md.', 'Islam', '2021/04/22 10:09:09', '2021/04/22 10:09:10', '2021/04/22 10:09:09', 0, 'zahid.islam@siemens-healthineers.com', 0, '22f07c3db8a9843e50705edd463db150'),
(423, 0, 0, 1, 0, 'udaya', 'kumar', '2021/04/22 10:09:48', '2021/04/22 11:30:21', '2021/04/22 10:09:48', 0, 'kumar.udaya@siemens-healthineers.com', 0, '1ac29fd26e40a31512da26465a16aa3c'),
(424, 0, 0, 1, 0, 'Keyur ', 'Bavishi', '2021/04/22 10:09:48', '2021/04/22 09:54:50', '2021/04/22 10:09:48', 0, 'keyur.bavishi@siemens-healthineers.com', 0, 'd441023055d4322374f00721da7e1b71'),
(425, 0, 0, 0, 0, 'Pradip', 'Shivhare', '2021/04/22 10:09:50', '2021/04/22 11:31:24', '2021/04/22 10:09:50', 0, 'pradip.shivhare@varian.com', 0, 'e2fe43b19561e4838da8e0bba8afe5f7'),
(426, 0, 0, 0, 0, 'Dheeraj', 'Kapil', '2021/04/22 10:10:07', '2021/04/22 08:43:07', '2021/04/22 10:10:07', 0, 'Dheeraj.kapil@varian.com', 0, 'f10fc79beaf103e52d852ad66ed4ec93'),
(427, 0, 0, 1, 0, 'Subodh ', 'Divgi', '2021/04/22 10:10:23', '2021/04/22 09:10:23', '2021/04/22 10:10:23', 0, 'Subodh.divgi@siemens-healthineers.com', 0, 'c0a0bb6dba16cf9267d13894fec6ffa6'),
(428, 0, 0, 1, 0, 'Micheal', 'Porattukkara', '2021/04/22 10:10:39', '2021/04/22 09:06:36', '2021/04/22 10:10:39', 0, 'micheal.porattukkara@varian.com', 0, '82039bceb03d468695b1b2800f5a1947'),
(429, 0, 0, 1, 0, 'Atul', 'Solanki', '2021/04/22 10:10:52', '2021/04/22 10:45:37', '2021/04/22 10:10:52', 0, 'Atul.Solanki@varian.com', 0, 'b2616666851775063db7e6efab9563ed'),
(430, 0, 0, 1, 0, 'Babasaheb ', 'Shingade', '2021/04/22 10:11:21', '2021/04/22 09:31:51', '2021/04/22 10:11:21', 0, 'babasaheb.shingade@varian.com', 0, '7cab0a1cbcdfdef0564453aead33d7c9'),
(431, 0, 0, 1, 0, 'Purushothaman', 'kalaiselvan', '2021/04/22 10:11:29', '2021/04/22 10:07:30', '2021/04/22 10:11:29', 0, 'purushothaman.kalaiselvan@varian.com', 0, '9e408ee1509eaa608f453859eeebacfc'),
(432, 0, 0, 1, 0, 'Brijesh', 'Verma', '2021/04/22 10:11:40', '2021/04/22 11:30:10', '2021/04/22 10:11:39', 0, 'brijesh.verma@siemens-healthineers.com', 0, 'd8bed588d01b19de046bd1e44d6fe6fd'),
(433, 0, 0, 1, 0, 'Abhinav', 'Chaudhary', '2021/04/22 10:12:11', '2021/04/22 09:11:42', '2021/04/22 10:12:11', 0, 'abhinav.chaudhary@siemens-healthineers.com', 0, '7419bfa58db5d027f2899ba03c37fd43'),
(434, 0, 0, 1, 0, 'Dharmendra', 'Kumar', '2021/04/22 10:12:24', '2021/04/22 11:30:13', '2021/04/22 10:12:24', 0, 'dharmendra-kumar@siemens-healthineers.com', 0, 'd561d4720018e35b5c744a07d76d48c6'),
(435, 1, 0, 0, 0, 'Priyanka', 'Singh', '2021/04/22 10:14:23', '2021/04/22 09:57:53', '2021/04/22 10:14:23', 0, 'priyanka.singh@varian.com', 0, 'bd490edec5fe40323f8fdbe5c5c32eec'),
(436, 0, 1, 0, 0, 'Shalki', 'Bhatnagar', '2021/04/22 10:14:59', '2021/04/22 09:17:29', '2021/04/22 10:14:59', 0, 'shalki.bhatnagar@siemens-healthineers.com', 0, '4b12601aa7d821338a572cf3273df644'),
(437, 0, 0, 1, 0, 'Md.Abdullah al', 'mahmid', '2021/04/22 10:15:00', '2021/04/22 10:01:31', '2021/04/22 10:15:00', 0, 'abdullah.mahmid@siemens-healthineers.com', 0, '000e3f57845cb1be82199af498b02102'),
(438, 0, 0, 1, 0, 'Vaanmugil', 'Vasu', '2021/04/22 10:15:00', '2021/04/22 10:01:28', '2021/04/22 10:15:00', 0, 'vaanmugil.vasu@siemens-healthineers.com', 0, '523d154e37a1eee5f20d92d0769ada8b'),
(439, 1, 0, 0, 0, 'somnath', 'dutta', '2021/04/22 10:15:27', '2021/04/22 14:07:09', '2021/04/22 10:15:27', 1, 'somnath.dutta@varian.com', 0, '8f26449aa409b8bf49a61190fbc9383f'),
(440, 0, 0, 0, 1, 'Manohar', 'Kollegal', '2021/04/22 10:23:16', '2021/04/22 11:30:23', '2021/04/22 10:15:29', 0, 'manohar.kollegal@siemens-healthineers.com', 0, '11249849021f2c5c6010b8ca6f07df3b'),
(441, 0, 0, 1, 0, 'Mintu', 'Pal', '2021/04/22 10:15:39', '2021/04/22 09:43:27', '2021/04/22 10:15:39', 0, 'mintu.pal@siemens-healthineers.com', 0, '2a23d010983bd3f1ebcd825c6a89ba9c'),
(442, 1, 0, 0, 0, 'Nishi', 'Satyawali', '2021/04/22 10:27:20', '2021/04/22 09:59:33', '2021/04/22 10:16:06', 0, 'nishi.satyawali@siemens-healthineers.com', 0, '906d361ad343b4ec7fcfacc284a7b8f2'),
(443, 0, 0, 0, 0, 'Ramya', 'Devaraj', '2021/04/22 10:16:36', '2021/04/22 10:28:50', '2021/04/22 10:16:36', 0, 'ramya.devaraj@siemens-healthineers.com', 0, '66fb4597f3212711a4c250075c9c8aa7'),
(444, 1, 0, 0, 0, 'Kushal', 'Sharma', '2021/04/22 10:16:50', '2021/04/22 09:41:26', '2021/04/22 10:16:50', 0, 'kushalkumar.sharma@varian.com', 0, '0feebc5ca6099cb93d63d318551aa9be'),
(445, 0, 0, 1, 0, 'Amil ', 'Shah', '2021/04/22 10:17:31', '2021/04/22 10:07:11', '2021/04/22 10:17:31', 0, 'shah.amil@siemens-healthineers.com', 0, '2041ba236ecffa402266979167f08571'),
(446, 0, 0, 1, 0, 'Milind ', 'JAdhav', '2021/04/22 10:18:19', '2021/04/22 11:34:11', '2021/04/22 10:18:19', 0, 'milind.jadhav@varian.com', 0, '1e7190676605d0bcaf95061f36e46100'),
(447, 0, 1, 0, 0, 'jemima', 'daniel', '2021/04/22 10:19:19', '2021/04/22 11:30:15', '2021/04/22 10:19:19', 0, 'jemima.daniel@varian.com', 0, 'dde1dccad1bd15195412211eb81d5063'),
(448, 0, 0, 1, 0, 'Bithin', 'Roy Chowdhury', '2021/04/22 10:19:27', '2021/04/22 11:30:28', '2021/04/22 10:19:27', 0, 'bithin.roychowdhury@siemens-healthineers.com', 0, '7963d0a63ab4212e28090638f462d3ab'),
(449, 0, 0, 1, 0, 'GOPAL KRISHNA', 'SRIVASTAVA', '2021/04/22 10:20:09', '2021/04/22 21:02:39', '2021/04/22 10:20:09', 1, 'gopal.srivastava@siemens-healthineers.com', 0, 'ba49b4141ea0b2b0123c96a8b9120a8e'),
(450, 0, 1, 0, 0, 'Shradha', 'Savla', '2021/04/22 10:21:09', '2021/04/22 10:01:10', '2021/04/22 10:21:09', 0, 'shradha.savla@varian.com', 0, '001906ce488269f4cca14ae705b1a815'),
(451, 0, 1, 0, 0, 'Rosebud', 'Gomes', '2021/04/22 10:21:31', '2021/04/22 10:01:31', '2021/04/22 10:21:31', 0, 'rosebud.gimes@siemens-healthineers.com', 0, '03618ec15dafb68111974f1217aa46ea'),
(452, 0, 0, 1, 0, 'Manoj', 'Lokare', '2021/04/22 10:22:13', '2021/04/22 11:30:20', '2021/04/22 10:22:13', 0, 'Manoj.Lokare@varian.com', 0, '4c8724d3da6543e586a30fa144e0897e'),
(453, 0, 0, 1, 0, 'Vishnu', 'Prasad', '2021/04/22 10:22:13', '2021/04/22 10:01:14', '2021/04/22 10:22:13', 0, 'vishnuprasad.tv@siemens-healthineers.com', 0, '3cd89ce3e9c8d4c9949e421fea5305fc'),
(454, 0, 0, 1, 0, 'ABHIJIT', 'GHOSH', '2021/04/22 10:22:57', '2021/04/22 11:04:04', '2021/04/22 10:22:57', 0, 'abhijit.ag.ghosh@siemens-healthineers.com', 0, '998f8390cae0f81b1df8194fc8a3c197'),
(455, 0, 0, 0, 1, 'Manohar', 'Kollegal', '2021/04/22 10:23:16', '2021/04/22 11:30:23', '2021/04/22 10:23:16', 0, 'manohar.kollegal@siemens-healthineers.com', 0, 'f4b3159f06f67d525433911def6444dc'),
(456, 0, 0, 1, 0, 'Aman', 'Tiwari', '2021/04/22 10:25:10', '2021/04/22 10:01:10', '2021/04/22 10:25:10', 0, 'aman.tiwari@varian.com', 0, '178942a48ecb801b9cab553b9ad95193'),
(457, 0, 0, 1, 0, 'Purushottam', 'Awasthi', '2021/04/22 10:26:10', '2021/04/22 10:01:11', '2021/04/22 10:26:10', 0, 'purushottam.awasthi@varian.com', 0, '8b076e2326c63346e6fb0b47f119b4b5'),
(458, 1, 0, 0, 0, 'Lokesh', 'Agrawal', '2021/04/22 10:26:49', '2021/04/22 11:30:15', '2021/04/22 10:26:49', 0, 'lokesh.agarwal@varian.com', 0, '71c0e0422734fd3aab7a8d3a65f33b72'),
(459, 1, 0, 0, 0, 'Nishi', 'Satyawali', '2021/04/22 10:27:20', '2021/04/22 09:59:33', '2021/04/22 10:27:20', 0, 'nishi.satyawali@siemens-healthineers.com', 0, '73738cf68b2d7fc20832560027a1fa8b'),
(460, 0, 0, 1, 0, 'Ashwini ', 'Kumar', '2021/04/22 10:28:52', '2021/04/22 09:25:44', '2021/04/22 10:28:52', 0, 'ashwinikumar@siemens-healthineers.com', 0, '6be2ebe54cb14ee362f008cb821da416'),
(461, 0, 0, 1, 0, 'Surajkumar ', 'Chandrasekharan', '2021/04/22 10:29:37', '2021/04/22 09:43:48', '2021/04/22 10:29:37', 0, 'c.suraj_kumar@siemens-healthineers.com', 0, '628c3158504fd5ba126b8a566e26baf6'),
(462, 0, 0, 0, 1, 'Deepa', 'Nebhnani', '2021/04/22 10:31:58', '2021/04/22 10:00:59', '2021/04/22 10:31:58', 0, 'deepa.nebhnani@siemens-healthineers.com', 0, '04f590307cbde8c744a8c9e7cc177856'),
(463, 0, 0, 1, 0, 'Singaravelu', 'V B', '2021/04/22 10:32:26', '2021/04/22 11:30:13', '2021/04/22 10:32:26', 0, 'vb.singaravelu@siemens-healthineers.com', 0, 'ba39223ff0847b145793a05e171e8260'),
(464, 0, 0, 1, 0, 'Subrata ', 'Patnaik ', '2021/04/22 10:49:50', '2021/04/22 10:01:20', '2021/04/22 10:33:19', 0, 'subrata.patnaik@siemens-healthineers.com', 0, '4bf7b69327866cc98ea856ccdddcce80'),
(465, 0, 0, 1, 0, 'Gourava', 'Taraphder ', '2021/04/22 10:33:43', '2021/04/22 11:15:09', '2021/04/22 10:33:43', 0, 'gourava.taraphder@siemens-healthineers.com', 0, '0e3c30a9f13182ffd1b120362830b4b6'),
(466, 0, 1, 0, 0, 'Prema', 'Kathait', '2021/04/22 10:34:17', '2021/04/22 11:30:11', '2021/04/22 10:34:17', 0, 'prema.kathait@siemens-healthineers.com', 0, 'd25eea9015a60a2afdcf674f9489e09b'),
(467, 0, 0, 1, 0, 'Pranav', 'Patil', '2021/04/22 10:36:06', '2021/04/22 10:07:16', '2021/04/22 10:36:06', 0, 'pranav-patil@siemens-healthineers.com', 0, 'ea1bd4305f6099591e15ed2ad9f532eb'),
(468, 0, 0, 1, 0, 'Parag', 'Kishor', '2021/04/22 10:43:32', '2021/04/22 09:59:33', '2021/04/22 10:41:08', 0, 'parag.kishor@siemens-healthineers.com', 0, '1db071ab73f61588c0f2d91c39878ced'),
(469, 0, 0, 1, 0, 'sushant ', 'dhir', '2021/04/22 10:42:03', '2021/04/22 10:01:03', '2021/04/22 10:42:03', 0, 'sushant.dhir@varian.com', 0, 'f7b388f07b0f0bca8c59ce19eb519f68'),
(470, 0, 0, 1, 0, 'Vishal ', 'Gudi', '2021/04/22 10:42:47', '2021/04/22 10:00:47', '2021/04/22 10:42:47', 0, 'vishal.gudi@siemens-healthineers.com', 0, '549fbe590c51004b48c835c0db6fb9b6'),
(471, 1, 0, 0, 0, 'Basumitra', 'Rao Mukherjee', '2021/04/22 10:42:53', '2021/04/22 11:30:13', '2021/04/22 10:42:53', 0, 'basumitra.raomukherjee@siemens-healthineers.com', 0, 'b9b2b0c1f5b43a6f5ad3072a544dc059'),
(472, 0, 0, 0, 0, 'Parag', 'Kishor', '2021/04/22 10:43:32', '2021/04/22 09:59:33', '2021/04/22 10:43:32', 0, 'parag.kishor@siemens-healthineers.com', 0, '74252dbf38e8375d322044752eff28c5'),
(473, 0, 0, 1, 0, 'Venkatnarayanan ', 'K', '2021/04/22 10:44:09', '2021/04/22 09:17:39', '2021/04/22 10:44:09', 0, 'venkatnarayanan.k@siemens-healthineers.com', 0, 'b730e3486f079f66e08fde406c95539a'),
(474, 0, 0, 0, 0, 'Abhishek', 'Sinha', '2021/04/22 10:44:18', '2021/04/22 13:48:58', '2021/04/22 10:44:18', 1, 'abhishekkumar.sinha@siemens-healthineers.com', 0, 'ca61c7989548034536075a5e44aec664'),
(475, 0, 0, 1, 0, 'Yash', 'Hemrajani', '2021/04/22 10:44:19', '2021/04/22 11:30:13', '2021/04/22 10:44:19', 0, 'yash.hemrajani@siemens-healthineers.com', 0, '624a6d9cf1bf07065a9be6dcec60e154'),
(476, 0, 0, 0, 0, 'Chinmay', 'Desau', '2021/04/22 10:45:20', '2021/04/22 09:27:20', '2021/04/22 10:45:20', 0, 'desai.chinmay@siemens-healthineers.com', 0, '444a2a80e9a935088a40d440fb8ff3b5'),
(477, 0, 0, 1, 0, 'Balaji', 'Jagannathan', '2021/04/22 11:08:27', '2021/04/22 11:30:10', '2021/04/22 10:46:59', 0, 'balaji.jagannathan@varian.com', 0, '931a73b90c58968f6517900563182b72'),
(478, 0, 0, 1, 0, 'Vaibhav', 'Mhatre', '2021/04/22 10:47:21', '2021/04/22 11:30:14', '2021/04/22 10:47:21', 0, 'vaibhav.mhatre@varian.com', 0, 'e8dba7757be6babaeff1c545aced6aa0'),
(479, 0, 0, 1, 0, 'Naveen', 'P.S. ', '2021/04/22 10:48:42', '2021/04/22 10:00:42', '2021/04/22 10:48:42', 0, 'ps.naveen@siemens-healthineers.com', 0, 'b55b04779ec67276131956f998b0a4ac'),
(480, 0, 0, 1, 0, 'Subrata ', 'Patnaik ', '2021/04/22 10:49:50', '2021/04/22 10:01:20', '2021/04/22 10:49:50', 0, 'subrata.patnaik@siemens-healthineers.com', 0, '91e01f742d898a45b2dc1cfa130d11f9'),
(481, 0, 0, 1, 0, 'Kiran', 'Ramabhadran', '2021/04/22 10:49:56', '2021/04/22 11:30:26', '2021/04/22 10:49:56', 0, 'kiran.ramabhadran@siemens-healthineers.com', 0, '47f9f9059d134654f6095b85e4025550'),
(482, 0, 0, 1, 0, 'Kalpesh', 'Bhatt', '2021/04/22 10:52:12', '2021/04/22 09:52:43', '2021/04/22 10:52:12', 0, 'kalpesh.bhatt@siemens-healthineers.com', 0, '6c0454356f09eee80da7a8450a23d2c1'),
(483, 0, 0, 1, 0, 'Ashish', 'Joshi', '2021/04/22 10:52:17', '2021/04/22 10:06:36', '2021/04/22 10:52:17', 0, 'ashish.joshi@siemens-healthineers.com', 0, '9ccf517dcc03fcda072d80f52c70439b'),
(484, 0, 0, 1, 0, 'Venkatnarayanan ', 'K', '2021/04/22 10:55:43', '2021/04/22 10:00:59', '2021/04/22 10:55:43', 0, 'vrnkatnarayanan.k@siemens-healthineers.com', 0, 'a78b54e36e2699f3229927544cebaf45'),
(485, 0, 0, 1, 0, 'Balaji', 'Jagannathan', '2021/04/22 11:08:27', '2021/04/22 11:30:10', '2021/04/22 11:03:13', 0, 'balaji.jagannathan@varian.com', 0, '1aa7f6d927ef5099901c17d3d9adb52a'),
(486, 0, 0, 1, 0, 'DEBASHIS', 'BISWAS', '2021/04/22 11:03:33', '2021/04/22 09:43:03', '2021/04/22 11:03:33', 0, 'debashis.biswas@varian.com', 0, 'fa731e11a9c0c77d024ccf315349cf39'),
(487, 0, 0, 1, 0, 'Halim', 'Ansari', '2021/04/22 11:04:20', '2021/04/22 11:30:29', '2021/04/22 11:04:20', 0, 'halim.ansari@siemens-healthineers.com', 0, '9e94284c8ef5819a9c0c337b5e67553e'),
(488, 0, 0, 0, 0, 'Vivek', 'Kadam', '2021/04/22 11:05:19', '2021/04/22 10:00:50', '2021/04/22 11:05:19', 0, 'vivek.kadam@varian.com', 0, 'f1ad67efcbe7bef52194ae66187cc62f'),
(489, 0, 0, 1, 0, 'Dinesh', 'B', '2021/04/22 11:07:51', '2021/04/22 10:00:51', '2021/04/22 11:07:51', 0, 'dinesh.balasundarababu@siemens-healthineers.com', 0, '897f181331b9a7317c925251e681e276'),
(490, 0, 0, 0, 0, 'Archana', 'Hattangadi ', '2021/04/22 11:07:57', '2021/04/22 10:00:58', '2021/04/22 11:07:57', 0, 'archana.hattangadi@siemens-healthineers.com', 0, '5952703879d1afbb5803094dfea173c6'),
(491, 0, 0, 1, 0, 'Balaji', 'Jagannathan', '2021/04/22 11:08:27', '2021/04/22 11:30:10', '2021/04/22 11:08:27', 0, 'balaji.jagannathan@varian.com', 0, '216a96b85ab79bfff13ce42840f35b55'),
(492, 0, 0, 0, 0, 'Rahul', 'C', '2021/04/22 11:08:52', '2021/04/22 09:58:52', '2021/04/22 11:08:52', 0, 'rahul.chhabria@siemens-healthineers.com', 0, '0e65f0b60583690b0eaf1e2fc61c438b'),
(493, 0, 0, 0, 0, 'Sumit', 'Ghosh', '2021/04/22 11:10:37', '2021/04/22 11:11:36', '2021/04/22 11:10:37', 0, 'sumit.ghosh@siemens-healthineers.com', 0, '35cab140f091643ff1f32d6f8b5eb25b'),
(494, 1, 0, 0, 0, 'Khushboo', 'Bhadani', '2021/04/22 11:11:59', '2021/04/22 11:30:23', '2021/04/22 11:11:59', 0, 'khushboo.bhadani@siemens-healthineers.com', 0, '69e7870c519e7ffda0bb088bcc5eeb08'),
(495, 0, 1, 0, 0, 'geetha', 'v', '2021/04/22 11:12:09', '2021/04/22 10:02:10', '2021/04/22 11:12:09', 0, 'geetha.vishwanathan@varian.com', 0, 'edd9581c5467967893abee1be4dbe058'),
(496, 0, 0, 1, 0, 'BHAGIRATH', 'YADAV', '2021/04/22 11:20:23', '2021/04/22 10:01:23', '2021/04/22 11:20:23', 0, 'bhagirath.yadav@varian.com', 0, 'c4a2312cf10f0ff4fdbb78e46dcbc649'),
(497, 1, 0, 0, 0, 'Katyayani', 'Mishra', '2021/04/22 11:59:23', '2021/04/22 22:39:56', '2021/04/22 11:59:23', 1, 'katyayani.mishra@siemens-healthineers.com', 0, '291313d590d3e8a2d615c872cae3d550'),
(498, 0, 0, 1, 0, 'Sanjay', 'David', '2021/04/22 15:08:07', '2021/04/22 15:09:07', '2021/04/22 15:08:07', 1, 'sanjay.david@siemens-healthineers.com', 0, '976d70d18c7714c7aebbd0dcfdb5003a'),
(499, 0, 0, 1, 0, 'KALYAN PRATAP ', 'KOLLI', '2021/04/22 16:22:35', '2021/04/22 16:22:57', '2021/04/22 16:22:35', 0, 'kalyan.kolli@varian.com', 0, '94af64bc58201cc8d8a0bca163d5f159'),
(500, 0, 0, 0, 0, 'Mustaq Ahmed', 'Ansary', '2021/04/24 15:50:48', '2021/04/24 15:51:10', '2021/04/24 15:50:48', 0, 'mustaq.ansary@siemens-healthineers.com', 0, 'cb5a7db2914969795d4e09ccc23c21ab'),
(501, 0, 0, 1, 0, 'Ishwar', 'Ganwani', '2021/04/25 13:00:58', '2021/04/25 13:01:58', '2021/04/25 13:00:58', 1, 'ishwar.ganwani@varian.com', 0, 'a7a1749f7164cc6a99fa6f0447c8b6a4'),
(502, 0, 0, 1, 0, 'Amit', 'Khann', '2021/04/28 08:17:55', '2021/04/28 07:33:57', '2021/04/28 08:17:55', 1, 'amit.khanna@siemens-healthineers.com', 0, '3ae95af1bb8fb33378d476974643ac9b'),
(503, 1, 0, 0, 0, 'Viraj', 'Khot', '2021/06/17 14:58:16', '2021/06/17 14:59:16', '2021/06/17 14:58:16', 1, 'viraj@coact.co.in', 0, '35a05cc5ded7e7ddf0fef4d597ea108f'),
(504, 0, 1, 0, 0, 'neeraj', 'reddy', '2021/08/05 08:40:38', '2021/08/05 08:42:39', '2021/06/18 10:13:06', 1, 'neeraj@coact.co.in', 0, 'da89c94babcc09a1c1b9baff4a5e4a7e'),
(505, 0, 0, 1, 0, 'Arjun', 'Vazhathattil ', '2021/06/19 11:46:15', '2021/06/19 10:17:46', '2021/06/19 11:46:15', 1, 'aejun.vazhathattil@varian.com', 0, '070b9b572ced710f6417eaf28271e803'),
(506, 0, 0, 1, 0, 'Arjun ', 'Vazhathattil ', '2021/06/19 11:47:56', '2021/06/19 11:48:56', '2021/06/19 11:47:56', 1, 'arjun.vazhathattil@varian.com', 0, '5c143484aa8ba4e6b0247d20f085b5ed'),
(507, 0, 0, 1, 0, 'Prajith', 'TA', '2021/06/19 11:50:05', '2021/06/19 11:51:05', '2021/06/19 11:50:05', 1, 'prajith.ta@varian.com', 0, 'b42c9de8061f8faabda31ab0642baffd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=508;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
