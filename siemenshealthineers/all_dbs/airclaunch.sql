-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 10, 2022 at 06:24 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airclaunch`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_empid` varchar(200) NOT NULL,
  `user_question` varchar(200) NOT NULL,
  `asked_at` varchar(200) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_empid`, `user_question`, `asked_at`, `speaker`, `answered`) VALUES
(3, 'Miss Rosebud  Gomes', 'rosebud.gomes@gmail.com', 'Test, What is your name?', '2020/12/08 16:37:24', 1, 0),
(4, 'Miss Rosebud  Gomes', 'rosebud.gomes@gmail.com', 'Hello test', '2020/12/08 16:37:40', 0, 0),
(5, 'Mr Akshat Jharia', 'akshat@coact.co.in', 'Hi', '2020/12/09 19:29:38', 0, 0),
(6, 'Mr Sanket Kathe', 'sanketrkathe@gmail.com', 'Hello, when will the solution be available?', '2020/12/09 19:34:07', 0, 0),
(7, 'Mr Akshat Jharia', 'akshatjharia@gmail.com', 'Hi', '2020/12/11 15:57:17', 0, 0),
(8, 'Mr Murugesapandi KS KS', 'murugesapandi.ks@siemens-healthineers.com', 'How does AI RAD companion solution adapt to Indian patient populations? Has the solution been trained with Indian datasets? ', '2020/12/11 17:19:13', 0, 0),
(9, 'Mr Murugesapandi KS KS', 'murugesapandi.ks@siemens-healthineers.com', 'What would be the best answer, AI Rad companian helps in better workflow or better clinical accuracy? ', '2020/12/11 17:20:51', 0, 0),
(10, 'Mr Murugesapandi KS KS', 'murugesapandi.ks@siemens-healthineers.com', 'What is accuracy rate of AI RAD Companion? Is there any official paper confirming the same? ', '2020/12/11 17:21:40', 0, 0),
(11, 'Dr Dinesh Makuny', 'drmdinesh@mvrccri.co', 'How far we have progressed in image texture analysis with AI to evaluate responses during radiationtherapy  and use them as predictive markers?', '2020/12/11 17:47:15', 0, 0),
(12, 'Dr Hirdesh Sahni', 'drhsahni@gmail.com', 'The pneumothorax was not evident. \r\nIt seems to be the medial margin of the scapula. Could you pl show it again and mark it.', '2020/12/11 18:02:51', 0, 0),
(13, 'Dr Dinesh Makuny', 'drmdinesh@mvrccri.co', 'Is there any comparison done with various population to see the variability and improve  the deep learning process involved in the anatomy registrations of OARs\r\n', '2020/12/11 18:10:24', 0, 0),
(14, 'Dr Atul Kapoor', 'masatulak@aim.com', 'Does airc also  comment on quality of  ct data acquisition.\r\nCacs requires ekg gating so does airc comment on cacs on non gated studies\r\n\r\n', '2020/12/11 18:27:16', 0, 0),
(15, 'Dr Nanjundan Murali', 'drnmurali@gmail.com', 'Does coronary artery stenosis measurement reliable with A.I.?\r\nDr Murali\r\nCoimbatore', '2020/12/11 18:30:36', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reactions`
--

CREATE TABLE `tbl_reactions` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `reaction` varchar(50) NOT NULL,
  `reaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `ms` int(11) NOT NULL,
  `miss` int(11) NOT NULL,
  `mr` int(11) NOT NULL,
  `dr` int(11) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `organisation` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `login_date` varchar(200) DEFAULT NULL,
  `logout_date` varchar(200) DEFAULT NULL,
  `joining_date` varchar(200) NOT NULL,
  `logout_status` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `is_varified` int(11) NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `ms`, `miss`, `mr`, `dr`, `fname`, `lname`, `mobile`, `organisation`, `city`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `is_varified`, `password`) VALUES
(2, 0, 0, 1, 0, 'Akshat', 'Jharia', '7204420017', 'COACT', 'Bangalore', '2020/12/11 14:35:15', '2021/04/21 08:53:17', '2020/12/02 10:25:25', 0, 'akshat@coact.co.in', 0, 'e40c75b134ffca237d6c8fb434c5c8d8'),
(4, 0, 1, 0, 0, 'Sujatha ', 'Natesh ', '9845563760', 'COACT ', 'bangalore ', NULL, NULL, '2020/12/02 14:04:52', 0, 'sujatha@coact.co.in', 0, 'ee99dc38e4c1f660a6b195a444c05df7'),
(5, 0, 0, 0, 0, 'Pooja', 'Jaiswal', '9768161921', 'COACT', 'Mumbai', '2020/12/11 18:14:20', '2020/12/11 18:19:42', '2020/12/03 10:56:38', 0, 'pooja@coact.co.in', 0, 'c078075a63bc462dce899cf7818f22f6'),
(6, 0, 0, 1, 0, 'Viraj', 'Khot', '9765875731', 'COACT', 'Pune', NULL, NULL, '2020/12/04 09:04:08', 0, 'viraj@coact.co.in', 0, 'e414590de62a79bd682dc52a4818f3c7'),
(7, 0, 1, 0, 0, 'Rosebud ', 'Gomes', '9821196504', 'Siemens Healthineers', 'Mumbai', '2020/12/08 16:33:28', '2020/12/08 16:46:59', '2020/12/04 14:14:54', 0, 'rosebud.gomes@gmail.com', 0, '4dea717753c03f12240f7f8b7d9ee133'),
(8, 0, 0, 1, 0, 'Shreekant ', 'Mokashi ', '9234521053', 'MTMH ', 'Jamshedpur ', NULL, NULL, '2020/12/04 15:21:15', 0, 'ssmokashi@mtmh.co.in', 0, '4c7dc99d1033480d11d28146ba3ee043'),
(9, 0, 0, 0, 1, 'Joe', 'Schoepf', '+1-843-810-0450', 'MUSC', 'Charleston ', NULL, NULL, '2020/12/04 16:54:58', 0, 'schoepf@musc.edu', 0, '9710cd359864def8370bd885bd57f603'),
(10, 0, 0, 0, 1, 'Atul', 'Kapoor', '+911832221136', 'Advanced imaging', 'Amritsar', '2020/12/11 17:45:57', '2020/12/11 18:51:18', '2020/12/04 17:13:31', 0, 'masatulak@aim.com', 0, '5c8f99b47e69899f640d26cc33b51dc3'),
(11, 0, 0, 0, 1, 'Birgi', 'Tamersoy', '+4915201887473', 'Siemens Healthineers', 'Erlangen', NULL, NULL, '2020/12/04 17:15:57', 0, 'birgi.tamersoy@siemens-healthineers.com', 0, 'd6262f9a774b0ccd632ccc8d0cd08d6b'),
(12, 0, 0, 0, 1, 'Nitin', 'Kumar ', '+919811441844', 'Modern Diagnostic and Research centre ', 'Gurgaon ', NULL, NULL, '2020/12/04 17:45:53', 0, 'yadavnitindr@gmail.com', 0, '1014ef70b88361497373bb99d33b375e'),
(13, 0, 0, 1, 0, 'Rajiv Sikka', 'Sikka', '9873032975', 'Medanta', 'NEW DELHI', NULL, NULL, '2020/12/04 18:23:12', 0, 'Rajiv.Sikka@Medanta.org', 0, '191df2935596469c370714fd6ebc9938'),
(14, 0, 0, 0, 1, 'Dinesh Makuny', 'Makuny', '+919847317361', 'MVRCCRI', 'Kozhikode', NULL, NULL, '2020/12/04 18:24:25', 0, 'makuny@gmail.com', 0, '87c3f7d2e0d813843640778f0a92cad1'),
(15, 0, 0, 0, 1, 'Sumedha', 'Kumanayake ', '718184365', 'CSHW', 'Colombo', NULL, NULL, '2020/12/04 18:27:32', 0, 'sumedha_kumanayake@yahoo.com', 0, '2562e5f5b831885383ba2a77616b434f'),
(16, 0, 0, 1, 0, 'Niraj', 'Garg', '9350000878', 'SHPL', 'Gurgaon ', NULL, NULL, '2020/12/04 18:27:37', 0, 'niraj.garg@siemens-healthineers.com', 0, 'eaa164d37ed284d0cbc0e72120911ccc'),
(17, 0, 0, 1, 0, 'Ivo', 'Driesser', '+491732533840', 'Siemens Healthineers', 'Erlangen', NULL, NULL, '2020/12/04 18:39:06', 0, 'ivo.driesser@siemens-healthineers.com', 0, 'c357dca77dabca7a491ba13c91c9ed59'),
(18, 0, 1, 0, 0, 'Echo', 'Shan', '01723901707', 'Siemens Healthcare', 'Erlangen', NULL, NULL, '2020/12/04 19:07:51', 0, 'echo.shan@siemens-healthineers.com', 0, '71c42d0576696943f8457e3cb8b7353b'),
(19, 0, 0, 1, 0, 'Joby', 'C Jacob', '8105667755', 'Siemens Healthcare', 'Bangalore', '2020/12/11 17:16:13', '2020/12/11 18:47:44', '2020/12/04 19:15:04', 0, 'joby.jacob@siemens-healthineers.com', 0, 'd1751424afb1b5d9494c7e18c3bf2e5e'),
(20, 0, 0, 1, 0, 'Joydeep', 'Bhattacharjee', '9831152034', 'DI', 'Kolkata', '2020/12/11 17:51:41', '2020/12/11 18:27:12', '2020/12/04 19:18:06', 0, 'joydeep.bhattacharjee@siemens-healthineers.com', 0, 'c2b716340e4b8f178ec7e345a698a5d1'),
(21, 0, 0, 0, 1, 'rajavel kannaiyan', 'kannaiyan', '9994384444', 'velan speciality hospitals', 'trichy', NULL, NULL, '2020/12/04 19:18:42', 0, 'rajavelk@velanhospitals.com', 0, '612b38d422476fb00ea968c330076c56'),
(22, 0, 0, 1, 0, 'Sainesh', 'Patekar', '9822113958', 'Siemens Healthineers', 'Pune', '2020/12/11 17:59:31', '2020/12/11 18:48:01', '2020/12/04 19:20:35', 0, 'sainesh.patekar@siemens-healthineers.com', 0, '0612f494afdb3ffb1c7fef5cbf093972'),
(23, 0, 0, 0, 1, 'Nimish ', 'Sharma', '9978662500', 'CIMS ', 'Ahmedabad ', NULL, NULL, '2020/12/04 19:29:27', 0, 'nimish.dr@gmail.com', 0, 'bbd72ef97bae134ca1500364a25ecb88'),
(24, 0, 0, 1, 0, 'Arjun', 'Somireddy', '7760099555', 'AIG hopsital', 'Hyderabad ', NULL, NULL, '2020/12/04 19:58:59', 0, 'arjunsomireddy@gmail.com', 0, '2202bca492fef087a727ce1df5fd5b9b'),
(25, 0, 1, 0, 0, 'Nisha Hirani', 'Hirani', '7621062240 ', 'Krishna hospital', 'morbi ', NULL, NULL, '2020/12/04 20:02:22', 0, 'nhirani335@rku.ac.in', 0, '9051de9816f60f34134696f5a3c10feb'),
(26, 0, 0, 1, 0, 'Ashutosh', 'Sharma', '9711099343', 'SHPL', 'Delhi', '2020/12/11 18:17:44', '2020/12/11 18:47:06', '2020/12/04 20:12:08', 0, 'sharma.ashutosh@siemens-healthineers.com', 0, '66cf99e6b4b2ba8f705ad3d9075d4d71'),
(27, 0, 0, 1, 0, 'Sathish', 'B', '9944935423', 'SHPL', 'Coimbatore', NULL, NULL, '2020/12/04 20:34:48', 0, 'sathish.b@siemens-healthineers.com', 0, '6d65d5c750a5f0f43bc8de8fb4b3114f'),
(28, 0, 0, 1, 0, 'Murugesapandi KS', 'KS', '09894918443', 'Siemens Healthineeers', 'Chennai', '2020/12/11 17:12:49', '2020/12/11 18:47:37', '2020/12/04 20:40:40', 0, 'murugesapandi.ks@siemens-healthineers.com', 0, '370ebfcc7c0404c0f4506ca6ea434788'),
(29, 1, 0, 0, 0, 'Sujata', 'Naidu', '9910902809', 'Siemens Healthineers', 'Gurgaon', '2020/12/11 18:12:24', '2020/12/11 18:46:56', '2020/12/04 20:46:28', 0, 'sujata.naidu@siemens-healthineers.com', 0, 'b0ad89d4f361d90b159202537f621964'),
(30, 0, 1, 0, 0, 'Anu', 'Lidia', '9003416225', 'SHPL', 'Chennai', NULL, NULL, '2020/12/04 20:57:13', 0, 'anulidia@gmail.com', 0, 'd2bd0b6b33128ef15db0bb47ae183c55'),
(31, 0, 0, 0, 1, 'SHELLEY', 'SIMON', '9884055655', 'APOLLO HOSPITALS', 'CHENNAI', NULL, NULL, '2020/12/04 21:40:25', 0, 'shelleysimon@rediffmail.com', 0, 'decbea925554d539b6aac04e85b7fa39'),
(32, 1, 0, 0, 0, 'Steven', 'Bell', '+61488400348', 'Siemens Healthineers', 'Melbourne ', NULL, NULL, '2020/12/05 01:11:10', 0, 'steven.bell@siemens-healthineers.com', 0, '89ebfc3962ece4c6e03eeb30ac5a59d8'),
(33, 0, 0, 1, 0, 'Mintu', 'Pak', '9831150228', 'SHPL', 'Kolkata ', '2020/12/11 17:49:09', '2020/12/11 18:53:30', '2020/12/05 03:56:50', 0, 'mintu.pal@siemens.com', 0, 'ce1c9ec23db1be0e1996664b5ff25434'),
(34, 0, 0, 1, 0, 'Sanket', 'Kathe', '9769539333', 'Abc hospitals ', 'Lucknow', '2020/12/11 17:18:13', '2020/12/11 18:53:04', '2020/12/05 05:11:59', 0, 'sanketrkathe@gmail.com', 0, '679867b5309878d96ba6ed1a9ce4614a'),
(35, 0, 0, 1, 0, 'Ankur', 'Kapoor', '9501001605', 'Siemens Healthcare ', 'Lucknow ', '2020/12/11 18:14:22', '2020/12/11 18:56:02', '2020/12/05 05:19:42', 0, 'kapoor.ankur@siemens-healthineers.com', 0, '28400375883924bc361d215971125396'),
(36, 0, 0, 0, 1, 'Arijit', 'Ghosh', '8007658278', 'Armed Forces ', 'Pune', NULL, NULL, '2020/12/05 06:09:24', 0, 'arijitkumar@rediffmail.com', 0, '854f5321af8621f975942f7634737067'),
(37, 0, 0, 1, 0, 'Rakesh', 'Kumar', '+919910074177', 'SHPL', 'Gurgaon', NULL, NULL, '2020/12/05 06:35:36', 0, 'rakesh.kumar@siemens-healthineers.com', 0, 'd39e52cf25f2660323f68b61f6faeeaa'),
(38, 0, 0, 1, 0, 'Sudhir', 'KN', '9845533266', 'SHPL', 'Bangalore', NULL, NULL, '2020/12/05 06:43:06', 0, 'kn.sudhir@siemens-healthineers.com', 0, 'a78a72cf184e719c0eafc8a2bbf2e067'),
(39, 0, 0, 0, 1, 'Jyoti ', 'Arora', '9599944196', 'Medanta', 'Gurugram ', NULL, NULL, '2020/12/05 06:43:59', 0, 'dr.jyotiarora@yahoo.co.uk', 0, '0b62e7cefb4c53d3862c6e67a5841e7a'),
(40, 0, 0, 0, 1, 'Hare Krishna ', 'Pradhan', '9165008276', 'Srs diagnostic ', 'Raipur ', NULL, NULL, '2020/12/05 07:04:24', 0, 'dr.harekrishna@gmail.com', 0, '58a5aafb72fbc89e584994f914b4a2d7'),
(41, 0, 0, 1, 0, 'Kishan', 'Raj', '9895353222', 'Siemens Healthcare Pvt. Ltd.', 'Kochi', '2020/12/11 17:25:47', '2020/12/11 18:47:23', '2020/12/05 07:24:07', 0, 'kishan.raj@siemens-healthineers.com', 0, '7714b2e59b8c3aa93327c35ee651aa05'),
(42, 0, 0, 0, 1, 'Mohammad Nazrul', 'Islam', '+8801731041921', 'National institute of Neuro sciences & Hospital', 'Dhaka', NULL, NULL, '2020/12/05 09:36:57', 0, 'drnazrulrad69@gmail.com', 0, 'f0d9f7a8c073d9dd25c428b6ab223138'),
(43, 0, 0, 0, 1, 'Atin', 'Kumar', '9810178605', 'AIIMS, ', 'New Delhi', NULL, NULL, '2020/12/05 09:47:22', 0, 'dratinkumar@gmail.com', 0, '5f48a09ea760313301f4c8ccec872e92'),
(44, 0, 0, 1, 0, 'Karanbir', 'Singh', '8283835389', 'Siemens Healthcare', 'Gurgaon', NULL, NULL, '2020/12/05 09:53:26', 0, 'karanbir.ahluwalia@siemens-healthineers.com', 0, 'd69b29e2634c7a309c7c4d4a741a3196'),
(45, 0, 0, 1, 0, 'SANJAY', 'DAVID', '9444382533', 'SIEMENS HEALTHCARE PVT. LTD.', 'CHENNAI', NULL, NULL, '2020/12/05 10:30:19', 0, 'sanjay.david@siemens-healthineers.com', 0, '6a19e88163bc2e08f960bf0c75c63f9c'),
(46, 0, 0, 1, 0, 'Jayanta', 'Mukherjee', '918879688408', 'Siemens Healthcare Pvt. Ltd.', 'Mumbai', NULL, NULL, '2020/12/05 11:08:38', 0, 'jayanta.mukherjee@siemens-healthineers.com', 0, '9736cde8e87476df23c6fd153433f57c'),
(47, 0, 0, 0, 1, 'Dileep', 'Kumar', '09897672185', 'Siemens Healthineers India', 'Bangalore', NULL, NULL, '2020/12/05 11:19:17', 0, 'dileep.utp@gmail.com', 0, 'ef82d370f9b1314977baf2a35eaaf0cb'),
(48, 0, 0, 0, 1, 'Dileep ', 'Kumar', '09897672185', 'Siemens Healthineers, India', 'Bangalore', '2020/12/11 17:25:38', '2020/12/11 18:58:10', '2020/12/05 11:20:18', 0, 'kumar.dileep@siemens-healthineers.com', 0, 'db03e45f907d4629cb77017b981dd749'),
(49, 0, 0, 1, 0, 'Vishnu', 'Priyan', '7539964260', 'Radiology ', 'Chennai', NULL, NULL, '2020/12/05 11:49:35', 0, 'vishnuvictores99@gmail.com', 0, 'b80a951cb38fa2a87c4ebc0b845f6782'),
(50, 0, 0, 0, 1, 'DEVENDRA SINGH', 'YADAV', '9811588338', 'MODERN DIAGNOSTIC & RESEARCH CENTRE', 'GURGAON', NULL, NULL, '2020/12/05 14:31:49', 0, 'dsyadavdr@gmail.com', 0, '2fc599690dccc3432cc7ab5c17415f1c'),
(51, 0, 0, 1, 0, 'Rahil', 'Shah', '9820012396', 'NM Medical', 'Mumbai ', NULL, NULL, '2020/12/05 14:33:18', 0, 'rahil.shah@nmmedical.com', 0, '6e3625e70cf8d3f10bfda28bb9f9bd4b'),
(52, 0, 0, 0, 1, 'Arunkumar Govindarajan', 'Govindarajan', '9940056116', 'Aarthi Scans and Labs', 'Chennai', '2020/12/11 18:34:16', '2020/12/11 18:38:48', '2020/12/05 14:49:08', 0, 'arunkumar@aarthiscans.com', 0, 'c74ec25a06f4032e0850f2984e234779'),
(53, 0, 0, 0, 1, 'Anil', 'Bhaya MD PGDHHM ', '8451032959', 'Independent consultant ', 'Mumbai', '2020/12/11 17:19:18', '2020/12/11 18:35:50', '2020/12/05 14:50:08', 0, 'bhaya45@gmail.com', 0, 'd163367bb443682de173ce098ccbbcf3'),
(54, 0, 0, 1, 0, 'mahendra kumar agarwal', 'agarwal', '+919967029931', 'GDC PLUS', 'HALOL', NULL, NULL, '2020/12/05 14:55:41', 0, 'cagoyal58@gmail.com', 0, '5a06d621ed34e6098c26b8fcdd77be62'),
(55, 0, 0, 0, 1, 'Balram ji', 'Omar', '9415026640', 'Aiims rishikesh ', 'Rishikesh ', NULL, NULL, '2020/12/05 16:49:04', 0, 'balram.micro@aiimsrishikesh.edu.in', 0, '3a2dc6b9087c8b92039d53442d3de1d5'),
(56, 0, 0, 0, 1, 'Onjal Taywade', 'Taywade', '09654824143', 'AIIMS', 'Bilaspur', NULL, NULL, '2020/12/05 17:19:04', 0, 'dronjal@gmail.com', 0, '1faabf157e6f779c1296244036cbb47f'),
(57, 0, 0, 1, 0, 'Kunhimohamed ', 'P', '971565118087', 'Siemens ', 'Dubai', NULL, NULL, '2020/12/05 17:25:06', 0, 'kunchu.mohamed@gmail.com', 0, 'f6c7c6933c0856ab56d5d82c258781dc'),
(58, 0, 0, 0, 1, 'Ghanshyam', 'ardeshana', '9825045721', 'super scan ', 'surat', NULL, NULL, '2020/12/05 18:02:49', 0, 'drghanshyam@gmail.com', 0, '03d13e08ae396768542be70803fdbf06'),
(59, 0, 0, 1, 0, 'Rajashyam', 'K', '9154889937', 'Apollo Telehealth', 'Hyderabad ', NULL, NULL, '2020/12/05 18:13:46', 0, 'rajashyamk@gmail.com', 0, '84556204fef182e2e8b5ca5a374ba421'),
(60, 0, 0, 0, 0, 'Rajoo', 'Narang', '8588871927', 'SHPL', 'Gurgaon ', '2020/12/11 18:29:48', '2020/12/11 18:45:34', '2020/12/05 20:45:22', 0, 'rajoo.narang@siemens-healthineers.com', 0, '67e3994e2d12caebd32db4cf204c8c30'),
(61, 0, 0, 0, 0, 'Arup', 'Ghosh', '9980056323', 'Wipro', 'Bangalore', NULL, NULL, '2020/12/05 21:55:40', 0, 'arupghosh.blr@gmail.com', 0, '18d81d89db1fe7ef010d1656d55e7a4f'),
(62, 0, 0, 0, 1, 'niraj', 'kanchankar', '9373670604', 'alexis hospital', 'nagpur', NULL, NULL, '2020/12/06 07:29:03', 0, 'greatmails@gmail.com', 0, 'b667bcba3a8e008692861b0393b1df5e'),
(63, 0, 0, 1, 0, 'SUBODH', 'DIVGI', '9920376313', 'SIEMENS ', 'MUMBAI', NULL, NULL, '2020/12/06 07:54:54', 0, 'Subodh.divgi@siemens-healthineers.com', 0, '7dd4f119788e00a899c5a02aa5f027a3'),
(64, 0, 0, 1, 0, 'Md Hafizur Rahman', 'Khan', '+8801711526888', 'Siemens Healthcare Ltd', 'Dhaka', NULL, NULL, '2020/12/06 08:22:17', 0, 'hafizur.rahman@siemens-healthineers.com', 0, 'ce2672406c12cb327ae2c950fde17883'),
(65, 0, 0, 0, 1, 'Gopal ', 'Dhoot ', '9823066077', 'Anushka scan centre, pune ', 'Pune', NULL, NULL, '2020/12/06 08:22:27', 0, 'gopaldhoot15@gmail.com', 0, '9f075066ce423de366e0b0a2ef6b813c'),
(66, 0, 0, 1, 0, 'Rahul', 'Chhabria', '9840587889', 'SHPL', 'Bangalore', NULL, NULL, '2020/12/06 09:42:28', 0, 'rahul.chhabria@siemens-healthineers.com', 0, '6a8690d4c23acf6ebeafe23ce711a6cf'),
(67, 0, 0, 0, 1, 'Girish', 'Ghodadara', '9913355621', 'Consultant radiologist', 'surat', NULL, NULL, '2020/12/06 10:07:32', 0, 'girishghodadara@yahoo.com', 0, 'bec468d16e18141a66d1f4d22846e224'),
(68, 0, 0, 1, 0, 'Dhananjay', 'Mainkar', '9769952213', 'SHPL', 'Mumbai', NULL, NULL, '2020/12/06 12:39:08', 0, 'dhananjay.mainkar@siemens-healthineers.com', 0, 'd61cdc781178adb2edc8c6f524310833'),
(69, 0, 0, 1, 0, 'Dipankar', 'Banerjee', '9831414882', 'Siemens Healthcare', 'Kolkata ', NULL, NULL, '2020/12/06 13:55:08', 0, 'dipankar.banerjee@siemens-healthineers.com', 0, '40c0b81f4086672331f3111dad250fd0'),
(70, 0, 0, 0, 1, 'Nilesh', 'Chandra', '7011295945', 'SSPHPGTI', 'Delhi', NULL, NULL, '2020/12/06 16:30:10', 0, 'drnileshchandra@gmail.com', 0, 'ff932a0b59027ceae31a863ecbeb1190'),
(71, 0, 0, 0, 1, 'RAJARSHI', 'BAHADUR', '09872612100', 'RadiologiX', 'Chandigarh', NULL, NULL, '2020/12/06 19:36:22', 0, 'rajarshibahadur@hotmail.com', 0, '9a7bd44cd5ef235effcda398cc1b004d'),
(72, 1, 0, 0, 0, 'Sangeeta', 'Bose', '09880206545', 'Karkinos Healthcare', 'Bengaluru', NULL, NULL, '2020/12/06 20:08:07', 0, 'sangeeta.bose@karkinos.in', 0, '656a4667af0d22905ca7b5a43fb13299'),
(73, 0, 0, 1, 0, 'Rakesh ', 'K', '8904860048', 'Healthcare ', 'Bengaluru', NULL, NULL, '2020/12/06 21:02:15', 0, 'raki291089@gmail.com', 0, '1953ff2d0eb85715ed0daff9ec38333e'),
(74, 0, 0, 1, 0, 'Brijesh ', 'Verma', '9971650038', 'SHPL', 'Varanasi', '2020/12/11 18:15:51', '2020/12/11 18:48:24', '2020/12/06 21:06:47', 0, 'brijesh.verma@siemens-healthineers.com', 0, 'cbcff351a6148bb1522e29f334d964b1'),
(75, 0, 0, 0, 1, 'Jayaraj ', 'Govindaraj', '9841014128', 'Apollo Hospitals', 'Chennai', NULL, NULL, '2020/12/06 23:07:49', 0, 'jayarajg@gmail.com', 0, 'cdf104fc9c41fd0703f8bebff57e2687'),
(76, 0, 0, 0, 1, 'Sandipan', 'De', '9916141465', 'Karkinos Healthcare', 'Bangalore', NULL, NULL, '2020/12/07 08:19:24', 0, 'sandipan.de@karkinos.in', 0, '28aca49d5092593a1089f61f912de0fd'),
(77, 0, 0, 1, 0, 'Surajkumar', 'Chandrasekharan ', '9500088843', 'SHPL', 'Chennai', '2020/12/11 17:17:03', '2020/12/11 18:48:04', '2020/12/07 08:36:42', 0, 'c.suraj_kumar@siemens-healthineers.com', 0, '3e4a3354cf59ea201feef5fe3944ff29'),
(78, 0, 0, 1, 0, 'Venkatnarayanan', 'K', '9930285601', 'Siemens Healthcare Pvt Ltd', 'Chennai', NULL, NULL, '2020/12/07 08:59:50', 0, 'venkatnarayanan.k@siemens-healthineers.com', 0, '5564d4480ecce4f602d61a47cee943dd'),
(79, 0, 0, 1, 0, 'Kailash Yagnik', 'Yagnik', '+919820702886', 'Siemens Healthcare Pv.t. Ltd.', 'Mumbai', '2020/12/11 18:03:09', '2020/12/11 18:47:40', '2020/12/07 09:11:40', 0, 'kailash.yagnik@siemens-healthineers.com', 0, '789cf0258ee30f9b956e195af59bdb58'),
(80, 0, 0, 1, 0, 'SANJEEV', 'BHATLI', '9953158138', 'SIEMENS HEALTHINEERS', 'MUMBAI', NULL, NULL, '2020/12/07 09:33:54', 0, 'SANJEEV.BHATLI@SIEMENS-HEALTHINEERS.COM', 0, '6a232af4481e50c89e38fe68fe612d75'),
(81, 0, 0, 0, 1, 'Bharat ', 'Aggarwal', '9810077484', 'Max Healthcare', 'New Delhi', '2020/12/11 17:32:49', '2020/12/11 18:21:22', '2020/12/07 09:38:56', 0, 'docbharat@gmail.com', 0, 'cc3219ab595c07e7ecb84162321cf107'),
(82, 0, 0, 1, 0, 'Brij Bhan', 'Sengar', '7506936806', 'Siemens Healthcare Private Limited', 'Gurugram', '2020/12/11 17:32:30', '2020/12/11 18:00:36', '2020/12/07 10:10:39', 0, 'brij.sengar@siemens-healthineers.com', 0, 'e5f1e2d6249ebd6f99fbc0a6a1b8fa9f'),
(83, 0, 0, 1, 0, 'Arshad', 'Kazi', '+918880916655', 'Siemens Healthineers', 'Bangalore', '2020/12/11 17:39:49', '2020/12/11 17:40:49', '2020/12/07 10:50:07', 0, 'arshad.kazi@siemens-healthineers.com', 0, '76533dfab79265708a4b6145151683f4'),
(84, 0, 0, 1, 0, 'Reynold', 'Roy', '+919611518064', 'Coact', 'Bangalore', '2020/12/11 15:28:33', '2020/12/11 15:37:09', '2020/12/07 16:25:30', 0, 'reynold@coact.co.in', 0, '2543ab9c58615f2f49655b60f6014039'),
(85, 0, 0, 1, 0, 'Prabhat', 'Kumar', '9811652607', 'Siemens Healthineers', 'Gurgaon', NULL, NULL, '2020/12/07 19:18:06', 0, 'kumar.prabhat@siemens-healthineers.com', 0, '9264995c3421193a5ed4127f40003882'),
(86, 0, 0, 1, 0, 'RAJ RISHI', 'GANGULY', '9933445565', 'Siemens Healthcare Pvt Ltd', 'KOLKATA', NULL, NULL, '2020/12/07 22:15:33', 0, 'raj.ganguly@siemens-healthineers.com', 0, 'b9e9f1a53ea2b52d58ed43b1c8730620'),
(87, 0, 0, 1, 0, 'Hugo', 'Antunes', '961725304', 'Siemens', 'Porto', '2021/01/07 23:52:59', '2021/01/07 23:53:59', '2020/12/08 04:47:13', 1, 'hugo.antunes@siemens.com', 0, 'a216d50b6aefabe07b115d006016c279'),
(88, 0, 0, 0, 1, 'Bharat ', 'Aggarwal', '9810077484', 'Max Healthcare', 'New Delhi', NULL, NULL, '2020/12/08 08:35:44', 0, 'bharat.aggarwal@maxhealthcare.com', 0, '90529e3aec604bd7cddfec26e78eb40b'),
(89, 1, 0, 0, 0, 'Dhiroj ', 'Barad', '9560875454', 'SHS India ', 'Mumbai ', NULL, NULL, '2020/12/08 11:12:29', 0, 'dhiroj.barad@siemens-healthineers.com', 0, 'c743fff8ae8807abbfb3e78ff810aee7'),
(90, 0, 0, 0, 1, 'Agam', 'Shrivastava', '9825177101', 'SRL Diagnostics', 'Vadodara', NULL, NULL, '2020/12/08 11:24:41', 0, 'agam.shrivastava@srl.in', 0, 'f2c1f31bfea4a753052cf327281147ff'),
(91, 0, 0, 0, 1, 'Saugata', 'Sen', '9830448007', 'Tata Medical Center', 'Kolkata ', NULL, NULL, '2020/12/08 11:49:11', 0, 'drsaugatasen@gmail.com', 0, 'c4300f19f165214f514eccaf3401eae9'),
(92, 0, 0, 1, 0, 'Subroto', 'Biswas', '9825010156', 'Siemens Healthcare Pvt Ltd', 'Mumbai', NULL, NULL, '2020/12/08 11:51:05', 0, 'subroto.biswas@siemens-healthineers.com', 0, 'b7692b0047957bb82ee64347279354ef'),
(93, 0, 0, 1, 0, 'Keyur', 'Bavishi ', '9824010063', 'SHPL', 'Ahmedabad', NULL, NULL, '2020/12/08 11:55:26', 0, 'Keyur.bavishi@siemens-healthineers.com', 0, 'b5cecac59394c9078d1479dffcebb987'),
(94, 0, 0, 0, 1, 'Vikrant', 'Agarwal', '09839268516', 'Aryavart MRI center, ', 'Gorakhpur', '2020/12/11 18:02:50', '2020/12/11 18:57:51', '2020/12/08 11:59:18', 0, 'vikrantagarwal9999@gmail.com', 0, '4e88747845b9c2dd8a76d5eea577451a'),
(95, 0, 0, 0, 1, 'Arvind', 'Ka.lra', '9417229760', 'Dr kalras mri scan centre', 'Pathankot', NULL, NULL, '2020/12/08 11:59:26', 0, 'drarvindkalra@yahoo.com', 0, '2b6dc15d57f34d96f158f70a89e12bcb'),
(96, 0, 0, 0, 1, 'Jagdish ', 'Chandra ', '9044466306', 'Rly', 'Moradabad ', NULL, NULL, '2020/12/08 12:02:39', 0, 'drjc64@gmail.com', 0, 'fa730a1ed565d2bb7e9cce242dcee339'),
(97, 0, 0, 0, 1, 'Ishan', 'Rudani', '9173659525', 'Radiologist', 'Ahmedabad', NULL, NULL, '2020/12/08 12:06:12', 0, 'ishrudani@gmail.com', 0, '9d744f1e6077f22c522c7b6143413c8f'),
(98, 0, 0, 0, 1, 'Jasmin', 'Shah', '9227798728', 'Kakadiya Hospital', 'Ahmedabad', NULL, NULL, '2020/12/08 12:15:39', 0, 'drjasminshah@yahoo.co.in', 0, '69eb629ca843c19a5f0f540d695ba8ea'),
(99, 0, 0, 0, 0, 'Pooja', 'Jaiswal', '9768161921', 'COACT', 'Mumbai', NULL, NULL, '2020/12/08 13:33:24', 0, 'support@coact.co.in', 0, '1beba6c1dcd5c712908eedf009835a60'),
(100, 0, 0, 0, 1, 'Tadu', 'HR', '9436248331', 'RKMH', 'Itanagar', NULL, NULL, '2020/12/08 14:02:50', 0, 'hrtadu@gmail.com', 0, 'ab989d4ec650845a338997f9aa5a48aa'),
(101, 0, 0, 1, 0, 'Akshat', 'Jharia', '+917204420017', 'COACT', 'Bangalore', '2021/04/20 18:37:09', '2021/04/20 18:45:09', '2020/12/08 14:30:24', 1, 'akshatjharia@gmail.com', 0, 'dd72eace7baaf486f179a25269b03405'),
(102, 0, 0, 0, 1, 'Virupaksha', 'Joshi', '9823160106', 'Ashwini hospital', 'Solapur', NULL, NULL, '2020/12/08 15:54:40', 0, 'joshivirupaksha3@gmail.com', 0, '6aa110c53a668ece86e90435b8638e28'),
(103, 0, 0, 0, 1, 'raghu', 'rushi', '9686621244', 'Ebisu diagnostics ', 'Bangalore', NULL, NULL, '2020/12/08 17:30:25', 0, 'drrushir@gmail.com', 0, 'a72fedfa327326e702fe40e9b4c9eb49'),
(104, 0, 0, 0, 0, 'Ranjit', 'Pradhan ', '09401924522', 'LAHRC ', 'Bongaigaon', NULL, NULL, '2020/12/08 18:22:10', 0, 'ranjit_pradhan3@yahoo.com', 0, '34a1d20b001d5b1260137f60edec9685'),
(105, 0, 0, 0, 1, 'Nandan', 'Kumar', '9742097021', 'Mhdc', 'Shivamogga', NULL, NULL, '2020/12/08 18:23:03', 0, 'nandankumars9876@gmail.com', 0, '4c7d6e306678e1e85337a6eb4d997241'),
(106, 0, 0, 1, 0, 'Raunak', 'Khandelwal', '8088708800', 'Advanced Imaging Point', 'Nagpur', '2020/12/11 17:15:46', '2020/12/11 18:04:51', '2020/12/08 20:57:43', 0, 'raunakkhandelwal2000@yahoo.com', 0, '470fb8cd289963be7501db03eb010beb'),
(107, 0, 0, 1, 0, 'Akash', 'Vegad', '9601951110', 'Lobo staffing', 'Rajkot', '2020/12/11 17:52:44', '2020/12/11 18:20:15', '2020/12/09 06:36:21', 0, 'vegadakash@gmail.com', 0, '9f88c751c8d028789e019f357dff6611'),
(108, 0, 0, 0, 1, 'Vinod', 'Kaila', '9825257493', 'Krishna Multispeciality hospital', 'Morbi', NULL, NULL, '2020/12/09 06:39:23', 0, 'drvinodkaila@gmail.com', 0, '4108457dddc7bc82f2ba6cb07797f52a'),
(109, 0, 0, 0, 1, 'Manhar', 'Baldaniya', '9426441357', 'Sadvichar General hospital and Nursing Home', 'Bhavanagar', NULL, NULL, '2020/12/09 06:40:26', 0, 'msdr057057@gmail.com', 0, '956439e4670a5bcb592560b1b4e0d261'),
(110, 0, 0, 0, 1, 'Mahendra', 'Fefar', '7506959640', 'Nakshatra Hospital', 'Morbi', NULL, NULL, '2020/12/09 06:41:13', 0, 'mahendra.fefar@yahoo.com', 0, '8ff23fb784cee7993ae73b2a0c98ecb2'),
(111, 0, 0, 0, 1, 'Love', 'Katira', '9825235065', 'Katira imaging center', 'Bhuj', NULL, NULL, '2020/12/09 06:42:34', 0, 'katiraimagingcenter@gmail.com', 0, '8fe20e94f9f5686695b3cab798981bff'),
(112, 0, 0, 0, 1, 'Priyansh', 'Thacker', '9727737074', 'Prarthna imaging center', 'Gandhidham', NULL, NULL, '2020/12/09 06:43:36', 0, 'drpriyanshthacker@gmail.com', 0, '364fab2a530674131c6b9b66dff92005'),
(113, 0, 0, 0, 1, 'Naitik', 'Chhatrala', '9712910014', 'K J Nidan Kendra', 'Junagadh', NULL, NULL, '2020/12/09 06:44:43', 0, 'dr.naitikchhatrala@gmail.com', 0, 'c9f8a87af7b3d11611a5fc1d7907d2db'),
(114, 0, 0, 0, 0, 'Pushkar', 'Dabhi', '9558270177', 'Het Imaging', 'Jasdan', NULL, NULL, '2020/12/09 06:45:53', 0, 'drpushkar777@gmail.com', 0, '38f2b8473a9284691431a19fe4e8bd8a'),
(115, 0, 0, 0, 1, 'Anup', 'Kurele', '8225095234', 'Nidaan imaging center', 'Bhuj', NULL, NULL, '2020/12/09 06:47:01', 0, 'nidaanradiology@gmail.com', 0, '5e261a8d587e9939fab9e1ba844a0c30'),
(116, 0, 0, 0, 1, 'Ramgopal', 'Pansuriya', '6352032198', 'Pooja Diagnostic and Imaging Center', 'Gandhidham', NULL, NULL, '2020/12/09 06:49:22', 0, 'poojadiagnostickutch@gmail.com', 0, '77198a353ed88a6bae45ea4854d3d51a'),
(117, 0, 0, 0, 1, 'Sunil', 'Patel', '9879290028', 'Ami imaging center', 'Junagadh', NULL, NULL, '2020/12/09 06:50:23', 0, 'sunilpateljnd@gmail.com', 0, '4a1dafac143f743840ac4d931cb3c6a5'),
(118, 0, 0, 0, 1, 'Kamlesh', 'Darania', '9825855359', 'Suvidha imaging', 'Junagadh', NULL, NULL, '2020/12/09 06:51:34', 0, 'suvidhaimaging@yahoo.in', 0, '5e1fa0684b2c50bf3d971b4c1442e2c9'),
(119, 0, 0, 0, 1, 'Jalpan', 'Rupapara', '9913378337', 'Aum imaging', 'Junagadh', NULL, NULL, '2020/12/09 06:52:26', 0, 'jalpan.gir@gmail.com', 0, '13f8ac7263410a00ffd96b18a1860965'),
(120, 0, 0, 0, 1, 'Paras', 'Patel', '9033320713', 'Apex imaging center', 'Wankaner', NULL, NULL, '2020/12/09 06:53:07', 0, 'paraspatel1113@gmail.com', 0, '696050d07a583146297bb54a6ed6e5dd'),
(121, 0, 0, 0, 1, 'Piyush', 'Desai', '9824229463', 'Ramkrupa diagnostics', 'Rajkot', NULL, NULL, '2020/12/09 06:54:03', 0, 'jpsdesai@gmail.com', 0, '8357be1b5ec46b2601acd5d5551a3af2'),
(122, 0, 0, 0, 1, 'Pravin', 'Lakum', '9377734149', 'Swastik CT Scan center', 'Surendranagar', NULL, NULL, '2020/12/09 06:55:00', 0, 'pjlakum9@gmail.com', 0, '214139b4cd0ad1ab18557c7c38e03034'),
(123, 0, 0, 0, 1, 'Vijaykumar', 'Gupta', '9824311959', 'Rajkot Cancer Society ', 'Rajkot', NULL, NULL, '2020/12/09 06:55:57', 0, 'drvkgupta2001@yahoo.com', 0, 'd996527b432d9acd40084b5a1851fbc0'),
(124, 0, 0, 0, 1, 'Nakul', 'Morakhia', '9099967713', 'Morakhia vastro Liver hospital & endoscopy center', 'Gandhidham', NULL, NULL, '2020/12/09 06:57:25', 0, 'drnakul84@gmail.com', 0, '6b19df42732ce99b7b4d154e54efbd66'),
(125, 0, 0, 0, 1, 'Manoj', 'Dholu', '7574014546', 'Satyam imaging center', 'Botad', NULL, NULL, '2020/12/09 06:58:14', 0, 'manojgdholu@gmail.com', 0, 'f23b1ad30390951e7056bf4c05c1d1fb'),
(126, 0, 0, 0, 1, 'Kurang', 'Gandhi', '9374384564', 'Gandhi\'s Diagnostic center', 'Anjar', NULL, NULL, '2020/12/09 06:59:09', 0, 'kjgandhi7@yahoo.com', 0, '54af24997494a16967113d9b5f0d6323'),
(127, 0, 0, 0, 1, 'Jyoti', 'Sorathiya', '9978912439', 'Shreeji imaging center', 'Anjar', NULL, NULL, '2020/12/09 06:59:55', 0, 'shreejiimaging2014@gmail.com', 0, 'f3e71ee2ef57ac042f8d2ac7bf00a1c6'),
(128, 0, 0, 0, 1, 'Harshad ', 'Rathod', '9879532500', 'Dr harshad Rathod\'s X-ray and Sonography Clinic', 'Amreli', NULL, NULL, '2020/12/09 07:01:39', 0, 'dr_harshadrathod@yahoo.com', 0, 'b0cb1e8bf4722630303d7d20b08b8ba5'),
(129, 0, 0, 0, 1, 'Hardik', 'Parmar', '9409443944', 'Parmar X-ray sonography and opg clinic', 'Amreli', NULL, NULL, '2020/12/09 07:02:44', 0, 'drhardikparmar@gmail.com', 0, '8c4db59bf391c29142c7915acda9d1df'),
(130, 0, 0, 0, 1, 'Deval', 'Chauhan', '9687633319', 'Dev Diagnostics', 'Amreli', NULL, NULL, '2020/12/09 07:03:48', 0, 'drdeval.chauhan716@gmail.com', 0, '81377e03c24e4a36686513c15a19efa8'),
(131, 0, 0, 0, 1, 'Darshan', 'Bosamia', '9824083416', 'Darshan diagnostic centre', 'Bhuj', NULL, NULL, '2020/12/09 07:04:48', 0, 'drdarshboss@yahoo.co.in', 0, '591f5928295ad0752b63c6ccc8d24dff'),
(132, 0, 0, 0, 1, 'Anwar ', 'Malek', '9725161456', 'Swaminarayan Gurukul Hospital', 'Rajkot', NULL, NULL, '2020/12/09 07:05:52', 0, 'dranwarfm@rediff.com', 0, '415a45701f4a4d7f84afc301a980a0bd'),
(133, 0, 0, 0, 1, 'Vimal', 'Vekariya', '8530665366', 'Krishna Diagnostics', 'Rajkot', NULL, NULL, '2020/12/09 07:06:37', 0, 'varnee.vv@gmail.com', 0, 'ad2b0c0e94525fef134722b2e3a891c4'),
(134, 0, 0, 0, 1, 'Jagruti', 'Ramna', '9741109236', 'Pulse plus Multispeciality hospital', 'Bhavanagar', NULL, NULL, '2020/12/09 07:07:18', 0, 'jagrutiramna13@gmail.com', 0, '0cbedc7176dce6cf16ddd6bf3b845b41'),
(135, 0, 0, 0, 1, 'Nishant', 'Chotai', '9898124128', 'G T Sheth Orthopedic hospital', 'Rajkot', NULL, NULL, '2020/12/09 07:08:01', 0, 'nishantchotai@yahoo.co.in', 0, '521f3d5bd0513607c69a0a4645f9cfd8'),
(136, 0, 0, 1, 0, 'Jansha', 'Rehman', '9894626966', 'VitalMed Solutions and Services Pvt Ltd', 'Coimbatore ', '2020/12/11 17:37:16', '2020/12/11 18:47:08', '2020/12/09 07:10:15', 0, 'jansha@vitalmed.in', 0, '00eb5ba65209566733b59e8d29764dad'),
(137, 0, 0, 0, 1, 'ANTO RAMESH ', 'DELVI ', '9916509090', 'Columbia Asia Hospital ', 'BENGALURU ', NULL, NULL, '2020/12/09 07:50:07', 0, 'anto.ramesh@columbiaindiahospitals.com', 0, '76cb0ec9d7118ab11d5f9bca63e37352'),
(138, 0, 0, 0, 1, 'Krishna M ', 'Rao', '09839802413', 'Rao Mri Imaging ', 'Gorakhpur', NULL, NULL, '2020/12/09 08:00:01', 0, 'drrao_n21@yahoo.com', 0, 'b4e185cf203bf7fdfc5b592369e8095c'),
(139, 0, 0, 0, 1, 'Kaleem', 'AHMAD', '09935213364', 'BRD medical college', 'Gorakhpur', NULL, NULL, '2020/12/09 08:00:15', 0, 'drkalim17@yahoo.co.in', 0, '6aca92b74500a862a73db188419246a4'),
(140, 0, 0, 0, 1, 'Harsha ', 'Chadaga', '9900093866', 'Columbia Asia Radiology Group', 'Bangalore', '2020/12/11 17:40:08', '2020/12/11 19:08:47', '2020/12/09 08:13:29', 0, 'harsha.c@columbiaindiahospitals.com', 0, '4fff5063c89df969c5d95c5af8c60a30'),
(141, 0, 0, 1, 0, 'vijayakumar', 'Chandrasekaran', '9945017249', 'Columbia Asia Hospitals ', 'bangalore', NULL, NULL, '2020/12/09 08:16:54', 0, 'vijayulhn7@gmail.com', 0, '2b2a676925f44838a74929c94f0c56ae'),
(142, 0, 0, 0, 1, 'Rattan', 'Paul', '8717008154', 'HIMS sitapur', 'Lucknow', NULL, NULL, '2020/12/09 08:35:16', 0, 'khajuria_rattan64@yahoo.com', 0, '5eafc0eae7e596fff0933f7254ace1ec'),
(143, 0, 0, 0, 0, 'Manoj', 'Ranjan', '9695007210', 'Heritage Hospital ', 'Varanasi ', NULL, NULL, '2020/12/09 08:58:03', 0, 'manran28@gmail.com', 0, '5a7dbf38d3aecee72f0436f9fdd8872f'),
(144, 0, 0, 0, 1, 'Pramod Kumar ', 'Singh', '9453915615', 'IMS bhu ', 'Varanasi', NULL, NULL, '2020/12/09 09:09:44', 0, 'pksimsbhu@gmail.com', 0, 'e5d08c79e5dd85e91f7503822bc6f028'),
(145, 0, 0, 1, 0, 'Vaibhav ', 'Mhatre', '9920428348', 'Varian Medical Systems ', 'Mumbai ', NULL, NULL, '2020/12/09 09:39:29', 0, 'vaibhav.mhatre@varian.com', 0, 'a55d764e28a0163089096c79961622dd'),
(146, 0, 0, 1, 0, 'Shyam', 'Harinath', '8892439907', 'Siemens', 'Bangalore', NULL, NULL, '2020/12/09 10:36:09', 0, 'shyamnath.harinath@siemens-healthineers.com', 0, '577aa0b4b153737dafced4a842387493'),
(147, 0, 0, 0, 1, 'Dr apurv veer', 'Sharma', '09838438182', 'Rays diagnointerventions pvt ltd', 'Varanasi', '2020/12/11 17:36:36', '2020/12/11 18:18:09', '2020/12/09 11:24:04', 0, 'chandpurdiagnostic@gmail.com', 0, '4d46e90b45d83b97b27c8c29d7303563'),
(148, 0, 0, 1, 0, 'Ashok Kumar', 'Gupta', '9839333013', 'Siemens healthcare ', 'Lucknow ', '2020/12/11 18:32:14', '2020/12/11 18:36:14', '2020/12/09 12:25:57', 0, 'ashokkumar.gupta@siemens-healthineers.com', 0, '2f88d94a9fe1aa82844c3dbc6e9a4da2'),
(149, 1, 0, 0, 0, 'Jaslin Kaur', 'Bawa', '9871880553', 'Siemens Healthineers', 'New Delhi', '2020/12/11 17:16:46', '2020/12/11 18:47:47', '2020/12/09 12:54:19', 0, 'jaslin.lamba@siemens-healthineers.com', 0, 'be1a2ec4560e5d52158f6e24d3e4f81a'),
(150, 0, 0, 1, 0, 'Ersin', 'Karci', '0549934420', 'Siemens', 'Dubai', NULL, NULL, '2020/12/09 13:25:38', 0, 'ersin.karci@siemens-healthineers.com', 0, '19f9e8a917c7a22833cb3c790072d1f9'),
(151, 0, 0, 1, 0, 'R VINOTH', 'KUMAR', '+919971871116', 'Siemens ', 'Chennai ', NULL, NULL, '2020/12/09 17:23:24', 0, 'vinothrad@gmail.com', 0, 'a3f80a689f812ca596f3d578283da5f9'),
(152, 0, 0, 0, 0, 'Ramesh', 'Byrapaneni', '9177000889', 'Endiya', 'Hyderabad', NULL, NULL, '2020/12/09 17:54:35', 0, 'ramesh.byrapaneni@gmail.com', 0, 'a2c830954d99a5689ffc2794e4fc3181'),
(153, 0, 0, 1, 0, 'Pranav', 'Patil', '9322297779', 'Siemens Healthineers ', 'Mumbai', '2020/12/11 17:17:12', '2020/12/11 18:51:43', '2020/12/09 18:10:57', 0, 'pranav.patil@siemens-healthineers.com', 0, '2ff8f67a5d049c758766b7cd05fef7e9'),
(154, 0, 0, 1, 0, 'Anil', 'Hari', '07436099411', 'Poole Hospital ', 'Dorset', NULL, NULL, '2020/12/09 18:45:34', 0, 'anilhari007@hotmail.com', 0, '94c3b9d6a26ba8beeff4975152adee82'),
(155, 0, 0, 0, 1, 'Nita', 'Sachan', '9952155115', 'Endiya partners ', 'Hyderabad ', NULL, NULL, '2020/12/09 18:55:53', 0, 'nita.sachan@endiya.com', 0, '701af2fb77644dd88c51ce7881c717d0'),
(156, 0, 0, 1, 0, 'Andre', 'Cavalleiro', '015222718924', 'Siemens Healthcare GmbH', 'Erlangen', '2020/12/11 21:43:32', '2020/12/12 01:19:29', '2020/12/09 19:12:28', 0, 'andre.cavalleiro@siemens-healthineers.com', 0, '2112504cbcfc74706cdd1d437d41a3b5'),
(157, 0, 0, 1, 0, 'Sathish', 'Narayan', '9986685054', 'ColumbiaAsia Hospitals', 'Bangalore', NULL, NULL, '2020/12/09 19:45:00', 0, 'sathish.n@columbiaindiahospitals.com', 0, '4e54d5a6646663a78815b0a8cf9333c1'),
(158, 0, 1, 0, 0, 'Test', 'Test', '9768161921', 'Test', 'Test', '2020/12/09 19:48:37', '2020/12/09 20:11:23', '2020/12/09 19:46:24', 0, 'nidhi.jaiswal1204@gmail.com', 0, '6afbddecfcb5bdbb2895b479f008f6c9'),
(159, 0, 0, 0, 1, 'amitkumar', 'tiwari', '09617085184', 'Samarpan imaging solutiona', 'Indore', NULL, NULL, '2020/12/09 19:59:41', 0, 'dr.aamtiwari@gmail.com', 0, 'e2e8839b4fa95406f0d9f577d9576333'),
(160, 0, 0, 1, 0, 'Anurag', 'Sodani', '9617770172', 'Sampurna Diagnostics', 'Indore', NULL, NULL, '2020/12/09 20:19:22', 0, 'anurag@sampurnadiagnostics.com', 0, 'f1e2f1f4aae68dabe229c86787a84b24'),
(161, 0, 0, 0, 1, 'Ketan', 'Patel', '9978867131', 'Horizon imaging ', 'Surat ', NULL, NULL, '2020/12/09 20:34:16', 0, 'drketan910@gmail.com', 0, 'b7729dc7ed7a8352ea4c3cc07e82cb18'),
(162, 0, 0, 1, 0, 'Joby', 'Abraham', '7593886699', 'Parco hospital', 'Kerala', '2020/12/11 18:05:08', '2020/12/11 18:46:10', '2020/12/09 20:40:01', 0, 'Joby.A@parcohospital.com', 0, 'a845ca42c6b625e5e71f140f01d31b82'),
(163, 0, 0, 0, 1, 'Krishnan ', 'Ramakrishnan ', '9894722221', 'Trichy premier scans', 'Tiruchirappalli ', NULL, NULL, '2020/12/09 21:13:56', 0, 'athma@athmahospitals.com', 0, 'fa9f06d5a95987722e868ca6ad83769d'),
(164, 0, 0, 0, 1, 'NASEER', 'P', '9447135167', 'PARCO', 'CALICUT ', NULL, NULL, '2020/12/09 21:50:03', 0, 'drnaseerp@parcohoapital.com', 0, '3af2faaa51bd4149bf53c74e991b3560'),
(165, 0, 1, 0, 0, 'SUJITHA', 'R', '8760323175', 'Sir ivan sterdeford hospital, Ambattur', 'Chennai', NULL, NULL, '2020/12/09 22:01:03', 0, 'sujitha16nov@gmail.com', 0, '2e4d1defae2c8a2ade2ecd14a549a22c'),
(166, 0, 0, 1, 0, 'Ajimsha', 'Abdul', '9544000004', 'Doctor', 'Cochin ', NULL, NULL, '2020/12/09 22:28:38', 0, 'ajim004@gmail.con', 0, 'bac5e52b37f83db59c72ad8102a34cfa'),
(167, 0, 0, 1, 0, 'Rinesh', 'Kochummen', '8089700464', 'Kisco diagnostics ', 'Kottayam ', NULL, NULL, '2020/12/09 23:02:43', 0, 'joshuaayyeneth@gmail.com', 0, '2ce29593f8b607790e4074ad99974084'),
(168, 0, 0, 0, 1, 'Rajendra', 'Voruganti', '9949056776', 'Surya diagnostic centre', 'Khammam', NULL, NULL, '2020/12/10 06:23:42', 0, 'rajendrasdc@gmail.com', 0, 'a8c71c53bb0f53c28d49a589586a462a'),
(169, 0, 0, 1, 0, 'Raveendra', 'Nimmagadda', '09885662375', 'Siemens', 'Bangalore ', '2020/12/11 17:39:22', '2020/12/11 18:47:08', '2020/12/10 07:26:44', 0, 'raveendra.nimmagadda@gmail.com', 0, 'b97d778753314f3471b74121b7971c4e'),
(170, 0, 0, 1, 0, 'Sanjeev', 'Kumar', '9687641236', 'Siemens Healthcare ', 'Ahmedabad ', '2020/12/11 18:25:13', '2020/12/11 18:37:45', '2020/12/10 07:39:28', 0, 'Sanjeev.kumar1@siemens-healthineers.com', 0, '91a1d849b371d795085f9da5feaa0f3a'),
(171, 0, 0, 0, 1, 'Dr.Kashiram ', 'JAISWAL ', '9437202782', 'C.W.S.Hospital  Jagda ', 'ROURKELA ', NULL, NULL, '2020/12/10 07:42:49', 0, 'dr.kjjaiswal@gmail.com', 0, '47be1da1b17a039efb98c9544d0b38a6'),
(172, 0, 0, 0, 1, 'Devasenathipathy', 'Kandasamy', '9873391531', 'AIIMS', 'New delhi', NULL, NULL, '2020/12/10 09:13:49', 0, 'devammc@gmail.com', 0, '59c09000077158e31d88669a5e4e3882'),
(173, 0, 0, 1, 0, 'Reynold', 'Roy', '+919611518064', 'Coact', 'Bangalore', '2020/12/10 10:33:12', '2020/12/10 10:33:16', '2020/12/10 10:31:18', 0, 'royreynoldexplorer@gmail.com', 0, '52725fd1626d2e0684b95d89248e7eab'),
(174, 0, 0, 0, 1, 'Dr Deepak', 'Mehta', '+919879167676', 'Pramukhswami Medical College & Shree Krishna Hospital, Karamsad ', 'Vadodara', NULL, NULL, '2020/12/10 13:34:23', 0, 'drdeepakvmehta1@gmail.com', 0, '7c68f7545dbc178a35efec3256c123e5'),
(175, 0, 0, 1, 0, 'Kshitij ', 'Jain', '9047574379 ', 'Arihant', 'Varanasi', '2020/12/11 18:13:48', '2020/12/11 19:30:56', '2020/12/10 14:49:56', 0, 'k_pravara@yahoo.in', 0, '433b35968d668391ac62d60f18b646ce'),
(176, 0, 0, 1, 0, 'Hariharan', 'NS', '9845121862', 'SHPL', 'Bangalore', NULL, NULL, '2020/12/10 15:21:24', 0, 'ns_hariharan@siemens-healthineers.com', 0, 'ff5f2cd21cdc86e7e6151a5813bce7bb'),
(177, 0, 0, 1, 0, 'Suresh ', 'Mallavarapu', '8885826292', 'Siemens Healthcare', 'Sambalpur', NULL, NULL, '2020/12/10 17:42:53', 0, 'sureshmallavarapu5@gmail.com', 0, 'c3e88407c85f2f789b73b506ccca6647'),
(178, 0, 0, 1, 0, 'Nikesh', 'Varughese', '9902766227', 'Siemens Healthineers', 'Bangalore', NULL, NULL, '2020/12/10 17:43:58', 0, 'nikesh.varughese@siemens-healthineers.com', 0, 'd886ae09cdf9ce5cc74cc1d6f0cac383'),
(179, 0, 0, 1, 0, 'vivek', 'kanade', '9167614550', 'Siemens Healthineers', 'mumbai', '2020/12/11 17:26:54', '2020/12/11 18:47:55', '2020/12/10 17:52:14', 0, 'vivek.kanade@siemens-healthineers.com', 0, '2dc7272dbf8c3e2bba53b0b40b8ac802'),
(180, 0, 0, 1, 0, 'BASIL ', 'PHILIP', '9773106208', 'Siemens Healthineers', 'Mumbai', '2020/12/11 18:17:45', '2020/12/11 18:47:04', '2020/12/10 17:59:27', 0, 'basil.philip@siemens-healthineers.com', 0, 'a4abc64cb080e1642e5fb59387ac2ea6'),
(181, 0, 0, 0, 0, 'Deep', 'Roy', '9864064127', 'Medical college', 'Jorhat', NULL, NULL, '2020/12/10 18:19:30', 0, 'drdeepkrroy@gmail.com', 0, '17877618ea46e823fc3a8f6dedd81e83'),
(182, 1, 0, 0, 0, 'Eric', 'Chacon', '+593998912190', 'HVQ SA ', 'Ecuador', NULL, NULL, '2020/12/10 18:29:15', 0, 'eric_chacon@hotmail.es', 0, 'bf57727b432da0e88d40cb4ec65b806e'),
(183, 0, 0, 0, 1, 'BABU PETER', 'SATHYANATHAN', '9444366278', 'MADRAS MEDICAL COLLEGE', 'CHENNAI', '2020/12/11 17:30:59', '2020/12/11 18:33:27', '2020/12/10 18:31:12', 0, 'drbabupeter@gmail.com', 0, 'c17ffa85d266658cc2f4e26308357fe1'),
(184, 0, 0, 0, 1, 'Hirdesh', 'Sahni', '8800750026', 'Govt', 'Pune', '2020/12/11 17:54:44', '2020/12/11 18:47:44', '2020/12/10 18:36:40', 0, 'drhsahni@gmail.com', 0, '3bbcb0fb13cc200cc60001451da09ca2'),
(185, 0, 0, 0, 1, 'Saikat ', 'Sarkar', '9820229554', 'SHPL', 'Guwahati ', '2020/12/11 17:20:37', '2020/12/11 18:47:04', '2020/12/10 18:54:09', 0, 'saikat.sarkar@siemens-healthineers.com', 0, 'bde28500d39c522deecf6a2c90e35e1b'),
(186, 0, 0, 0, 1, 'Senthil', 'Kumaran ', '9868570857', 'All India Institute of Medical Sciences', 'New Delhi ', '2020/12/11 17:32:54', '2020/12/11 18:47:09', '2020/12/10 19:38:10', 0, 'senthilssk@yahoo.com', 0, 'a4ad862f84ea0b0222a3d7de4af794e5'),
(187, 0, 0, 1, 0, 'Gourava', 'Taraphder', '9836400520', 'Siemens Healthcare', 'Guwahati', NULL, NULL, '2020/12/10 19:46:15', 0, 'gourava.taraphder@siemens-healthineers.com', 0, '726d12ea7da690adbc1b530f338775b3'),
(188, 0, 0, 1, 0, 'Ashwini', 'Kumar', '8861712712', 'SHPL', 'Bengaluru', '2020/12/11 17:38:03', '2020/12/11 18:47:01', '2020/12/10 21:06:09', 0, 'ashwinikumar@siemens-healthineers.com', 0, '80df7af63a4a5ce9cd9ff2b21c986317'),
(189, 0, 0, 0, 1, 'Mrunalini ', 'Rao', '8985992727', 'Sri vamsi scan centre ', 'Eluru ', NULL, NULL, '2020/12/10 22:26:54', 0, 'drmrunalini2709@gmail.com', 0, 'f84bd87ac36e3cb99f73c2db15ce6fb4'),
(190, 0, 0, 0, 1, 'swarna', 'Girish Babu', '+919626594959', 'Sravanagiri Imaging and Diagnostics', 'Machilipatnam', NULL, NULL, '2020/12/11 04:47:08', 0, 'swarnagirish@gmail.com', 0, '0ce0df99a67276c0f0512ad0918ec1a4'),
(191, 0, 0, 1, 0, 'Halim ', 'Ansari', '7838066503', 'siemens Healthcare', 'Ranchi', '2020/12/11 18:10:33', '2020/12/11 18:49:20', '2020/12/11 06:22:25', 0, 'halim.ansari@siemens-healthineers.com', 0, 'da29ba6674c4694afff4371e0986afdb'),
(192, 0, 0, 0, 1, 'Niraj ', 'Kumar', '+91 94315 89280', 'Doctor diagnostic', 'Jamshedpur', NULL, NULL, '2020/12/11 06:31:59', 0, 'Kumarnirajdr@yahoo.co.in', 0, '7f0c389cad287b8a9c78cf971f9fe14c'),
(193, 0, 0, 0, 1, 'Bejoy', 'Thomas', '9447719481', 'SCTIMST', 'TRIVANDRUM', NULL, NULL, '2020/12/11 06:55:20', 0, 'drbejoy2002@gmail.com', 0, 'cbda13a6630f3db519f78f5779ec9611'),
(194, 0, 0, 0, 1, 'Mihir ', 'Jha', '+9193316 25736', 'Avishkar Diagnostic', 'Dhanbad', NULL, NULL, '2020/12/11 06:57:36', 0, 'dr_mihir_online@yahoo.co.in', 0, '0f9ad55de9d00000aba9a9763ed09b2c'),
(195, 0, 0, 0, 1, 'Praveen ', 'Tripathi', '+919798608820', 'Radiopath diagnostic', 'Ranchi', NULL, NULL, '2020/12/11 07:00:39', 0, 'sonipraveentrpth22@gmail.com', 0, 'fb9f67f71019ff68127d8779ca201ee0'),
(196, 0, 0, 0, 1, 'Nanjundan', 'Murali', '9843032425', 'Govt Medical College Hospital', 'Coimbatore', '2020/12/11 17:36:43', '2020/12/11 18:43:44', '2020/12/11 07:34:57', 0, 'drnmurali@gmail.com', 0, '95cd7c2042a2fed6997c8e1c2d219202'),
(197, 0, 0, 1, 0, 'PRODYUT', 'SAHA', '+919748003381', 'Siemens Healthcare Pvt. Ltd.', 'KOLKATA', '2020/12/11 17:02:33', '2020/12/11 18:46:53', '2020/12/11 07:46:10', 0, 'prodyut.saha@gmail.com', 0, '99ca4cd019a729eb0c142c9a3163ea5b'),
(198, 0, 0, 0, 1, 'Ishtiaque ', 'Ahmad', '+918750004932', 'Medica Hospital', 'Ranchi', NULL, NULL, '2020/12/11 07:55:15', 0, 'mail2ishteyaque75@gmail.com', 0, '761f4d2210243939f1e6644a2096423c'),
(199, 0, 0, 0, 1, 'Ambuj ', 'Srivastava', '+918971449003', 'Samford Hospital', 'Ranchi ', NULL, NULL, '2020/12/11 08:07:04', 0, 'ambuj.dr@gmail.com', 0, '40357b2a8183de2202e03acd87fb0e90'),
(200, 0, 0, 0, 1, 'Pranav', 'Narayan', '+919771454222', 'V & L Diagnostics', 'Ranchi', NULL, NULL, '2020/12/11 08:09:16', 0, 'pnarayan99@gmail.com', 0, '36e1de098aca4228c2594555fee4db5f'),
(201, 0, 0, 0, 1, 'Priya', 'Chudgar', '9820612062', 'Jupiter Hospital', 'Mumbai', NULL, NULL, '2020/12/11 08:25:27', 0, 'priyachudgar@gmail.com', 0, '3efd390754196528cf00906d153784cf'),
(202, 0, 0, 1, 0, 'Surender Pal', 'Singh', '9711989062', 'Siemens Healthcare Pvt. Ltd.', 'Gurgaon', NULL, NULL, '2020/12/11 08:32:35', 0, 'surenderpal.singh@siemens-healthineers.com', 0, '94456568a95e4fe81250d5da0fab5f47'),
(203, 0, 1, 0, 0, 'shalini', 'kumari', '8779293619', 'Siemens Healthineers ', 'Mumbai', '2020/12/11 19:56:17', '2020/12/11 19:58:18', '2020/12/11 08:37:15', 0, 'shalini.kumari@siemens-healthineers.com', 0, '1f57b383ad5367e172a4070c81be8671'),
(204, 0, 1, 0, 0, 'Deepti', 'Goyal', '9007785233', 'Ex-PwC', 'Mumbai', '2020/12/11 17:25:23', '2020/12/11 18:47:55', '2020/12/11 08:37:16', 0, 'deeptigoyal1@gmail.com', 0, 'c25e17a06c1043ea5b9f3f7ec2ec0333'),
(205, 0, 0, 0, 1, 'Ramachandra', 'Dasar', '9740159229', 'AGMC', 'Agartala', NULL, NULL, '2020/12/11 08:53:48', 0, 'rcdasar70@gmail.com', 0, '96ce7207614df7113f61ef550e409ae4'),
(206, 0, 0, 0, 1, 'Dr .GOPINATH ', 'ARUNACHALAM', '8870009015', 'Barani scans', 'Tirunelveli ', NULL, NULL, '2020/12/11 09:06:10', 0, 'gokabi@gmail', 0, '13607fc80d794607c8600fca392de4cc'),
(207, 0, 0, 0, 1, 'Pratiksha', 'Yadav', '7709085551', 'Dr.D Y Patil Medical College,Hospital and Research Center,Pune', 'Pune', NULL, NULL, '2020/12/11 09:25:01', 0, 'yadavpratiksha@hotmail.com', 0, 'bc27e4cc3c8bf336145531484aa4f898'),
(208, 0, 0, 0, 1, 'Jyoti', 'Kumar', '09968604361', 'Maulana Azad Medical college', 'Delhi', NULL, NULL, '2020/12/11 09:40:42', 0, 'jyotiatin@refiffmail.com', 0, '5db99fdb8b0356c2a1f5571e2091bcfe'),
(209, 0, 0, 1, 0, 'Mohammad', 'Eqbal', '9611272707', 'Siemens Healthcare Pvt Ltd', 'Bengaluru', NULL, NULL, '2020/12/11 09:44:40', 0, 'md.eqbal@siemens-healthineers.com', 0, '111028bb26095d189c301b07f7ad606b'),
(210, 0, 0, 1, 0, 'Aswin ', 'Vk', '09591165100', 'Siemens Healthcare Private Limited ', 'HYDERABAD TG', '2020/12/11 17:11:36', '2020/12/11 18:48:10', '2020/12/11 09:53:02', 0, 'aswin.vk@siemens-healthineers.com', 0, '14f9f48e2b274070d5dcfac5ee88b410'),
(211, 0, 0, 0, 1, 'Sonal ', 'Krishnan', '9717885551', 'Medanta Hospital', 'Gurgaon', NULL, NULL, '2020/12/11 09:53:55', 0, 'sonalkrishan11@googlemail.com', 0, 'e5d16e8947d223f2cee1857cdc056e31'),
(212, 0, 0, 1, 0, 'Sunil', 'Garg', '9810599160', 'SHPL', 'Delhi', NULL, NULL, '2020/12/11 09:54:32', 0, 'sunil.garg@siemens-healthineers.com', 0, '942f13c5dd682f53908163be7c3210a6'),
(213, 0, 0, 0, 1, 'Sonal', 'Verma', '+919717885551', 'Medanta-The Medicity', 'Gurgaon', '2020/12/11 18:09:07', '2020/12/11 18:45:37', '2020/12/11 09:56:10', 0, 'sonal.krishan@medanta.org', 0, '41b02b58d54c12815cc7ca8f8174d6d6'),
(214, 0, 0, 1, 0, 'Yash', 'Jain', '9172737195', 'Siemens Healthcare', 'Mumbai', NULL, NULL, '2020/12/11 09:57:10', 0, 'yash.jain@siemens-healthineers.com', 0, '7a95ff3e6da588d3dc955fc53f8eb8bc'),
(215, 0, 0, 1, 0, 'Vidur', 'Saigal', '9999733883', 'Siemens Healthcare Private Limited', 'New Delhi', '2020/12/11 17:30:38', '2020/12/11 17:51:39', '2020/12/11 09:58:29', 0, 'vidur.saigal@siemens-healthineers.com', 0, '5baf31f10a64a15e079ec3186cf32128'),
(216, 0, 0, 1, 0, 'Himanshu', 'Singh', '9711743393', 'AIIMS,NEW DELHI', 'New Delhi', NULL, NULL, '2020/12/11 10:01:03', 0, 'himanshu.nmr@gmail.com', 0, '2a2de39d046e7a6efdc0836d54b5f5a7'),
(217, 0, 0, 1, 0, 'Rajneesh ', 'Handa', '9818818614', 'Siemens Healthineers ', 'Gurugram ', NULL, NULL, '2020/12/11 10:15:59', 0, 'rajneesh.handa@siemens-healthineers.com', 0, '78a5267d56f7b5d8a1b446804abaedef'),
(218, 0, 0, 1, 0, 'Rahul', 'Kamath', '+919611104960', 'Siemens Healthineers', 'Bangalore', NULL, NULL, '2020/12/11 10:23:28', 0, 'rahul.kamath@siemens-healthineers.com', 0, 'a5eb99e8a803abb0b3f119032a7e066c'),
(219, 0, 0, 1, 0, 'Lokesh', 'Agrawal', '7869916478', 'SIEMENS HEALTHCARE', 'Indore', '2020/12/11 18:39:32', '2020/12/11 18:47:06', '2020/12/11 10:31:02', 0, 'lokesh.agarwal@siemens-healthineers.com', 0, 'eec03bf7337d78d2832c557c28565019'),
(220, 0, 0, 1, 0, 'Pankaj', 'Pankaj', '9413856737', 'All India Institute of Medical Sciences (AIIMS)', 'New Delhi', '2020/12/11 17:28:21', '2020/12/11 20:12:21', '2020/12/11 10:34:33', 0, 'pankajneuroit@gmail.com', 0, '14942d8aee56aa0632a89c78d0ec0dc3'),
(221, 0, 0, 1, 0, 'ARUN', 'Kaul', '9811307988', 'Siemens Healthcare Pvt ltd', 'FARIDABAD ', NULL, NULL, '2020/12/11 10:44:29', 0, 'arun.kaul@siemens-healthineers.com', 0, 'ae1813871f1144ba45f15110cd5d3633'),
(222, 0, 0, 1, 0, 'Singaravelu', 'V B', '0000000000', 'SHPL', 'Mumbai', '2020/12/11 17:47:19', '2020/12/11 17:51:32', '2020/12/11 10:58:28', 0, 'vb.singaravelu@siemens-healthineers.com', 0, '3f75d1bd4a03ce25bd4a7e325ac1bb7d'),
(223, 0, 0, 0, 1, 'mihir', 'munshi', '0987654321', 'KDAH', 'Mumbai', NULL, NULL, '2020/12/11 10:59:42', 0, 'mihir.munshi@kokilabenhospitals.com', 0, '1b225b5b985d076232906d83667fb2c0'),
(224, 0, 0, 0, 0, 'Shakthi', 'Parvathi', '9947077010', 'Medical Trust Hospital', 'Kochi', '2020/12/11 18:04:40', '2020/12/11 18:47:40', '2020/12/11 11:04:41', 0, 'shakthiparvathy@gmail.com', 0, 'f91a4952d9069b043c41e71a34d5e788'),
(225, 0, 0, 0, 1, 'SUMITA', 'KUNDU', '9830233263', 'EKO XRAY AND IMAGING INSTITUTE', 'KOLKATA', NULL, NULL, '2020/12/11 11:06:20', 0, 'kundusumita@yahoo.com', 0, 'd70623b9ffcbc4491a6e81b09eca221e'),
(226, 0, 0, 0, 1, 'Noaline', 'Sinha', '09810209733', 'Artemis Hospital Gurugram haryana India', 'DELHI', NULL, NULL, '2020/12/11 11:09:49', 0, 'noalinesinha@gmail.com', 0, 'ca06321e0f80b05794e6099ae515cf8e'),
(227, 0, 0, 0, 1, 'Harsh', 'Shah', '40856141', 'MRRCH-RBCL', 'Mumbai', '2020/12/11 17:29:16', '2020/12/11 18:34:54', '2020/12/11 11:14:21', 0, 'harsh.shah@mrrch.com', 0, 'ea8d501d71e74e660c1eeee2a130116e'),
(228, 0, 0, 0, 1, 'Arul', 'Dasan', '9739312244', 'Bangalore medical college', 'Bangalore', NULL, NULL, '2020/12/11 11:16:54', 0, 'arul_dsn@yahoo.co.in', 0, 'c949192b365c22e4c28a1b3ce86d2c72'),
(229, 0, 0, 0, 1, 'Preeti', 'Chavan', '9869005057', 'ACTREC-TMC', 'Mumbai', '2020/12/14 10:24:30', '2020/12/14 10:24:38', '2020/12/11 11:23:29', 0, 'drprreeti@yahoo.com', 0, 'aebdfc2cd54e3f8a10d8f55d9ea743b3'),
(230, 0, 0, 1, 0, 'Pinku', 'Pal', '9874194411', 'SHPL', 'Kolkata', NULL, NULL, '2020/12/11 11:26:30', 0, 'pinku.pal@siemens-Healthineers.com', 0, '8cf306c0bef1e484abb182af9dbd5d36'),
(231, 0, 0, 0, 1, 'Karuna', 'Agawane', '7666935335', 'Ruby Ailcare', 'Mumbai', NULL, NULL, '2020/12/11 11:27:20', 0, 'agawanedkaruna@gmail.com', 0, 'f67741ffb33bf5ed46b0e38987503d17'),
(232, 0, 0, 0, 1, 'ANITHA ', 'Sen', '9447455442', 'RCC', 'Thiruvananthapuram', '2020/12/11 18:40:58', '2020/12/11 18:47:00', '2020/12/11 11:29:40', 0, 'dranithasen@hotmail.com', 0, '2261cdd4d4c423845b55910e5501a783'),
(233, 0, 0, 0, 1, 'Mukesh', 'Kumar', '9718254333', 'AIIMS', 'Delhi', '2020/12/11 17:55:27', '2020/12/11 18:11:13', '2020/12/11 11:32:43', 0, 'mukeshcct@gmail.com', 0, 'c7f46ce7360eae5ab719a719fcb20ddf'),
(234, 0, 0, 0, 1, 'Madhulika', 'VijayaKumar', '9894593210', 'PSG IMSR', 'Coimbatore', NULL, NULL, '2020/12/11 11:33:01', 0, 'madhulika.dr@gmail.com', 0, '45a4508be6461586fb76678ca7b7e3de'),
(235, 0, 0, 0, 1, 'Dinesh', 'Makuny', '09847317361', 'MVRCCRI', 'Calicut', '2020/12/11 17:55:36', '2020/12/11 18:45:37', '2020/12/11 11:33:30', 0, 'drmdinesh@mvrccri.co', 0, '74a269a527ed11e3a72809779586b6a1'),
(236, 0, 0, 1, 0, 'Lalit M', 'Aggarwal', '9336936073', 'IMS BHU', 'Varanasi ', '2020/12/11 17:51:08', '2020/12/11 17:55:13', '2020/12/11 11:35:18', 0, 'lalitm@bhu.ac.in', 0, '4ada7407ad0d3090ef9d65ab457b1569'),
(237, 1, 0, 0, 0, 'Maria Ester', 'Rinesi-Patton', '+491743144456', 'Siemens Healthineers', 'Erlangen', NULL, NULL, '2020/12/11 11:39:38', 0, 'maria-ester.rinesi-patton@siemens-healthineers.com', 0, '9db02fae43ebcb5435a645658782ef14'),
(238, 0, 0, 0, 1, 'Jagannath ', 'Debnath ', '9435061514', 'Ultralab Centre ', 'Nagaon ', NULL, NULL, '2020/12/11 11:42:01', 0, 'drjagannath7@gmail.com', 0, 'def1a4ecf7ae5a42183cd27663efd082'),
(239, 0, 0, 0, 1, 'IMSU', 'Jamir', '9485200234', 'Lenjem hospital', 'Mokokchung', '2020/12/11 17:45:48', '2020/12/11 18:03:22', '2020/12/11 11:42:59', 0, 'drimsu@yahoo.com', 0, '0d041fc05a194b68a54e0e9541a3679c'),
(240, 0, 0, 0, 1, 'Wangju', 'Sumnyan', '96124 84249 ', 'TRIHMS', 'ITANAGAR', NULL, NULL, '2020/12/11 11:43:27', 0, 'sumnyanwang@gmail.com', 0, '6aa5116a129f941fe8c933ab7c0cbd14'),
(241, 0, 0, 0, 0, 'sandhya', 'mangalore', '+918026995424', 'NIMHANS', 'bangalore', NULL, NULL, '2020/12/11 11:50:24', 0, 'drsandym@gmail.com', 0, '7c54301f45f50f55329f6745101492cd'),
(242, 0, 0, 1, 0, 'Bhupendar', 'Negi', '8505911820', 'Siemens Healthineers', 'Chandigarh', NULL, NULL, '2020/12/11 11:54:50', 0, 'bhuvandtu@gmail.com', 0, 'b19f3ad440a26489777d74a1f0e2ac09'),
(243, 0, 0, 0, 1, 'Utpal', 'Saikia', '9435337547', 'Iocl', 'digboi', NULL, NULL, '2020/12/11 12:06:28', 0, 'utpalkumarsaikia@rediffmail.com', 0, 'c0125fc18c8ae64e8d89766a17ddb487'),
(244, 1, 0, 0, 0, 'SHEFALI', 'BHATT', '9327051222', 'Global longlife Hospital', 'AHMEDABAD', NULL, NULL, '2020/12/11 12:08:49', 0, 'bhattshefali71@gmail.com', 0, '047c38b5d0dbe8754b49e15cc6a74ab4'),
(245, 0, 0, 0, 1, 'Abhishek', 'Kumar', '+919999234308', 'Tata Main Hospital', 'Jamshedpur', NULL, NULL, '2020/12/11 12:16:55', 0, 'Mymail.Abhishek@gmail.com', 0, 'c1a36c5e4f753111baa11480e56bf37d'),
(246, 0, 0, 0, 1, 'Nishi', 'Kant', '+919731450424', 'Advance Diagnostic Centre', 'Ranchi ', NULL, NULL, '2020/12/11 12:22:05', 0, 'Nishikant_2804@yahoo.co.in', 0, 'ac68b17b5e61b44eae7ed43a87350a7c'),
(247, 0, 0, 1, 0, 'Rupendra Singh', 'Saini', '9810599156', 'SHS AP IND DI PM', 'New Delhi', NULL, NULL, '2020/12/11 12:23:31', 0, 'rupendrasingh.saini@siemens-healthineers.com', 0, '3f51a5ee5b68e2743424de80f2f278a3');
INSERT INTO `tbl_users` (`id`, `ms`, `miss`, `mr`, `dr`, `fname`, `lname`, `mobile`, `organisation`, `city`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `is_varified`, `password`) VALUES
(248, 0, 0, 0, 1, 'GAURAV ', 'LUTHRA ', '9415433399', 'RSL', 'Lucknow ', '2020/12/12 17:41:36', '2020/12/12 18:07:29', '2020/12/11 12:27:39', 1, 'drgauravluthra@gmail.com', 0, '85ccb650e44ed04d48726d08fa42d076'),
(249, 0, 0, 0, 1, 'Parveen', 'Gulati', '9811081309', 'DGII', 'Delhi', NULL, NULL, '2020/12/11 12:43:27', 0, 'pagulati@gmail.com', 0, '8d6f90270462daad9a44bdca46d9fcb6'),
(250, 0, 0, 0, 1, 'VIVEK', 'JAIN', '9820432303', 'P.H.MEDICAL CENTRE', 'MUMBAI', NULL, NULL, '2020/12/11 12:57:25', 0, 'drvivek@phmedicalcentre.com', 0, '483bddcdb71db440d93eebb940c497e7'),
(251, 0, 0, 1, 0, 'Onkar', 'Tendulkar', '9930479711', 'SHPL', 'Pune', '2020/12/11 17:56:37', '2020/12/11 18:57:08', '2020/12/11 13:03:52', 0, 'onkar.tendulkar@siemens-healthineers.com', 0, '7d783901d00e5e4759056aaedfdf3a7a'),
(252, 0, 0, 1, 0, 'Prabodha ', 'Mohanty ', '9111999633', 'Siemens ', 'Raipur ', '2020/12/11 17:38:34', '2020/12/11 18:40:01', '2020/12/11 13:08:17', 0, 'mprabodha007@gmail.com', 0, 'a6d40160e37f7b34abd7832918f86a91'),
(253, 0, 0, 0, 1, 'Bernice ', 'T S', '9443404387', 'Christian Medical College ', 'Vellore ', NULL, NULL, '2020/12/11 13:57:36', 0, 'bernice.devarajan@yahoo.com', 0, 'f873ff134091738eb7bcbd0ce63916f9'),
(254, 0, 0, 1, 0, 'SIYA ', 'Ram', '9630364423', 'PRATHAM hospital bilaspur', 'Bilaspur', NULL, NULL, '2020/12/11 14:23:25', 0, 'schandrakarggggg@gmail.com', 0, '27ef896e60ffb0ee2603eec529648b92'),
(255, 0, 0, 0, 1, 'Vineet', 'Maheshwari', '7303907895', 'CC', 'Kawardha ', NULL, NULL, '2020/12/11 15:00:06', 0, 'vineet.is.maheshwari@gmail.com', 0, '21fd6bdda5bbddfc7aa52741bcaed2fc'),
(256, 0, 0, 0, 1, 'Abhishek', 'Jaiswal', '+919013778814', 'Meharbai Tata Memorial Hospital', 'Jamshedpur', NULL, NULL, '2020/12/11 15:20:16', 0, 'abhishek.jaiswal@mtmh.co.in', 0, 'f003e55a96e2fe6c45133e39797f16bb'),
(257, 0, 0, 0, 1, 'Sagar', 'Sonone', '9422200102', 'KEM hospital ', 'Mumbai', NULL, NULL, '2020/12/11 15:23:22', 0, 'sagarsonone@yahoo.com', 0, 'd70f6334e81d8821f68520f03999bcc4'),
(258, 0, 0, 1, 0, 'chandra ', 'sekhar', '9989128328', 'SHPL', 'India', '2020/12/11 17:35:21', '2020/12/11 17:48:29', '2020/12/11 15:42:45', 0, 'chandra.chimakurthy@siemens-healthineers.com', 0, '951ef12557879d7db069aec6f74317c1'),
(259, 0, 0, 0, 1, 'Nilesh', 'Shingala', '9824240918', 'Parth Imaging Centre', 'Rajkot', '2020/12/11 17:24:01', '2020/12/11 18:34:46', '2020/12/11 16:07:35', 0, 'n.shingala@yahoo.com', 0, '8820e43a60032670efd74324266d97cd'),
(260, 0, 0, 0, 1, 'Somen', 'Chakravarthy', '+919204058142', 'Tata Main Hospital', 'Jamshedpur', NULL, NULL, '2020/12/11 16:09:52', 0, 'somen@tatasteel.com', 0, '2987b528a0cb1e610b08ca7cb077a245'),
(261, 0, 0, 1, 0, 'Subhasish', 'Ghosh', '9830909440', 'Siemens Healthcare ', 'Kolkata ', NULL, NULL, '2020/12/11 16:15:15', 0, 'subhasish.ghosh@siemens.com', 0, '698eac41876910b9c7823430815d3087'),
(262, 0, 0, 0, 1, 'Raman Pratap ', 'Singh', '9855580709', 'Superb MRI and PET-CT Centre', 'Chandigarh ', '2020/12/11 18:13:26', '2020/12/11 18:54:23', '2020/12/11 16:22:19', 0, 'drramanchawla7@gmail.com', 0, 'f16c3158d49732ec24acdd2c060ab2f3'),
(263, 0, 0, 1, 0, 'MOHAMMEDNABEEL ', 'Shaikh ', '8007447700', 'Siemens ', 'Nasik', '2020/12/11 17:31:33', '2020/12/11 18:15:34', '2020/12/11 16:53:00', 0, 'nabeelinbox@gmail.com', 0, 'f469bc5df1d87c90d0e54016a2cb8796'),
(264, 0, 0, 0, 1, 'Vaibhav', 'Pandey', '8826972226', 'Stella Imaging & Diagnostics', 'Varanasi', '2020/12/11 17:35:11', '2020/12/11 17:39:11', '2020/12/11 17:04:26', 0, 'pandey2025@gmail.com', 0, '0b5f30b4226af385c292a03b55a96451'),
(265, 0, 0, 1, 0, 'Abhishek', 'Tripathi', '7518614615', 'Siemens Healthineers', 'Lucknow', '2020/12/11 18:03:09', '2020/12/11 18:51:40', '2020/12/11 17:12:20', 0, 'abhishektripathi2390@gmail.com', 0, '347bc1d82c90f5ef8bffa5d913672594'),
(266, 0, 0, 1, 0, 'krijesh', 'panoli kunnath', '9645963030', 'Siemens Healthineers', 'kerala', '2020/12/11 17:26:22', '2020/12/11 18:47:02', '2020/12/11 17:24:40', 0, 'krijesh.kunnath@siemens-healthineers.com', 0, '8a6444f1d67cc78277513cba600023cf'),
(267, 0, 0, 1, 0, 'Praveen', 'PG', '9831967728', 'Siemens Healthineers ', 'Kolkata', '2020/12/13 16:47:18', '2020/12/13 18:53:19', '2020/12/11 17:26:52', 1, 'praveen.pg@siemens-healthineers.com', 0, 'a68492393c87ff533a66aa6bcd800517'),
(268, 0, 0, 1, 0, 'Manesh', 'Madhavan', '+919544098889', 'Aster DM Healthcare', 'Calicut', NULL, NULL, '2020/12/11 17:31:53', 0, 'manesh.madhavan@asterhospital.com', 0, '568417f09dd56dab8cf7e56bb84abde8'),
(269, 0, 0, 0, 1, 'Md', 'Ibrar', '+919431367367', 'Ayushman Diagnostic centre', 'Dhanbad', '2020/12/11 17:35:42', '2020/12/11 17:38:45', '2020/12/11 17:32:33', 0, 'Ibrar999@gmail.com', 0, '41c06d7d83ea2bba0ec09fefbe76b3fe'),
(270, 0, 0, 1, 0, 'Manesh', 'Madhavan', '9544098889', 'Aster DM Healthcare', 'Calicut', '2020/12/11 17:39:33', '2020/12/11 21:53:36', '2020/12/11 17:36:24', 0, 'maneshtm@yahoo.com', 0, '70553848bfee41e640cc52d2d5936635'),
(271, 0, 0, 0, 1, 'DILINIKA', 'PERERA', '0094719000669', 'Teaching Hospital ', 'Kurunegala', NULL, NULL, '2020/12/11 17:40:00', 0, 'dilinikadr@yahoo.com', 0, '950617fbba3472dd59f1e17b88265e2c'),
(272, 0, 0, 1, 0, 'Halim', 'Ansari', '7838066503', 'Siemens Healthcare ', 'Ranchi', NULL, NULL, '2020/12/11 17:42:37', 0, 'halim.ansari88@hotmail.com', 0, '46521057b2f0b03c9bfc1733403ccd56'),
(273, 0, 0, 1, 0, 'Ravi sheshu', 'Kolli', '9591208967', 'Siemens healthineers', 'Bangalore', NULL, NULL, '2020/12/11 17:53:37', 0, 'ravi.kolli@siemens-heathineers.com', 0, 'eb092bff0bf0cb14543d1ffe9e616f07'),
(274, 0, 0, 0, 1, 'Adarsh', 'Anil', '9035020014', 'AIIMS RAIPUR', 'Raipur', NULL, NULL, '2020/12/11 17:54:35', 0, 'adarsh.anil@gmail.com', 0, 'f4238aa2776812f11340f16500adceac'),
(275, 0, 0, 0, 1, 'Anupama', 'Govindan', '09480685535', 'Siemens Healthineers', 'Bangalore', '2020/12/11 17:56:50', '2020/12/11 18:09:51', '2020/12/11 17:55:18', 0, 'anupama.govindan@gmail.com', 0, '7519ee5edd1b96b7fa1537dc1269a9e9'),
(276, 0, 0, 1, 0, 'Ashish', 'Jaiswal', '7829061411', 'Siemens healthcare', 'Bangalore', '2020/12/11 17:58:20', '2020/12/11 18:45:30', '2020/12/11 17:57:03', 0, 'ashishjaiswal@siemens-healthineers.com', 0, '7fadd16e7f9506fbaedf269c66acb184'),
(277, 0, 0, 1, 0, 'Yogesh ', 'Puranik', '9820382011', 'SHPL', 'Mumbai', NULL, NULL, '2020/12/11 17:57:52', 0, 'yogeshrp78@gmail.com', 0, '6e887f4759d1228400f045aac5aa45f2'),
(278, 0, 0, 0, 1, 'Venkatesh', 'Kasi', '9940944588', 'KMCH', 'Coimbatore', '2020/12/11 18:08:50', '2020/12/11 18:17:21', '2020/12/11 18:08:00', 0, 'drkasivenkatesh@yahoo.co.in', 0, '2ce2f339de73f367420f560eedf75417'),
(279, 1, 0, 0, 0, 'Hemalatha ', 'Pallem ', '09880633633', 'Siemens healthineers ', 'Bangalore', NULL, NULL, '2020/12/11 18:11:50', 0, 'Hemalatha.pallem@gmail.com', 0, '6821a4265966273dd4be9aab0d4bfb2e'),
(280, 0, 0, 0, 1, 'Sunetra', 'Mukherjee', '09830477112', 'Quadra Medical Services', 'Kolkata, West Bengal', NULL, NULL, '2020/12/11 18:18:47', 0, 'docsunetra@gmail.com', 0, 'db4af3260f42b816a9d4c194fea50447'),
(281, 0, 0, 0, 1, 'Asim', 'De', '9436120388', 'Medical services ', 'Agartala', NULL, NULL, '2020/12/11 18:30:52', 0, 'drasimde@rediffmail.com', 0, 'a4ea39c79a81c2e665909fa60c04376f'),
(282, 0, 0, 0, 1, 'Susmita Rani', 'Ghosh', '8413090731', 'Agartala Government Medical College', 'Agartala', NULL, NULL, '2020/12/12 09:05:30', 0, 'susmitaghosh.agt@gmail.com', 0, 'a4bcefd6f17ddcfd3cb1f4e67a3ebfd7'),
(283, 0, 0, 1, 0, 'Kamal', 'Chaudhry', '9988889852', 'Siemens Healthcare Pvt Ltd', 'Chandigarh', NULL, NULL, '2020/12/12 21:16:25', 0, 'kamal.chaudhry@siemens-healthineers.com', 0, 'bb397674f1c3b3191e959194861266cd'),
(284, 0, 0, 1, 0, 'Suthirth', 'Vaidya', '9789958688', 'Predible', 'Bangalore', '2020/12/14 11:21:58', '2020/12/14 11:23:30', '2020/12/14 11:21:15', 1, 'suthirth@predible.com', 0, '4d11c677858cb96952e33d70afa9b399'),
(285, 0, 0, 0, 1, 'Abhishek', 'Yadav', '8826489090', 'Siemens Healthineers', 'Gurugram', NULL, NULL, '2020/12/26 20:02:49', 0, 'abhishek.yadav@siemens-healthineers.com', 0, '71d59e8f9f127088d40d868da399f40e'),
(286, 0, 0, 1, 0, 'Dr Bichitra Kr', 'Hazarika', '9435060545 ', 'STARLAB', 'Nagaon', NULL, NULL, '2020/12/31 17:29:24', 0, 'drbk_hazarika@rediffmail.com', 0, 'd1dbfdfe9c04dd14d2e1a9b238d18f53'),
(287, 0, 0, 1, 0, 'Swaminathan', 'N', '9003159027', 'SHS', 'Chennai', '2021/01/16 10:23:24', '2021/01/16 10:23:44', '2021/01/16 10:22:23', 0, 'njrupesh@gmail.com', 0, '83a09c06d82726ad3e07eb674762f50e'),
(288, 0, 0, 1, 0, 'rakshith', 'shetty', '9591539386', 'nesoulsoft solution', 'banglore', '2021/01/26 08:26:29', '2021/01/26 08:27:29', '2021/01/25 12:07:01', 1, 'rakshith@neosoulsoft.com', 0, 'bd6bd8bb020d7ed2412e010c089ac502'),
(289, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '09880621750', 'coact', 'Kunnur', NULL, NULL, '2021/04/20 08:44:07', 0, 'somunaik@coact.co.in', 0, '1108ac2a72e040d3dc5bea27af47e1f5'),
(290, 0, 0, 1, 0, 'SOMNATH', 'NAIK KOLI', '09880621750', 'coact', 'Kunnur', NULL, NULL, '2021/04/20 08:44:28', 0, 'somnath@coact.co.in', 0, 'e242599919a9da69302444ce2e01610f');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
