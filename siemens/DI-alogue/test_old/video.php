<?php
	/*require_once "config.php";
	
	if(!isset($_SESSION["user_empid"]))
	{
		header("location: index.php");
		exit;
	}*/
?>   
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Video AWS</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#player{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<div id="player"></div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
        //source: 'https://cdn3.wowza.com/1/TzYvcHhDR0M2c3A2/ZzJKUUs4/hls/live/playlist.m3u8',
        source: "https://dnnzuzbuznubl.cloudfront.net/maricoawards/live.m3u8",      
        //source: "https://d38233lepn6k1s.cloudfront.net/out/v1/6e61249a62484f0a8d05c530af9ed8b4/4553131ab95f4d748725f284186e6605/57c32c311a6e4f23bdd4bd4d63d426a1/index.m3u8",
        parentId: "#player",
        plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              //2: '720p',              
              //1: '480p', 
              1: '360p',
              0: '180p' 
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },
        poster: "img/Blank.png",
        width: "100%",
        height: "100%",
        mediacontrol: { buttons: "#00b2dd"}
        
    });
    
    //player.play();
</script>
</body>
</html>