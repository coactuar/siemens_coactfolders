<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SIEMENS:: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link href="/your-path-to-fontawesome/css/fontawesome.css" rel="stylesheet">
  <link href="/your-path-to-fontawesome/css/brands.css" rel="stylesheet">
  <link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet">
</head>

<body>
<div class="container-fliud ">

<div class="nav navbar-default ml-3">
  <img src="img/logo.png" alt=""  width="200px" class="img-fluid logo1"  style="margin-top: 30px; margin-left:60px;">
</div>
<div class="col-12"> 
<img src="img/Headline.png" alt="" class="img-fluid  head" width="60%" style="margin-top: 105px; margin-left:50px;">

</div>
<div class="d-xl-none d-md-none">
<img src="img/Visual.png"  class="img-fluid "   alt="">

</div>
<div class="content-area " style=" margin-left:0px;">
			

				
			
			<div id="loginForm " class="ml-1">
		
			
			<form id="login-form" method="post" role="form">
			<div class="">
	
      
    
			</div>	
			<br/>
			<div class="backs float-right" style="margin-top:200px;width:400px">	
			  <div id="login-message"></div>
			  <!-- <div class="input-group">
				<input type="text" class="form-control" placeholder="Name" name="empName" id="empName" autocomplete="off" required>
			  </div> -->
        <img src="img/text.png" width=84% style="margin-left: 37px; "  class=""alt=""/>
			  <div class="input-group">
				<input type="email" class="form-control" placeholder="Email ID" name="empid" id="empid" autocomplete="off" required>
			  </div>
			  <div class="input-group">
<div>
<input type="image" id="login" src="img/login_button.png" class="btn_width" alt="Submit" width="150" >
</div>
     
        <div class="ml-2">
       <a href="mailto:tanushree@coact.co.in"><img  src="img/needhelp.png" class="btn_width"  width="150" /></a>
        </div>

			  </div>
			  </div>
			  <!-- <div class="offset-4">
			  <p style="font-size:24px" id="demo"></p>
			  </div> -->
        <div class="d-xl-none d-md-none mt-3">
  <img src="img/bottomlogo.png" class="img-fluid" alt="">
  </div>
			</form> 
		  </div> 

</div>

<script>
// Set the date we're counting down to
// var countDownDate = new Date("Dec 10, 2020 11:00:00").getTime();

// // Update the count down every 1 second
// var x = setInterval(function() {

//   // Get today's date and time
//   var now = new Date().getTime();
    
//   // Find the distance between now and the count down date
//   var distance = countDownDate - now;
    
//   // Time calculations for days, hours, minutes and seconds
//   var days = Math.floor(distance / (1000 * 60 * 60 * 24));
//   var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
//   var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
//   var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
//   // Output the result in an element with id="demo"
//   document.getElementById("demo").innerHTML = days + "d " + hours + "h "
//   + minutes + "m " + seconds + "s ";
    
//   // If the count down is over, write some text 
//   if (distance < 0) {
//     clearInterval(x);
//     document.getElementById("demo").innerHTML = "Live Now";
//   }
// }, 1000);
</script>

      
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  



	
  // var phoneNo = document.getElementById('empid');

  // if (phoneNo.value.length < 5 || phoneNo.value.length > 5) {
	//   console.log('Employee ID. is not valid,');
  //   alert("Please Enter The Valid Employee ID Max-Length Should be 5 ");
  //   return false;
  // }


  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
     $('#login').attr('disabled', false);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      else if (data == '0')
      {
          $('#login-message').text('Invalid Email Id. Please try again.');
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      $('#login').attr('disabled', false);
  });

  
  return false;
});
</script>
</body>
</html>