<?php 
require_once "config.php";

if(!isset($_GET["first_name"]) || !isset($_GET["last_name"]) || !isset($_GET["email"]))
{
    echo "You are not authorized to enter";
    exit;
}

if((empty($_GET["first_name"])) || (empty($_GET["last_name"])) || (empty($_GET["email"])))
{
    echo "You are not authorized to enter";
    exit;
}

    $fname = mysqli_real_escape_string($link, $_GET["first_name"]);
    $lname = mysqli_real_escape_string($link, $_GET['last_name']);
    $email  = mysqli_real_escape_string($link, $_GET['email']);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Siemens</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-6 offset-md-3 p-4">
        <div class="card bg-success">
          
          <div class="card-body">
            <h5 class="card-title text-light"><strong>Confirm Consent</strong></h5>
            <p class="card-text text-white">
            To enable full functionality of the login and conference experience your<br><br>
            First Name: <?= $fname ?> <br>
            Last Name: <?= $lname ?> <br>
            Email: <?= $email ?> <br>
            <br>
            has been provided to COACT and will be stored in cloud storage/back-end server located in Sydney, Australia .<br>
            Please confirm your consent to the process of this information for the purpose of <br>
            <ul class="text-white">
            <li>Reporting event viewership to Siemens</li>
            </ul>
            </p>
            <a href="login.php?first_name=<?= $fname ?>&last_name=<?= $lname ?>&email=<?= $email ?>&consent=Yes" class="btn btn-primary">CONSENT</a> <a href="login.php?first_name=<?= $fname ?>&last_name=<?= $lname ?>&email=<?= $email ?>&consent=No" class="btn btn-danger">Do Not CONSENT</a>
          </div>
        </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>