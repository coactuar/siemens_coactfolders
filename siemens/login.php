<?php 
require_once "config.php";

if(!isset($_GET["first_name"]) || !isset($_GET["last_name"]) || !isset($_GET["email"])  || !isset($_GET["consent"]))
{
    echo "You are not authorized to enter";
}
else{
    $fname = mysqli_real_escape_string($link, $_GET["first_name"]);
    $lname = mysqli_real_escape_string($link, $_GET['last_name']);
    $email  = mysqli_real_escape_string($link, $_GET['email']);
    $consent  = mysqli_real_escape_string($link, $_GET['consent']);
    
    $query="select email from users where email ='$email'";
    $res = mysqli_query($link, $query) or die(mysqli_error($link));
    
    if (mysqli_affected_rows($link) > 0)
    {
        $login_date   = date('Y/m/d H:i:s');
        $logout_date   = date('Y/m/d H:i:s', time() + 60);
        
        if($consent=='No'){
            $query="UPDATE users set f_name = '$fname', l_name='$lname', consent='No', login_date='$login_date', logout_date='$login_date' where email='$email'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            
            header('location: logout.php');
        }
        
        else{
        
            $row = mysqli_fetch_assoc($res); 
            
            $today=date("Y/m/d H:i:s");
            
            $dateTimestamp1 = strtotime($row["logout_date"]);
            $dateTimestamp2 = strtotime($today);
            //echo $row[5];
            if ($dateTimestamp1 > $dateTimestamp2)
            {
              echo "You are already logged in from another location. Logout from other locations and try again.";
            }
            else
            {
              $query="UPDATE users set f_name = '$fname', l_name='$lname', consent='Yes', login_date='$login_date', logout_date='$logout_date' where email='$email'";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              
              $query="Insert into tbl_logins(user_emailid, join_time, leave_time) values('$email','$login_date','$logout_date')";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              
              $_SESSION['user_email']    = $email;
              $_SESSION['user_fname']     = $fname;
              $_SESSION['user_lname']     = $lname;
              
              header('location: watchlive.php');
            }
        }
        
    }
    else{
        $login_date   = date('Y/m/d H:i:s');
        $logout_date   = date('Y/m/d H:i:s', time() + 60);
        
        if($consent=='No'){
            $query="insert into users(f_name, l_name, email, consent, joined_at, login_date, logout_date) values ('$fname','$lname','$email','No','$login_date','$login_date','$login_date')";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            
            header('location: logout.php');
        }
        else{
        
        $query="insert into users(f_name, l_name, email, consent, joined_at, login_date, logout_date) values ('$fname','$lname','$email', 'Yes','$login_date','$login_date','$logout_date')";
        $res = mysqli_query($link, $query) or die(mysqli_error($link));
    
        $_SESSION['user_email']    = $email;
        $_SESSION['user_fname']     = $fname;
        $_SESSION['user_lname']     = $lname;
        
        header('location: watchlive.php');
        }
        
    }
    
}
?>