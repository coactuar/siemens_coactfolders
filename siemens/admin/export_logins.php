<?php
require_once "../config.php";

$sql = "SELECT `f_name`,`l_name`,`email`,`consent`,`joined_at`,`login_date`,`logout_date`  FROM `users` order by login_date asc";  
$setRec = mysqli_query($link, $sql); 
$columnHeader = '';  
$columnHeader = "#" . "\t". "First Name" . "\t" . "Last Name" . "\t" . "Email ID" . "\t"."Consent" . "\t". "Registered On" ."\t" ."Last Login On" ."\t" ."Last Logout On" ."\t";  
$setData = '';  
  $i = 1;
  while ($rec = mysqli_fetch_row($setRec)) {  
    $rowData = '"'.$i.'"' . "\t";  
    foreach ($rec as $value) {  
        $value = '"' . $value . '"' . "\t";  
        $rowData .= $value;  
    }  
    $setData .= trim($rowData) . "\n";  
    //echo $rowData.'<br>';
    $i = $i + 1;
}  

  
$file = 'Userlist.xls';  
header("Content-Type: application/octet-stream");  
header("Content-Disposition: attachment; filename=".$file);  
header("Pragma: no-cache");  
header("Expires: 0");  


echo ucwords($columnHeader) . "\n" . $setData . "\n";  

?>